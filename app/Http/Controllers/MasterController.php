<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\WidgetController;
use Illuminate\Http\Request;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class IndexController extends Controller
{
	public $data;
	public $widget;
	function __construct(Request $request )
	{
		$this->widget = new WidgetController($request);
		$this->widget->isLogin($request);
		$this->data['menu']=$this->widget->loadMenu();
	}
	public function propertylisting()
	{
		
		return view('pages.dashboard')->with($this->data);;
	}


}
