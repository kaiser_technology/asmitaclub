<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Helper; // Important
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\WidgetController;
use Illuminate\Http\Request;
use PDF;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class ReportController extends Controller
{
	public $data;
	public $widget;
	function __construct(Request $request )
	{
		setlocale(LC_MONETARY, 'en_IN');
		$this->widget = new WidgetController($request);
		$this->widget->isLogin($request);
		$this->data['menu']=$this->widget->loadMenu($request);
	}
	public function event()
	{
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,b.id as pid,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.bookingFor = 'Event'";
		
		$this->data['list'] = Helper::selectQeury($query);
		return view('report.event')->with($this->data);
	}
	public function reportAjax($for,$id)
	{
		if($for == "eventRecept")
		{
			$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.id = '".$id."'";
			
			$list = Helper::selectQeury($query);
		
			$list[0]->amountInWord =	Helper::numberTowords($list[0]->bookingAmount)." rupees only";
			
			echo json_encode($list);
		}
		if($for == "eventlist")
		{
			 $query ="select * from eventbooking where mobile like '".$id."%' or custName like '".$id."%'";
			
			$list = Helper::selectQeury($query);
			//print_r($list);exit;
			//$list[0]->amountInWord =	Helper::numberTowords($list[0]->bookingAmount)." rupees only";
			
			echo json_encode($list);
		}
	}
	public function propertyListing()
	{
		$this->data['list'] = Helper::getData('property');
		return view('pages.propertyListing')->with($this->data);;
	}
	public function eventReceipt($id)
	{
		 $query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.id = '".$id."'";
			
		$list = Helper::selectQeury($query);
	//print_r($list);exit;
		
		$list[0]->amountInWord 	=	Helper::numberTowords($list[0]->bookingAmount)." rupees only"; 
			
		
	
	$per = explode("|",$list[0]->period);
	$sec = explode("|",$list[0]->Session);
		
		$pdfFilePath = base_path('public');
		$tax = round(($list[0]->price_total * 18)/100);
		$datas = [
			'id' => $id, 
			'event' => $list[0]->event, 
			'recieptno' => $list[0]->recieptno, 
			'custName' => strtoupper($list[0]->custName), 
			'mobile' => $list[0]->mobile, 
			'pincode' => $list[0]->pincode, 
			'address' => $list[0]->address, 
			'email' => $list[0]->email, 
			'gendar' => $list[0]->gendar, 
			'dob' => $list[0]->dob, 
			'age' => $list[0]->age, 
			'properyname' => $list[0]->properyname, 
			'bookingAmount' => $list[0]->bookingAmount, 
			'balance' => $list[0]->balance, 
			'advance' => $list[0]->advance, 
			'tax' => $tax , 
			'basic' => $list[0]->price_total - $tax , 
			'price_total' => $list[0]->price_total, 
			'amountInWord' => strtoupper($list[0]->amountInWord), 
			'period' => $per[0], 
			'Session' => $sec[0], 
			'fromdate' => $list[0]->fromdate, 
			'todate' => $list[0]->todate, 
			'createDate' => $list[0]->createDate, 
			'paymentBy' => $list[0]->paymentBy, 
			'cheque_tran_no' => $list[0]->cheque_tran_no, 
			'cheque_tran_date' => $list[0]->cheque_tran_date, 
			'bankName' => $list[0]->bankName, 
		];
		$pdf = PDF::loadView('report.eventRecipt', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('document.pdf');
		//return view('report.eventRecipt')->with($this->data);;
	}
	public function eventBookingList(Request $request)
	{
		$postdata = $request->all();
		$where = "";
		if(isset($postdata['property']) &&  $postdata['property'] != "All")
		{
			$where.= " && a.properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
		
		  $query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where bookingFor = 'Event' ".$where;
		  $lists = Helper::selectQeury($query);
		 
		  
		
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			 $per = explode("|",$list->period);
	$sec = explode("|",$list->Session);
		 $datas['listing'][] = [
			'bno' => $list->id, 
			'frdate' => date('d-m-Y',strtotime($list->fromdate)), 
			'todate' => date('d-m-Y',strtotime($list->todate)), 
			'season' => $per[0], 
			'session' => $sec[0], 
			'propertyName' => $list->properyname, 
			'bkgdate' => date('d-m-Y', strtotime($list->createDate)), 
			'totamt' => $list->price_total, 
			'bookamt' => $list->bookingAmount, 
			'adv' => $list->advance, 
			'discount' => $list->discount, 
			'balance' => $list->balance, 
			'duedate' => date('d-m-Y',strtotime($list->duedate)), 
			'custName' => $list->custName, 
			'number' => $list->contactNumber, 
		]; 
		 }
		$pdf = PDF::loadView('report.eventBkgList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('BookingList.pdf');
		  
	}
	public function eventReceiptList (Request $request)
	{
		$postdata = $request->all();
		$datas['listing']=array();
		$where = "";
		if(isset($postdata['property']) && $postdata['property'] != "All")
		{
			$where.= " && a.properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
		
		   $query ="select a.*,b.recieptno,b.bookingAmount,b.bookingFinalAmount,b.bookingOpenAmount,b.balanceAmount,b.createDate as dated,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where bookingFor = 'Event' ".$where;
		   $lists = Helper::selectQeury($query);
		 
		  
		
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			 $per = explode("|",$list->period);
	$sec = explode("|",$list->Session);
		 $datas['listing'][] = [
			'rno' => $list->recieptno, 
			'rdate' => date('d-m-Y', strtotime($list->createDate)), 
			'amt' => $list->bookingAmount, 
			'custName' => $list->custName, 
			'number' => $list->contactNumber, 
			'propertyName' => $list->properyname,
			'payMode' => $list->paymentBy,
			'openBal' => $list->bookingOpenAmount,
			'credit' => $list->bookingAmount,
			'closbal' => $list->balanceAmount,
			'trnDate' => $list->cheque_tran_date,
			'dated' => date('d-m-Y',strtotime($list->dated)),
			'bankName' => $list->bankName,
			'frdate' => date('d-m-Y',strtotime($list->fromdate)), 
			'todate' => date('d-m-Y',strtotime($list->todate)), 
			
		]; 
		 }
		$pdf = PDF::loadView('report.eventRecList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('BookingList.pdf');
		  
	}
	public function eventDetail($id) {
		 $query ="select a.*,b.recieptno,b.bookingAmount,b.bookingFinalAmount,b.bookingOpenAmount,b.balanceAmount,b.createDate as dated,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.id = '".$id."' ";
		   $list = Helper::selectQeury($query);
		
		   $per = explode("|",$list[0]->period);
	$sec = explode("|",$list[0]->Session);
		   $datas= [
			'cname' => $list[0]->custName, 
			'add' => $list[0]->address, 
			'contNum' => $list[0]->mobile, 
			'season' => $per[0], 
			'bfrom' => date('d-m-Y',strtotime($list[0]->fromdate)),
			'bto' => date('d-m-Y',strtotime($list[0]->todate)),
			'proname' => $list[0]->properyname,
			'bcode' => $list[0]->id,
			'email' => $list[0]->email,
			'event' => $list[0]->event,
			'pan' => $list[0]->pan,
			'sessionname' => $sec[0],
			'bookno' => $list[0]->id, 
			'bookdate' => date('d-m-Y',strtotime($list[0]->createDate)),
			'rentCharge'=>$list[0]->price_rental,
			'cleaningCharge'=>$list[0]->price_cleaning,
			'eleCharge'=>$list[0]->price_electrical,
			'additionalCharge'=>$list[0]->price_additinal,
			'totalCharge'=>$list[0]->price_total,
			'totalBookAmt'=>$list[0]->bookingAmount,
			'adv'=>$list[0]->advance,
			'discount'=>$list[0]->discount,
			'balAmt'=>$list[0]->balanceAmount,
			'lastPayment'=>$list[0]->dated,
		];
		$pdf = PDF::loadView('report.detailInv', $datas, [], [
		  'title' => 'Asmita Organic Form',
		  'margin_top' => 0
		]);
		$pdf->getMpdf();
		return $pdf->stream('Booking Detial.pdf');   
	}
	public function club()
	{
		$this->data['package'] = Helper::getData('package','','','no',array('pkg_name','pkg_code'));
		$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,b.id as pid,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from memberregistration as a left join paymenttbl as b on  a.id = b.bookingID  left join paymenttransaction as c on b.id = c.paymentID where b.bookingType = 'Club Payment'";
		
		$this->data['list'] = Helper::selectQeury($query);
		
		return view('report.club')->with($this->data);
	}
	public function clubReceipt($id)
	{
		 $query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from memberregistration as a left join paymenttbl as b on  a.id = b.bookingID  left join paymenttransaction as c on b.id = c.paymentID where b.id = '".$id."'";
			
		$list = Helper::selectQeury($query);
	//print_r($list);exit;
		
		$list[0]->amountInWord 	=	Helper::numberTowords($list[0]->bookingAmount)." rupees only"; 
			
		
	$mcod = explode("-",$list[0]->memborCode);
		
		$pdfFilePath = base_path('public');
		$tax = ($list[0]->bookingAmount * 18 )/100;
		$basicCharge = $list[0]->bookingAmount - $tax;
		
		$datas = [
			'id' => $id, 
			'recieptno' => $list[0]->recieptno, 
			'custName' => $list[0]->name, 
			'memberID' => $list[0]->memborCode, 
			'contactNumber' => $list[0]->contactNumber, 
			'bookingAmount' => $list[0]->bookingAmount, 
			'basicCharge' => $basicCharge, 
			'cgst' => $tax / 2, 
			'sgst' => $tax / 2, 
			'totalamount' => $list[0]->bookingAmount, 
			'amountInWord' => $list[0]->amountInWord, 
			'Package_name' => $list[0]->Package_name, 
			'fromdate' => $list[0]->fromdate, 
			'todate' => $list[0]->todate, 
			'createDate' => $list[0]->createDate, 
			'paymentBy' => $list[0]->paymentBy, 
			'cheque_tran_no' => $list[0]->cheque_tran_no, 
			'cheque_tran_date' => $list[0]->cheque_tran_date, 
			'bankName' => $list[0]->bankName, 
		];
		$pdf = PDF::loadView('report.clubRcipt', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.clubRcipt', $datas);
		return $pdf->stream('document.pdf');
		//return view('report.clubRcipt')->with($this->data);;
	}
	
	public function clubMemberList(Request $request)
	{
		$postdata = $request->all();
		$where = "";
		if($postdata['value'] != "")
		{
			$where.= " && member_type = '". $postdata['value']."' && Package_name = '". $postdata['value']."'";
		}
		if($postdata['from'] != "")
		{
			$where.= " && fromdate = '". $postdata['from']."'";
		}
		if($postdata['to'] != "")
		{
			$where.= " && todate = '". $postdata['to']."'";
		}
			
		
		  $query ="select * from memberregistration where 1 ".$where;
		  $lists = Helper::selectQeury($query);
		 
		  
		
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			
		 $datas['listing'][] = [
			'mcode' => $list->memborCode, 
			'mtype' => $list->member_type, 
			'pname' => $list->Package_name, 
			'name' => $list->name, 
			'mobile' => $list->contactNumber, 
			'age' => $list->age, 
			'gender' => $list->gender, 
			'status' => $list->status, 
			'validFrom' => date('d-m-Y',strtotime($list->fromdate)), 
			'validupto' => date('d-m-Y',strtotime($list->todate))
		]; 
		 }
		$pdf = PDF::loadView('report.memberList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('memberList.pdf');
		  
	}
	public function clubPricerList(Request $request)
	{
		$postdata = $request->all();
		$where = "";
		 if($postdata['property'] != "")
		{
			$where.= " && Package_name = '". $postdata['property']."'";
		} 
		if($postdata['from'] != "")
		{
			$where.= " && fromdate = '". $postdata['from']."'";
		}
		if($postdata['to'] != "")
		{
			$where.= " && todate = '". $postdata['to']."'";
		}
			
		
		  $query ="select * from memberregistration where 1 ".$where;
		  $lists = Helper::selectQeury($query);
		 $datas['listing'] = array();
		  
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			
		 $datas['listing'][] = [
			'pcode' => $list->pricecode, 
			'mtype' => $list->member_type, 
			'pname' => $list->Package_name, 
			'period' => $list->period, 
			'season' => "season", 
			'basicCharge' => $list->basicCharge, 
			'tax' => $list->taxPer, 
			'taxamnt' => $list->taxamount, 
			'totalAmnt' => $list->totalamount,
			]; 
		 }
		$pdf = PDF::loadView('report.priceList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('priceList.pdf');
		  
	}
	public function clubDVList(Request $request)
	{
		$postdata = $request->all();
		$where = "";
			if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND visitdate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND visitdate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND visitdate = '".$postdata['to']."'";	
		}
			
		
		  $query ="select * from dailyvisitor where 1 ".$where;
		 
		
		  $lists = Helper::selectQeury($query);
		 $datas['listing'] = array();
		  
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			
		 $datas['listing'][] = [
			'memcode' => $list->memcode, 
			'memtype' => $list->memtype, 
			'period' => $list->period, 
			'packageName' => $list->packageName, 
			'person' => $list->person, 
			'totalcharge' => $list->totalcharge, 
			'tax' => $list->tax, 
			'contactnumber' => $list->contactnumber,
			'name' => $list->name,
			'trnid' => $list->trnid,
			'rcptno' => $list->rcptno,
			'gendar' => $list->gendar,
			'visitdate' => $list->visitdate,
			]; 
		 }
		$pdf = PDF::loadView('report.dvList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('priceList.pdf');
		  
	}
	public function clubDvReceipt($id)
	{
		 $query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from dailyvisitor as a left join paymenttbl as b on  a.id = b.dvid  left join paymenttransaction as c on b.id = c.paymentID where a.id = '".$id."' and b.bookingType = 'Daily Visitor'";
			
		$list = Helper::selectQeury($query);
	//print_r($list);exit;
		
		$list[0]->amountInWord 	=	Helper::numberTowords($list[0]->bookingAmount)." rupees only"; 
			
		
		$pdfFilePath = base_path('public');
		$datas = [
			'id' => $id, 
			'recieptno' => $list[0]->recieptno, 
			'custName' => $list[0]->name, 
			'memberID' => $list[0]->memcode, 
			'contactNumber' => $list[0]->contactnumber, 
			'basicCharge' => $list[0]-> totalcharge - $list[0]-> tax, 
			'cgst' => round($list[0]-> tax / 2), 
			'sgst' => round($list[0]-> tax / 2), 
			'totalamount' => $list[0]-> totalcharge, 
			'amountInWord' => $list[0]->amountInWord, 
			'Package_name' => $list[0]->packageName, 
			'createDate' => $list[0]->createdDate,
			'fromdate' => $list[0]->createdDate, 
			'todate' => $list[0]->createdDate, 			
			'paymentBy' => $list[0]->paymentBy, 
			'cheque_tran_no' => $list[0]->cheque_tran_no, 
			'cheque_tran_date' => $list[0]->cheque_tran_date, 
			'bankName' => $list[0]->bankName, 
		];
		$pdf = PDF::loadView('report.dvRcipt', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('document.pdf');
		//return view('report.eventRecipt')->with($this->data);;
	}
	public function clubReceiptList(Request $request)
	{
		$postdata = $request->all();
		$datas['listing']=array();
		$where = " ";
		if(isset($postdata['package']) && !empty($postdata['package'])){
				
			$where.= " AND a.Package_name = '".$postdata['package']."'";	
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
		
		   $query ="select a.*,b.recieptno,b.bookingAmount,b.bookingFinalAmount,b.bookingOpenAmount,b.balanceAmount,b.createDate as dated,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from memberregistration as a left join paymenttbl as b on  a.id = b.bookingID  left join paymenttransaction as c on b.id = c.paymentID where b.bookingType = 'Club Payment' ".$where;
		   $lists = Helper::selectQeury($query);
		// print_r($lists);exit;
		  
		
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			 $per = explode("|",$list->period);
	
	
		 $datas['listing'][] = [
			'rno' => $list->recieptno, 
			'rdate' => date('d-m-Y', strtotime($list->createDate)), 
			'amt' => $list->bookingAmount, 
			'custName' => $list->name, 
			'number' => $list->contactNumber, 
			'propertyName' => $list->Package_name,
			'payMode' => $list->paymentBy,
			'openBal' => $list->bookingOpenAmount,
			'credit' => $list->bookingAmount,
			'closbal' => $list->balanceAmount,
			'trnDate' => $list->cheque_tran_date,
			'dated' => date('d-m-Y',strtotime($list->dated)),
			'bankName' => $list->bankName,
			'frdate' => date('d-m-Y',strtotime($list->fromdate)), 
			'todate' => date('d-m-Y',strtotime($list->todate)), 
			
		]; 
		 }
		$pdf = PDF::loadView('report.cblubRecList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('BookingList.pdf');
	}
	public function clubReceiptPage(Request $request){
		
		$postdata = $request->all();
		$where = " ";
		if(isset($postdata['package']) && !empty($postdata['package'])){
				
			$where.= " AND a.Package_name = '".$postdata['package']."'";	
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND b.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND b.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND b.createDate = '".$postdata['to']."'";	
		}
		
		
		$this->data['package'] = Helper::getData('package','','','no',array('pkg_name','pkg_code'));
		$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,b.id as pid,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from memberregistration as a left join paymenttbl as b on  a.id = b.bookingID  left join paymenttransaction as c on b.id = c.paymentID where b.bookingType = 'Club Payment' and a.settlement = 'Yes'".$where;
		
		$this->data['list'] = Helper::selectQeury($query);
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		return view('report.clubReceiptPage')->with($this->data);
	}
	public function clubMemberPage(Request $request){
		
		$postdata = $request->all();
		$where = " ";
		if(isset($postdata['package']) && !empty($postdata['package'])){
				
			$where.= " AND Package_name = '".$postdata['package']."'";	
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND createDate = '".$postdata['from']."'";	
		} 
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND createDate = '".$postdata['to']."'";	
		}
		
		 $query ="select * from memberregistration where 1 ".$where;
		 
		
		$this->data['list'] = Helper::selectQeury($query);
		$whr = array('field'=>"todate","val"=>date("m"));			
		$this->data['expdata'] = Helper::getExpData('memberregistration',$whr);
		
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		return view('report.clubMemberPage')->with($this->data);
	}
		public function clubDailyVisitorPage(Request $request)
	{
		$postdata = $request->all();
		$where = "";
		
		
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND visitdate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND visitdate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND visitdate = '".$postdata['to']."'";	
		}
			
		
		  $query ="select * from dailyvisitor where 1 ".$where;
		 
		
		$this->data['list'] = Helper::selectQeury($query);
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		return view('report.clubDailyVisitorPage')->with($this->data);
		  
	}
	
	/*Event Report*/
	public function EventReceiptPage(Request $request)
	{
		
		$postdata = $request->all();
		
		$where = "";
		if(isset($postdata['property']) && $postdata['property'] != "All")
		{
			$where.= " && a.properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
			
			
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,b.id as pid,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.bookingFor = 'Event'".$where;
		
		$this->data['list'] = Helper::selectQeury($query);
		return view('report.eventreceiptPage')->with($this->data);
	}
	public function EventBDPage(Request $request)
	{
		
		$postdata = $request->all();
		$where = "";
		if(isset($postdata['property']) &&  $postdata['property'] != "All")
		{
			$where.= " && properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND createDate = '".$postdata['to']."'";	
		}
		
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$query ="select * from eventbooking where bookingFor like 'Event'".$where;
		$this->data['list'] = Helper::selectQeury($query);
		return view('report.eventbdPage')->with($this->data);
	}
	public function RentReceiptPage(Request $request)
	{
		$postdata = $request->all();
		
		$where = "";
		if(isset($postdata['property']) && $postdata['property'] != "All")
		{
			$where.= " && a.properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$query ="select a.*,b.recieptno,b.bookingAmount,b.createDate,b.id as pid,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where a.bookingFor = 'Rental'".$where;
		
		$this->data['list'] = Helper::selectQeury($query);
		return view('report.rentreceiptPage')->with($this->data);
	}
	
	public function rentreceipt (Request $request)
	{
		$postdata = $request->all();
		$datas['listing']=array();
		$where = "";
		if(isset($postdata['property']) && $postdata['property'] != "All")
		{
			$where.= " && a.properyname = '". $postdata['property']."'";
		}
		if(isset($postdata['from']) && !empty($postdata['from']) && isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate BETWEEN '".$postdata['from']."' AND '".$postdata['to']."'";	
			//$where.= " AND createDate >= '".$postdata['from']."' AND createDate <= '".$postdata['to']."' ";	
		}
		else if(isset($postdata['from']) && !empty($postdata['from'])){
				
			$where.= " AND a.createDate = '".$postdata['from']."'";	
		}
		else if(isset($postdata['to']) && !empty($postdata['to'])){
				
			$where.= " AND a.createDate = '".$postdata['to']."'";	
		}
			
		
		   $query ="select a.*,b.recieptno,b.bookingAmount,b.bookingFinalAmount,b.bookingOpenAmount,b.balanceAmount,b.createDate as dated,c.paymentBy,c.cheque_tran_no,c.cheque_tran_date,c.bankName from eventbooking as a left join paymenttbl as b on  a.id = b.evtid  left join paymenttransaction as c on b.id = c.paymentID where bookingFor = 'Rental' ".$where;
		   $lists = Helper::selectQeury($query);
		 
		  
		
		$pdfFilePath = base_path('public');
		 foreach($lists as $list){
			 $per = explode("|",$list->period);
	$sec = explode("|",$list->Session);
		 $datas['listing'][] = [
			'rno' => $list->recieptno, 
			'rdate' => date('d-m-Y', strtotime($list->createDate)), 
			'amt' => $list->bookingAmount, 
			'custName' => $list->custName, 
			'number' => $list->contactNumber, 
			'propertyName' => $list->properyname,
			'payMode' => $list->paymentBy,
			'openBal' => $list->bookingOpenAmount,
			'credit' => $list->bookingAmount,
			'closbal' => $list->balanceAmount,
			'trnDate' => $list->cheque_tran_date,
			'dated' => date('d-m-Y',strtotime($list->dated)),
			'bankName' => $list->bankName,
			'frdate' => date('d-m-Y',strtotime($list->fromdate)), 
			'todate' => date('d-m-Y',strtotime($list->todate)), 
			
		]; 
		 }
		$pdf = PDF::loadView('report.rentRecList', $datas, [], [
  'title' => 'Asmita Organic Form',
  'margin_top' => 0
]);
		$pdf->getMpdf();
		//$pdf = PDF::loadView('report.eventRecipt', $data);
		return $pdf->stream('BookingList.pdf');
		  
	}
}
