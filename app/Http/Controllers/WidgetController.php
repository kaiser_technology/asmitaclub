<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class WidgetController extends Controller
{
	public $data;
	function __construct($request)
	{
		//$req = $this->isLogin
	
	}
	
	
	public function isLogin($request)
	{
		$session=$request->session()->get('logindetail');
		
		if($session['loginstatus']== "true")
		{
			define("USERNAME",$session['name']);	
			define("USERID",$session['user_id']);	
			define("USEREMAIL",$session['email']);	
			//define("ROLEID",);	
			return true;
		}
		else{
			
			return redirect('/');	
		}
	}
	public function loadMenu($request)
	{
		$session=$request->session()->get('logindetail');
		$result = DB::table('menu_tbl')
					->where('parent_menu_id', 0)
					->Where('role_id', 'like', '%' . $session['role_id'] . '%')
					->get();
					
					
		$i=0;			
		foreach($result as $value){
			$menu[$i]['main'] = $value;
			$childeresult = DB::table('menu_tbl')
					->where('parent_menu_id', $value->id)
					->get();
			if(count($childeresult)>0){
				$menu[$i]['chield'] = json_decode($childeresult);
			}
			$i++;
			//print_r($value);exit;
		}			
		return $menu;
	}
	public function insQue($tbl,$arr)
	{
			DB::table($tbl)
					->insert($arr);
	}
	
}
