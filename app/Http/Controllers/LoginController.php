<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\WidgetController;
use Illuminate\Http\Request;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class LoginController extends Controller
{
	public $data;
	public $widget;
	function __construct(Request $request )
	{
		
		$this->widget = new WidgetController($request);
		$this->data['menu']=$this->widget->loadMenu($request);
	}

	public function login()
	{
		
		return view('pages.login');
	}
	public function loginSubmit(Request $request)
	{
		 $input = $request->all();
		$email = $input['username'];
		$password = sha1($input['password']); 

		$cnt = DB::table('login_credential')
		->where([['email', '=', $email],['password', '=',$password],['status', '=','Active']])->count();
	
		if($cnt == 1){
			$result = DB::table('user_tbl')
					->where('user_email', $email)->get();
					if(isset($result[0])){		
					$Name = $result[0]->user_prefix.' '.$result[0]->user_firstName.' '.$result[0]->user_lastName;
					$id = $result[0]->user_id;
					$roleid = $result[0]->role_id;
					$email = $result[0]->user_email;
					$sesarr = array("email"=>$email,"name"=>$Name,"user_id"=>$id,"role_id"=>$roleid,"loginstatus"=>"true");
					$request->session()->put('logindetail',$sesarr);
					
					$this->data['pageclass'] = "external-page sb-l-c sb-r-c";
					return redirect('/dashboard');
			}else{
				$this->data['pageclass'] = "external-page sb-l-c sb-r-c";
				$this->data['msg'] = "You have enter wrong detail";
				return view('pages.login')->with($this->data);
			}			
		}else{
			$this->data['pageclass'] = "external-page sb-l-c sb-r-c";
			return view('pages.login')->with($this->data);
		}		
	}
	public function logout(Request $request){
		
		$request->session()->forget('logindetail');
		return redirect('/');
	}
}
