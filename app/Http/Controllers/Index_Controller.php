<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Helper; // Important
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\WidgetController;
use Illuminate\Http\Request;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class IndexController extends Controller
{
	public $data;
	public $widget;
	function __construct(Request $request )
	{
		
		$this->widget = new WidgetController($request);
		$this->widget->isLogin($request);
		$this->data['menu']=$this->widget->loadMenu($request);
	}
	public function index()
	{
		
		return view('pages.dashboard')->with($this->data);;
	}
	public function propertyListing()
	{
		$this->data['list'] = Helper::getData('property');
		return view('pages.propertyListing')->with($this->data);;
	}
	public function propertyAdd()
	{
		
		return view('pages.propertyAdd')->with($this->data);;
	}
	public function priceListing()
	{
		$this->data['list'] = Helper::getData('package_price_tbl','','','no',array('*'));
		$this->data['prolist'] = Helper::getData('property_price_tbl','','','no',array('*'));
		return view('pages.priceListing')->with($this->data);;
	}
	public function priceAdd()
	{
		$rec = Helper::lastRecord('package_price_tbl','id');
		$this->data['pkgid'] = $rec->id + 1;
		$rec = Helper::lastRecord('property_price_tbl','id');
		if(!empty($rec)){
		$this->data['proid'] = $rec->id + 1;
		}else{
			$this->data['proid'] = 1;
		}
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name','code'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.priceAdd')->with($this->data);;
	}
	public function pkgpricesubmite(Request $request)
	{
		$postdata = $request->all();
		$memType = explode('|',$postdata['memtype']);
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		$pkgname = explode('|',$postdata['pkgname']);
		
		$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		
		$param=array("price_code"=>$priceCode,"pkg_name"=>$pkgname[0],"member_type"=>$memType[0],"period"=>$perType[0],"season"=>$seaType[0],"basic_charge"=>$postdata['basiccharge'],"tax_per"=>$postdata['tax'],"tax_amnt"=>$postdata['taxamt'],"total_amnt"=>$postdata['total'],"start_date"=>$postdata['from'],"end_date"=>$postdata['to']);
		Helper::insertData('package_price_tbl',$param);
		
		return redirect('masters/price/list');
		
	}
	public function propricesubmite(Request $request)
	{
		$postdata = $request->all();
		$memType = explode('|',$postdata['memtype']);
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		$pkgname = explode('|',$postdata['pkgname']);
		
		$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		
		$param=array("season"=>$seaType[0],
					"startDate"=>$postdata['from'],
					"endDate"=>$postdata['to'],
					"type"=>$postdata['type'],
					"proName"=>$postdata['prop'],
					"proCode"=>$postdata['prccode'],
					"priceCode"=>$postdata['propcode'],
					"period"=>$perType[0],
					"rentRate"=>$postdata['rentrate'],
					"rentTax"=>$postdata['renttax'],
					"rentTotalPrice"=>$postdata['rentamount'],
					"cleaningRate"=>$postdata['clerate'],
					"cleaningTax"=>$postdata['cletax'],
					"cleaningTotalRate"=>$postdata['cleamount'],
					"eleRate"=>$postdata['elerate'],
					"eleTax"=>$postdata['eletax'],
					"eleTotalRate"=>$postdata['eleamount'],
					"othRate"=>$postdata['othrate'],
					"othTax"=>$postdata['othtax'],
					"othTotalRate"=>$postdata['othamount'],
					"totalAmount"=>$postdata['totalAmount'],
					"min"=>$postdata['min'],
					"max"=>$postdata['max'],
					"capacity"=>$postdata['capacity']);
					
		Helper::insertData('property_price_tbl',$param);
		
		return redirect('masters/price/list');
		
	}
	public function packageListing()
	{
		$this->data['list'] = Helper::getData('package','','','no',array('*'));
		return view('pages.packageListing')->with($this->data);;
	}
	public function packageAdd()
	{
		$this->data['packtype'] = Helper::getData('package_type','','','no',array('name','code'));
		
		$this->data['services'] = Helper::getData('services','','','no',array('name','code'));
		return view('pages.packageAdd')->with($this->data);;
	}
	public function pkgSubmite(Request $request)
	{
		$postdata = $request->all();
		//$priceCode
		$pkgtype = explode('|',$postdata['pkgtype']);
		
		$serv = implode(',',$postdata['serv']);
		$param=array("pkg_name"=>$postdata['pkgname'],"pkg_type"=>$pkgtype[0],"pkg_code"=>$postdata['pkgcode'],"no_of_per"=>$postdata['noofper'],"services"=>$serv);
		Helper::insertData('package',$param);
		return redirect('masters/package/list');
	}
	public function vendorListing()
	{
		$this->data['list'] = Helper::getData('vendor_tbl','','','no',array('*'));
		
		return view('pages.vendorListing')->with($this->data);
	}
	public function vendorAdd()
	{
		
		return view('pages.vendorAdd')->with($this->data);;
	}
	public function vendorsubmite(Request $request)
	{
		$postdata = $request->all();
		$servicesdetail="";
		$image = $request->file('upload1');
		$imgname = time().'.'.$image->getClientOriginalExtension();
		$destinationPath = public_path('/assets/images/vendor/');
		$image->move($destinationPath, $imgname);
		$param=array("vendor_name"=>$postdata['venname'],
					"address"=>$postdata['venadd'],
					"contactnumber1"=>$postdata['number'],
					"contactnumber2"=>$postdata['number1'],
					"pincode"=>$postdata['pincode'],
					"Acc_no"=>$postdata['accno'],
					"Bank_name"=>$postdata['bnkname'],
					"vend_code"=>$postdata['vencode'],
					"vend_type"=>$postdata['ventype'],
					"vend_status"=>$postdata['venstatus'],
					"pancard"=>$postdata['pan'],
					"diposit"=>$postdata['deposit'],
					"regd_no"=>$postdata['regno'],
					"ifsc_code"=>$postdata['ifsccode'],
					"benificiary_name"=>$postdata['benefname'],
					"validity"=>$postdata['validity'],
					"img_name"=>$imgname,
					"photography"=>$postdata['phoroy'],
					"music"=>$postdata['musroy'],
					"catering"=>$postdata['catroy'],
					"Decoration"=>$postdata['decroy'],
					);
		Helper::insertData('vendor_tbl',$param);
		return redirect('masters/vendor/list');
	}
	public function enquiryListing()
	{
		$this->data['list'] = Helper::getData('enquiry','','','no',array('*'));
		return view('pages.enquiryListing')->with($this->data);;
	}

	public function enquiryAdd()
	{
		$rec = Helper::lastRecord('enquiry','id');
		$this->data['lastid'] = $rec->enq_no + 1;
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.enquiryAdd')->with($this->data);;
	}
	public function enquirysubmite(Request $request)
	{
		$postdata = $request->all();
		$param=array("enq_no"=>$postdata['enqno'],
					"enq_for"=>$postdata['enqfor'],
					"contact_no"=>$postdata['number'],
					"contact_name"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"email"=>$postdata['email'],
					"refer"=>$postdata['refer'],
					"enq_detail"=>$postdata['enqdetail'],
					"status"=>$postdata['enqstatus'],
					"next_follow_date"=>$postdata['enqdate'],
					"rental"=>$postdata['rental'],
					"from"=>$postdata['from'],
					"to"=>$postdata['to'],
					"period"=>$postdata['period'],
					"session"=>$postdata['session'],
					"propertyName"=>$postdata['proptypename'],
					"persion"=>$postdata['person'],
					"electrical"=>$postdata['electrical'],
					"cleaning"=>$postdata['cleaning'],
					"other"=>$postdata['other'],
					"total"=>$postdata['total'],
					);
		Helper::insertData('enquiry',$param);
		return redirect('enquiry/listing');
	}
	public function ragistrationListing()
	{
		$this->data['list'] = Helper::getData('memberregistration','','','no',array('*'));
		
		return view('pages.ragistrationListing')->with($this->data);;
	}
	public function ragistration()
	{
		$rec = Helper::lastRecord('memberregistration','id');
		$this->data['lastid'] = $rec->id + 1;
		$this->data['membercode'] = "ASGC-".str_pad( $rec->id + 1, 4, "0", STR_PAD_LEFT );
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		
		return view('pages.ragistration')->with($this->data);;
	}
	public function ragistrationSubmit(Request $request)
	{
		$postdata = $request->all();
			
		
		$param=array("memborCode"=>$postdata['memcode'],
					"Package_name"=>$postdata['proptypename'],
					"member_type"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"person"=>$postdata['person'],
					"contactNumber"=>$postdata['number'],
					"email"=>$postdata['email'],
					"name"=>$postdata['name'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"dob"=>$postdata['dob'],
					"gender"=>$postdata['gender'],
					"age"=>$postdata['age'],
					"status"=>$postdata['memstatus'],
					"cardStatus"=>$postdata['cardstatus'],
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"basicCharge"=>$postdata['basic_charge'],
					"taxPer"=>$postdata['taxper'],
					"taxamount"=>$postdata['tax'],
					"totalamount"=>$postdata['totalamount'],
					"discount"=>$postdata['discount'],
					"balance"=>$postdata['balance'],
					"paystatus"=>$postdata['paystatus'],
					"pricecode"=>$postdata['pricecode']
					);
		Helper::insertData('memberregistration',$param);
		return redirect('club/registration');
	}
	public function dailyVisitor()
	{
		$this->data['list'] = Helper::getData('dailyvisitor','','','no',array('*'));
		
		return view('pages.dailyVisitorListing')->with($this->data);;
	}
	public function dailyVisitorAdd()
	{
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		
		return view('pages.dailyvisitor')->with($this->data);;
	}
	public function dailyVisitorSubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array("memcode"=>$postdata['memCode'],
					"memtype"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"packageName"=>$postdata['packageName'],
					"person"=>$postdata['person'],
					"totalcharge"=>$postdata['totcharge'],
					"tax"=>$postdata['tax'],
					"contactnumber"=>$postdata['contno'],
					"name"=>$postdata['name'],
					"trnid"=>$postdata['trnid'],
					"rcptno"=>$postdata['rcpt'],
					"gendar"=>$postdata['gender'],
					"visitdate"=>$postdata['visitdate'],
					);
		Helper::insertData('dailyvisitor',$param);
		return redirect('club/dailyvisitor');
	}
	public function bookingListing()
	{
		$whr[] = array('field'=>"bookingFor","value"=>"Event","action"=>"=");	
		$this->data['list'] = Helper::getData('eventbooking',$whr,'','no',array('*'));
		return view('pages.eventListing')->with($this->data);;
	}

	public function bookingAdd()
	{
		$rec = Helper::lastRecord('eventbooking','id');
		if(!empty($rec)){
		$this->data['eventid'] = $rec->id + 1;
		}else{
			$this->data['eventid'] = 1;
		}
		
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.bookingAdd')->with($this->data);;
	}
	public function bookingsubmite(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"bookingFor"=>"Event",
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"period"=>$postdata['period'],
					"Session"=>$postdata['session'],
					"property_type"=>"",
					"properyname"=>$postdata['proptypename'],
					"status"=>"",
					"mobile"=>$postdata['mobnum'],
					"custName"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"contactNumber"=>$postdata['contnum'],
					"pincode"=>$postdata['pincode'],
					"event"=>$postdata['evetn'],
					"dob"=>$postdata['dob'],
					"age"=>$postdata['age'],
					"anndate"=>$postdata['anndate'],
					"gendar"=>$postdata['gender'],
					"pan"=>$postdata['pancard'],
					"email"=>$postdata['email'],
					"price_rental"=>$postdata['rental'],
					"price_cleaning"=>$postdata['cleaning'],
					"price_electrical"=>$postdata['electrical'],
					"price_additinal"=>$postdata['other'],
					"price_total"=>$postdata['totalA'],
					"minAdv"=>$postdata['minadv'],
					"totalB"=>$postdata['totalb'],
					"gtotal"=>$postdata['totalg'],
					"advance"=>$postdata['adv'],
					"discount"=>$postdata['disount'],
					"balance"=>$postdata['balance'],
					"duedate"=>$postdata['due'],
					);
					//print_r($param);exit;
		$lstid=Helper::insertData('eventbooking',$param);
		$receptNo = "EB_".time()."_".$lstid;
		if($postdata['balance']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Event Booking",
					"bookingID"=>$lstid,
					"bookingAmount"=>$postdata['adv'],
					"bookingDiscount"=>$postdata['disount'],
					"bookingFinalAmount"=>$postdata['totalA'],
					"balanceAmount"=>$postdata['balance'],
					"paymentStatus"=>$pstatus,
					"dueDate"=>$postdata['due'],
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['adv'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"advance",
					);	
		Helper::insertData('paymenttransaction',$param3);				
		return redirect('event/booking/list');
	}
	public function renitalListing()
	{
		$whr[] = array('field'=>"bookingFor","value"=>"Rental","action"=>"=");	
		
		$this->data['list'] = Helper::getData('eventbooking',$whr,'','no',array('*'));
		return view('pages.rentListing')->with($this->data);;
	}
	
	public function rentalAdd()
	{
		$rec = Helper::lastRecord('eventbooking','id');
		if(!empty($rec)){
		$this->data['eventid'] = $rec->id + 1;
		}else{
			$this->data['eventid'] = 1;
		}
		
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.rentalAdd')->with($this->data);;
	}
	public function rentalsubmite(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"bookingFor"=>"Rental",
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"period"=>$postdata['period'],
					"Session"=>$postdata['session'],
					"property_type"=>"",
					"properyname"=>$postdata['proptypename'],
					"status"=>"",
					"mobile"=>$postdata['mobnum'],
					"custName"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"contactNumber"=>$postdata['contnum'],
					"pincode"=>$postdata['pincode'],
					"event"=>$postdata['evetn'],
					"dob"=>$postdata['dob'],
					"age"=>$postdata['age'],
					"anndate"=>$postdata['anndate'],
					"gendar"=>$postdata['gender'],
					"pan"=>$postdata['pancard'],
					"email"=>$postdata['email'],
					"price_rental"=>$postdata['rental'],
					"price_cleaning"=>$postdata['cleaning'],
					"price_electrical"=>$postdata['electrical'],
					"price_additinal"=>$postdata['other'],
					"price_total"=>$postdata['totalA'],
					"minAdv"=>$postdata['minadv'],
					"totalB"=>$postdata['totalb'],
					"gtotal"=>$postdata['totalg'],
					"advance"=>$postdata['adv'],
					"discount"=>$postdata['disount'],
					"balance"=>$postdata['balance'],
					"duedate"=>$postdata['due'],
					);
					//print_r($param);exit;
		$lstid=Helper::insertData('eventbooking',$param);
		$receptNo = "EB_".time()."_".$lstid;
		if($postdata['balance']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Event Booking",
					"bookingID"=>$lstid,
					"bookingAmount"=>$postdata['adv'],
					"bookingDiscount"=>$postdata['disount'],
					"bookingFinalAmount"=>$postdata['totalA'],
					"balanceAmount"=>$postdata['balance'],
					"paymentStatus"=>$pstatus,
					"dueDate"=>$postdata['due'],
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['adv'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"advance",
					);	
		Helper::insertData('paymenttransaction',$param3);				
		return redirect('event/bookingrentals/list');
	}
	public function ajaxCall($table,$id)
	{
		$where[]=array("field"=>'id',"action"=>"=","value"=>$id);	
		$list = Helper::getData($table,$where,'','no',array('name'));
		echo json_encode($list);
	}
	public function ajaxFieldCall($table,$field,$id)
	{
		
		$where[]=array("field"=>$field,"action"=>"=","value"=>$id);	
		$list = Helper::getData($table,$where,'','no',array('name'));
		echo json_encode($list);
		
	}
	public function evetnBookingAvaibilty($fromdate,$todate)
	{
		$query = "Select * from `eventbooking` where '".$todate."' between fromdate and todate OR `todate` between '".$fromdate."' and '".$todate."' OR '".$fromdate."' between `fromdate` and `todate` OR `fromdate` between '".$fromdate."' and '".$todate."' ";
			$res = Helper::selectQeury($query);
			if(empty($res)){ $r = array("status"=>"available"); } else {
			$r = array("status"=>"booked");
			}
			echo json_encode($r);
	}
	public function pymntSubmit(Request $request)
	{
		$postdata = $request->all();
		if($postdata['closbal']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$postdata['trnid'],
					"bookingType"=>"Event Booking",
					"bookingID"=>$postdata['bookingNo'],
					"bookingAmount"=>$postdata['credit'],
					"bookingDiscount"=>$postdata['discount'],
					"bookingFinalAmount"=>$postdata['bkamt'],
					"balanceAmount"=>$postdata['closbal'],
					"paymentStatus"=>$pstatus,
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['credit'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"balance amount",
					);	
		Helper::insertData('paymenttransaction',$param3);
		return redirect('event/billing/add');
	}
	public function billingAdd()
	{
		$rec = Helper::lastRecord('paymenttbl','id');
		if(!empty($rec)){
		$this->data['pymid'] = $rec->id + 1;
		}else{
			$this->data['pymid'] = 1;
		}
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['trnid'] = $receptNo = "EB_".time()."_".$this->data['pymid'];
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.billingAdd')->with($this->data);
	}
	public function search()
	{
		return view('pages.search')->with($this->data);
	}
	
}
