<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Helper; // Important
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\WidgetController;
use Illuminate\Http\Request;
/**
 * IndexController
 *
 * Controller to house all the functionality directly
 * related to the ModuleOne.
 */
class IndexController extends Controller
{
	public $data;
	public $widget;
	public $sessiondata;
	function __construct(Request $request )
	{
		setlocale(LC_MONETARY, 'en_IN');
		$this->widget = new WidgetController($request);
		$this->widget->isLogin($request);
		$this->data['menu']=$this->widget->loadMenu($request);
		
		$this->data['sessiondata'] = $request->session()->get('logindetail');
		//print_r($sessiondata);exit;
	}
	public function index()
	{
		$whr = array('field'=>"createdDate","val"=>date("Y-m-d"));	
		$this->data['todaydv'] = Helper::getDayData('dailyvisitor',$whr,'totalcharge');
		
		//$whr= array('field'=>"createDate","val"=>date("d"));
		$whr= array('field'=>"settlementDate","val"=>date("Y-m-d"));
		$this->data['todaymem'] = Helper::getDayMemData('memberregistration',$whr,'totalamount');
		
		//amit changes
		$whr = array('field'=>"createDate","val"=>date("Y-m-d"));	
		$this->data['todayeve'] = Helper::getDayEvnData('eventbooking',$whr,'price_total');
		//amit changes end
		
		$whr = array('field'=>"createdDate","val"=>date("m"));			
		$this->data['mondv'] = Helper::getMonthData('dailyvisitor',$whr,'totalcharge');
		
		$whr= array('field'=>"createDate","val"=>date("m"));	
		$this->data['monmem'] = Helper::getMonthMemData('memberregistration',$whr,'totalamount');
		
		//amit  changes
		$whr = array('field'=>"createDate","val"=>date("m"));			
		$this->data['moneve'] = Helper::getMonthEvnData('eventbooking',$whr,'price_total');
		//amit changes
		
		$whr = array('field'=>"createdDate","val"=>date("Y"));			
		$this->data['yerdv'] = Helper::getYearData('dailyvisitor',$whr,'totalcharge');
		
		$whr= array('field'=>"createDate","val"=>date("Y"));	
		$this->data['yermem'] = Helper::getYearMemData('memberregistration',$whr,'totalamount');
		
		//amit changes
		$whr = array('field'=>"createDate","val"=>date("Y"));			
		$this->data['yereve'] = Helper::getYearEvnData('eventbooking',$whr,'price_total');
		//amit changes end
		
		return view('pages.dashboard')->with($this->data);;
	}
	public function propertyListing()
	{
		$this->data['list'] = Helper::getData('property');
		return view('pages.propertyListing')->with($this->data);;
	}
	public function propertyAdd()
	{
		$rec = Helper::lastRecord('property','id');
		$this->data['propid'] = $rec->id + 1;
		return view('pages.propertyAdd')->with($this->data);;
	}
	public function propertySubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"pro_code"=>$postdata['propcode'],
					"pro_name"=>$postdata['propname'],
					"pro_address"=>$postdata['propadd'],
					"pro_type"=>$postdata['proptype'],
					"capacity"=>$postdata['capacity'],
					"min_per"=>$postdata['minper'],
					"max_per"=>$postdata['maxper'],
					"ben_type"=>$postdata['bentype'],
					"ben_name"=>$postdata['benname'],
					);
					
		Helper::insertData('property',$param);
		return redirect('masters/property/list');
	}
	public function propertyUpdate($id)
	{
		
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('property',$whr,'','no',array('*'));
		
		$this->data['prop'] = $res;
		return view('pages.propertyUpdate')->with($this->data);;
	}
	public function propertyUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"pro_code"=>$postdata['propcode'],
					"pro_name"=>$postdata['propname'],
					"pro_address"=>$postdata['propadd'],
					"pro_type"=>$postdata['proptype'],
					"capacity"=>$postdata['capacity'],
					"min_per"=>$postdata['minper'],
					"max_per"=>$postdata['maxper'],
					"ben_type"=>$postdata['bentype'],
					"ben_name"=>$postdata['benname'],
					);
			$where = array("id"=>$postdata['srno']);		
		Helper::updateData('property',$where,$param);
		return redirect('masters/property/list');
	}
	public function deleteRecord($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('property',$where);
		return redirect('masters/property/list');
	}
	public function priceListing()
	{
		$this->data['list'] = Helper::getData('package_price_tbl','','','no',array('*'));
		$this->data['prolist'] = Helper::getData('property_price_tbl','','','no',array('*'));
		return view('pages.priceListing')->with($this->data);;
	}
	public function priceAdd()
	{
		$rec = Helper::lastRecord('package_price_tbl','id');
		$this->data['pkgid'] = $rec->id + 1;
		$rec = Helper::lastRecord('property_price_tbl','id');
		if(!empty($rec)){
		$this->data['proid'] = $rec->id + 1;
		}else{
			$this->data['proid'] = 1;
		}
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name','code'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code','seasonFor'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.priceAdd')->with($this->data);;
	}
	public function pkgpricesubmite(Request $request)
	{
		$postdata = $request->all();
		$memType = explode('|',$postdata['memtype']);
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		$pkgname = explode('|',$postdata['pkgname']);
		
		$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		//print_r($postdata);exit;
		$param=array("price_code"=>$priceCode,"pkg_name"=>$pkgname[0],"member_type"=>$memType[0],"period"=>$perType[0],"season"=>$seaType[0],"basic_charge"=>$postdata['basiccharge'],"tax_cgst"=>$postdata['tax_cgst'],"tax_sgst"=>$postdata['tax_sgst'],"tax_amnt"=>$postdata['taxamt'],"total_amnt"=>$postdata['total'],"start_date"=>$postdata['cfrom'],"end_date"=>$postdata['cto']);
		Helper::insertData('package_price_tbl',$param);
		
		return redirect('masters/price/list');
		
	}
	public function clubPriceEdit($id)
	{
			$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name','code'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code','seasonFor'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('package_price_tbl',$whr,'','no',array('*'));
		//print_r($res);exit;
		$this->data['prop'] = $res;
		return view('pages.clubPriceEdit')->with($this->data);;
	}
	
	public function pkgpriceUpdate(Request $request)
	{
		$postdata = $request->all();
		$memType = explode('|',$postdata['memtype']);
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		$pkgname = explode('|',$postdata['pkgname']);
		
		$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		//print_r($postdata);exit;
		$param=array("price_code"=>$priceCode,"pkg_name"=>$pkgname[0],"member_type"=>$memType[0],"period"=>$perType[0],"season"=>$seaType[0],"basic_charge"=>$postdata['basiccharge'],"tax_cgst"=>$postdata['tax_cgst'],"tax_sgst"=>$postdata['tax_sgst'],"tax_amnt"=>$postdata['taxamt'],"total_amnt"=>$postdata['total'],"start_date"=>$postdata['cfrom'],"end_date"=>$postdata['cto']);
		
		$where = array("id"=>$postdata['srNo']);		
		Helper::updateData('package_price_tbl',$where,$param);
		return redirect('masters/price/list');
		
	}
	public function pkgpriceDelete($id)
	{
		$where = array("id"=>$id);
		Helper::deleteRecord('package_price_tbl',$where);
		return redirect('masters/price/list');
	}
	public function propricesubmite(Request $request)
	{
		$postdata = $request->all();
		$memType = explode('|',$postdata['memtype']);
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		$pkgname = explode('|',$postdata['pkgname']);
		
		$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		
		$param=array("season"=>$seaType[0],
					"startDate"=>$postdata['from'],
					"endDate"=>$postdata['to'],
					"time"=>$postdata['timing'],
					"type"=>$postdata['type'],
					"proName"=>$postdata['prop'],
					"proCode"=>$postdata['propcode'],
					"priceCode"=>$postdata['prccode'],
					"period"=>$perType[0],
					"rentRate"=>$postdata['rentrate'],
					"rentTax"=>$postdata['renttax'],
					"rentTotalPrice"=>$postdata['rentamount'],
					"cleaningRate"=>$postdata['clerate'],
					"cleaningTax"=>$postdata['cletax'],
					"cleaningTotalRate"=>$postdata['cleamount'],
					"eleRate"=>$postdata['elerate'],
					"eleTax"=>$postdata['eletax'],
					"eleTotalRate"=>$postdata['eleamount'],
					"othRate"=>$postdata['othrate'],
					"othTax"=>$postdata['othtax'],
					"othTotalRate"=>$postdata['othamount'],
					"totalAmount"=>$postdata['totalAmount'],
					"min"=>$postdata['min'],
					"max"=>$postdata['max'],
					"capacity"=>$postdata['capacity']);
					
		Helper::insertData('property_price_tbl',$param);
		
		return redirect('masters/price/list');
		
	}
	public function propertyPriceEdit($id)
	{
			$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name','code'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code','seasonFor'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('property_price_tbl',$whr,'','no',array('*'));
		//print_r($res);exit;
		$this->data['prop'] = $res;
		return view('pages.propertyPriceEdit')->with($this->data);;
	}
	public function propriceUpdate(Request $request)
	{
		$postdata = $request->all();
		
		$seaType = explode('|',$postdata['season']);
		$perType = explode('|',$postdata['period']);
		
		//$priceCode = $seaType[1].''.$pkgname[1].''.$memType[1].''.$perType[1];
		
		$param=array("season"=>$seaType[0],
					"startDate"=>$postdata['from'],
					"endDate"=>$postdata['to'],
					"time"=>$postdata['timing'],
					"type"=>$postdata['type'],
					"proName"=>$postdata['prop'],
					"proCode"=>$postdata['propcode'],
					"priceCode"=>$postdata['prccode'],
					"period"=>$perType[0],
					"rentRate"=>$postdata['rentrate'],
					"rentTax"=>$postdata['renttax'],
					"rentTotalPrice"=>$postdata['rentamount'],
					"cleaningRate"=>$postdata['clerate'],
					"cleaningTax"=>$postdata['cletax'],
					"cleaningTotalRate"=>$postdata['cleamount'],
					"eleRate"=>$postdata['elerate'],
					"eleTax"=>$postdata['eletax'],
					"eleTotalRate"=>$postdata['eleamount'],
					"othRate"=>$postdata['othrate'],
					"othTax"=>$postdata['othtax'],
					"othTotalRate"=>$postdata['othamount'],
					"totalAmount"=>$postdata['totalAmount'],
					"min"=>$postdata['min'],
					"max"=>$postdata['max'],
					"capacity"=>$postdata['capacity']);
		$where = array("id"=>$postdata['srno']);		
		Helper::updateData('property_price_tbl',$where,$param);	
		
		return redirect('masters/price/list');
		
	}
	public function propriceDelete($id)
	{
		$where = array("id"=>$id);
		Helper::deleteRecord('property_price_tbl',$where);
		return redirect('masters/price/list');
	}
	public function packageListing()
	{
		$this->data['list'] = Helper::getData('package','','','no',array('*'));
		return view('pages.packageListing')->with($this->data);;
	}
	public function packageAdd()
	{
		$this->data['packtype'] = Helper::getData('package_type','','','no',array('name','code'));
		
		$this->data['services'] = Helper::getData('services','','','no',array('name','code'));
		return view('pages.packageAdd')->with($this->data);;
	}
	public function pkgSubmite(Request $request)
	{
		$postdata = $request->all();
		//$priceCode
		$pkgtype = explode('|',$postdata['pkgtype']);
		
		$serv = implode(',',$postdata['serv']);
		$param=array("pkg_name"=>$postdata['pkgname'],"pkg_type"=>$pkgtype[0],"pkg_code"=>$postdata['pkgcode'],"no_of_per"=>$postdata['noofper'],"services"=>$serv);
		Helper::insertData('package',$param);
		return redirect('masters/package/list');
	}
	public function packageUpdate($id)
	{
		
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('package',$whr,'','no',array('*'));
		$this->data['packtype'] = Helper::getData('package_type','','','no',array('name','code'));
		
		$this->data['services'] = Helper::getData('services','','','no',array('name','code'));
		$this->data['prop'] = $res;
		return view('pages.packageUpdate')->with($this->data);;
	}
	public function packageUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		//$priceCode
		$pkgtype = explode('|',$postdata['pkgtype']);
		$serv = implode(',',$postdata['serv']);
		$param=array("pkg_name"=>$postdata['pkgname'],"pkg_type"=>$pkgtype[0],"pkg_code"=>$postdata['pkgcode'],"no_of_per"=>$postdata['noofper'],"services"=>$serv);
		$where = array("id"=>$postdata['id']);
		Helper::updateData('package',$where,$param);
		return redirect('masters/package/list');
	}
	public function packageRecordDel($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('package',$where);
		return redirect('masters/package/list');
	}
	
	
	public function vendorListing()
	{
		$this->data['list'] = Helper::getData('vendor_tbl','','','no',array('*'));
		
		return view('pages.vendorListing')->with($this->data);
	}
	public function vendorAdd()
	{
		
		return view('pages.vendorAdd')->with($this->data);;
	}
	public function vendorsubmite(Request $request)
	{
		$postdata = $request->all();
		$servicesdetail="";
		$image = $request->file('upload1');
		$imgname = time().'.'.$image->getClientOriginalExtension();
		$destinationPath = public_path('/assets/images/vendor/');
		$image->move($destinationPath, $imgname);
		$param=array("vendor_name"=>$postdata['venname'],
					"address"=>$postdata['venadd'],
					"contactnumber1"=>$postdata['number'],
					"contactnumber2"=>$postdata['number1'],
					"pincode"=>$postdata['pincode'],
					"Acc_no"=>$postdata['accno'],
					"Bank_name"=>$postdata['bnkname'],
					"vend_code"=>$postdata['vencode'],
					"vend_type"=>$postdata['ventype'],
					"vend_status"=>$postdata['venstatus'],
					"pancard"=>$postdata['pan'],
					"diposit"=>$postdata['deposit'],
					"regd_no"=>$postdata['regno'],
					"ifsc_code"=>$postdata['ifsccode'],
					"benificiary_name"=>$postdata['benefname'],
					"validity"=>$postdata['validity'],
					"img_name"=>$imgname,
					"photography"=>$postdata['phoroy'],
					"music"=>$postdata['musroy'],
					"catering"=>$postdata['catroy'],
					"Decoration"=>$postdata['decroy'],
					);
		Helper::insertData('vendor_tbl',$param);
		return redirect('masters/vendor/list');
	}
	public function vendorUpdate($id)
	{
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('vendor_tbl',$whr,'','no',array('*'));
		$this->data['pro'] = $res;
		return view('pages.vendorUpdate')->with($this->data);;
	}
	public function vendorUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		$servicesdetail="";
		
		$param=array("vendor_name"=>$postdata['venname'],
					"address"=>$postdata['venadd'],
					"contactnumber1"=>$postdata['number'],
					"contactnumber2"=>$postdata['number1'],
					"pincode"=>$postdata['pincode'],
					"Acc_no"=>$postdata['accno'],
					"Bank_name"=>$postdata['bnkname'],
					"vend_code"=>$postdata['vencode'],
					"vend_type"=>$postdata['ventype'],
					"vend_status"=>$postdata['venstatus'],
					"pancard"=>$postdata['pan'],
					"diposit"=>$postdata['deposit'],
					"regd_no"=>$postdata['regno'],
					"ifsc_code"=>$postdata['ifsccode'],
					"benificiary_name"=>$postdata['benefname'],
					"validity"=>$postdata['validity'],
					"photography"=>$postdata['phoroy'],
					"music"=>$postdata['musroy'],
					"catering"=>$postdata['catroy'],
					"Decoration"=>$postdata['decroy'],
					);
		$where = array("id"=>$postdata['id']);
		Helper::updateData('vendor_tbl',$where,$param);
		return redirect('masters/vendor/list');
	}
	public function vendorRecordDel($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('vendor_tbl',$where);
		return redirect('masters/vendor_tbl/list');
	}
	public function enquiryListing()
	{
		$this->data['list'] = Helper::getData('enquiry','','','no',array('*'));
		return view('pages.enquiryListing')->with($this->data);;
	}

	public function enquiryAdd()
	{
		$rec = Helper::lastRecord('enquiry','id');
		$this->data['lastid'] = $rec->enq_no + 1;
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.enquiryAdd')->with($this->data);;
	}
	public function enquirysubmite(Request $request)
	{
		$postdata = $request->all();
		
		$seaType = explode('|',$postdata['session']);
		$perType = explode('|',$postdata['period']);
		$proType = explode('|',$postdata['proptypename']);
		
		$param=array("enq_no"=>$postdata['enqno'],
					"enq_for"=>$postdata['enqfor'],
					"contact_no"=>$postdata['number'],
					"contact_name"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"email"=>$postdata['email'],
					"refer"=>$postdata['refer'],
					"enq_detail"=>$postdata['enqdetail'],
					"status"=>$postdata['enqstatus'],
					"next_follow_date"=>$postdata['enqdate'],
					"rental"=>$postdata['rental'],
					"from"=>$postdata['from'],
					"to"=>$postdata['to'],
					"period"=>$perType[0],
					"session"=>$seaType[0],
					"propertyName"=>$proType[0],
					"persion"=>$postdata['person'],
					"min"=>$postdata['minperson'],
					"mex"=>$postdata['maxperson'],
					"electrical"=>$postdata['electrical'],
					"cleaning"=>$postdata['cleaning'],
					"other"=>$postdata['other'],
					"total"=>$postdata['total'],
					);
		
		Helper::insertData('enquiry',$param);
		return redirect('enquiry/listing');
	}
	
	public function enquiryDelete($id)
	{
		$where = array("id"=>$id);
		Helper::deleteRecord('enquiry',$where);
		return redirect('enquiry/listing');
	}
	public function enquiryUpdate($id)
	{
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('enquiry',$whr,'','no',array('*'));
		$this->data['pro'] = $res[0];
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.enquiryupdate')->with($this->data);;
	}
	public function enquiryUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array("enq_no"=>$postdata['enqno'],
					"enq_for"=>$postdata['enqfor'],
					"contact_no"=>$postdata['number'],
					"contact_name"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"email"=>$postdata['email'],
					"refer"=>$postdata['refer'],
					"enq_detail"=>$postdata['enqdetail'],
					"status"=>$postdata['enqstatus'],
					"next_follow_date"=>$postdata['enqdate'],
					"rental"=>$postdata['rental'],
					"from"=>$postdata['from'],
					"to"=>$postdata['to'],
					"period"=>$postdata['period'],
					"session"=>$postdata['session'],
					"propertyName"=>$postdata['proptypename'],
					"persion"=>$postdata['person'],
					"electrical"=>$postdata['electrical'],
					"cleaning"=>$postdata['cleaning'],
					"other"=>$postdata['other'],
					"total"=>$postdata['total'],
					);
		$where = array("enq_no"=>$postdata['enqno']);
		Helper::updateData('enquiry',$where,$param);
		return redirect('enquiry/listing');
	}
	
	
	
	
	
	public function ragistrationDelete($id)
	{
		$where = array("id"=>$id);
		Helper::deleteRecord('memberregistration',$where);
		return redirect('club/registration');
	}
	public function ragistrationListing()
	{
		$this->data['list'] = Helper::getData('memberregistration','','','no',array('*'));
		
		return view('pages.ragistrationListing')->with($this->data);;
	}
	public function ragistration()
	{
		$rec = Helper::lastRecord('memberregistration','id');
		$this->data['lastid'] = $rec->id + 1;
		$this->data['membercode'] = str_pad( $rec->id + 1, 4, "0", STR_PAD_LEFT );
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		
		return view('pages.ragistration')->with($this->data);;
	}
	public function ragistrationSubmit(Request $request)
	{
		$postdata = $request->all();

		if($postdata['memtype'] == "Couple"){
			
			$postdata['tax'] = round($postdata['tax']/2);
			$postdata['totalamount'] = round($postdata['totalamount']/2);
			$postdata['balance'] = round($postdata['balance']/2);
		}
			
		$proptypename = explode('|',$postdata['proptypename']);
		$param=array("memborCode"=>$postdata['memcode'],
					"Package_name"=>$proptypename[0],
					"member_type"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"person"=>$postdata['person'],
					"contactNumber"=>$postdata['number'],
					"email"=>$postdata['email'],
					"name"=>$postdata['name'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"dob"=>$postdata['dob'],
					"gender"=>$postdata['gender'],
					"age"=>$postdata['age'],
					"status"=>$postdata['memstatus'],
					"cardStatus"=>$postdata['cardstatus'],
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"basicCharge"=>$postdata['basic_charge'],
					"taxPer"=>$postdata['taxper'],
					"taxamount"=>$postdata['tax'],
					"totalamount"=>$postdata['totalamount'],
					"discount"=>$postdata['discount'],
					"balance"=>$postdata['balance'],
					"paystatus"=>$postdata['paystatus'],
					"pricecode"=>$postdata['pricecode']
					);
		Helper::insertData('memberregistration',$param);
		
		
		if($postdata['memtype'] == "Couple"){
			
			$param=array("memborCode"=>$postdata['memcode'],
					"Package_name"=>$proptypename[0],
					"member_type"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"person"=>$postdata['person'],
					"contactNumber"=>$postdata['number2'],
					"email"=>$postdata['email2'],
					"name"=>$postdata['name2'],
					"address"=>$postdata['custadd2'],
					"pincode"=>$postdata['pincode2'],
					"dob"=>$postdata['dob2'],
					"gender"=>$postdata['gender2'],
					"age"=>$postdata['age2'],
					"status"=>$postdata['memstatus'],
					"cardStatus"=>$postdata['cardstatus2'],
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"basicCharge"=>$postdata['basic_charge'],
					"taxPer"=>$postdata['taxper'],
					"taxamount"=>$postdata['tax'],
					"totalamount"=>$postdata['totalamount'],
					"discount"=>$postdata['discount'],
					"balance"=>$postdata['balance'],
					"paystatus"=>$postdata['paystatus'],
					"pricecode"=>$postdata['pricecode']
					);
		Helper::insertData('memberregistration',$param);
			
			
		}
		
		
		
		return redirect('club/registration');
	}
	public function ragistrationUpdate($id)
	{
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('memberregistration',$whr,'','no',array('*'));
		$this->data['pro'] = $res[0];
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		return view('pages.memberregistrationUpdate')->with($this->data);;
	}
	public function ragistrationUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		$proptypename = explode('|',$postdata['proptypename']);
		$param=array("memborCode"=>$postdata['memcode'],
					"Package_name"=>$proptypename[0],
					"member_type"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"person"=>$postdata['person'],
					"contactNumber"=>$postdata['number'],
					"email"=>$postdata['email'],
					"name"=>$postdata['name'],
					"address"=>$postdata['custadd'],
					"pincode"=>$postdata['pincode'],
					"dob"=>$postdata['dob'],
					"gender"=>$postdata['gender'],
					"age"=>$postdata['age'],
					"status"=>$postdata['memstatus'],
					"cardStatus"=>$postdata['cardstatus'],
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"basicCharge"=>$postdata['basic_charge'],
					"taxPer"=>$postdata['taxper'],
					"taxamount"=>$postdata['tax'],
					"totalamount"=>$postdata['totalamount'],
					"discount"=>$postdata['discount'],
					"balance"=>$postdata['balance'],
					"paystatus"=>$postdata['paystatus'],
					"pricecode"=>$postdata['pricecode']
					);
		$where = array("id"=>$postdata['srno']);
		Helper::updateData('memberregistration',$where,$param);
		return redirect('club/registration');
	}
	
	
	
	
	public function dailyVisitor()
	{
		
		$this->data['list'] = Helper::getData('dailyvisitor','','','no',array('*'));
		$whr[] = array('field'=>"createdDate","value"=>date("Y-m-d"),"action"=>">=");	
		$this->data['todaylist'] = Helper::getData('dailyvisitor',$whr,'','no',array('*'));
		return view('pages.dailyVisitorListing')->with($this->data);;
	}
	public function dailyVisitorAdd()
	{
		$rec = Helper::lastRecord('dailyvisitor','id');
		$lid = $rec->id + 1;
		$this->data['DLcode'] = "ARPLDV".$lid;
		$this->data['DVRECcode'] = "DV/19-20/".$lid;
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		
		return view('pages.dailyvisitor')->with($this->data);;
	}
	public function dailyVisitorUpdate($id)
	{
		$whr[] = array('field'=>"id","value"=>$id,"action"=>"=");	
		$res = Helper::getData('dailyvisitor',$whr,'','no',array('*'));
		$this->data['pro'] = $res[0];
		$this->data['package'] = Helper::getData('package','','','no',array('*'));
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		$this->data['msterType'] = Helper::getData('member_type','','','no',array('name'));
		
		return view('pages.dailyvisitorUpdate')->with($this->data);;
	}
	public function dailyVisitorSubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array("memcode"=>$postdata['memCode'],
					"memtype"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"packageName"=>$postdata['packageName'],
					"person"=>$postdata['person'],
					"totalcharge"=>$postdata['totcharge'],
					"tax"=>$postdata['tax'],
					"contactnumber"=>$postdata['contno'],
					"name"=>$postdata['name'],
					"trnid"=>$postdata['trnid'],
					"rcptno"=>$postdata['rcpt'],
					"gendar"=>$postdata['gender'],
					"visitdate"=>$postdata['visitdate'],
					);
		$lstid=Helper::insertData('dailyvisitor',$param);
		
		$receptNo = "DV_".time()."_".$lstid;
			$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Daily Visitor",
					"dvid"=>$lstid,
					"bookingAmount"=>$postdata['totcharge'],
					"bookingDiscount"=>"0",
					"bookingFinalAmount"=>$postdata['totcharge'],
					"bookingOpenAmount"=>$postdata['totcharge'],
					"balanceAmount"=>"0",
					"paymentStatus"=>"Completed",
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['totcharge'],
					"paymentBy"=>"Cash",
					"remarks"=>"Dialy Visitor",
					);	
		Helper::insertData('paymenttransaction',$param3);
		return redirect('report/club/dvreceipt/'.$lstid);
		//return redirect('club/dailyvisitor');
	}
	public function dailyVisitorUpdateSubmit(Request $request)
	{
		$postdata = $request->all();
		$param=array("memcode"=>$postdata['memCode'],
					"memtype"=>$postdata['memtype'],
					"period"=>$postdata['session'],
					"packageName"=>$postdata['packageName'],
					"person"=>$postdata['person'],
					"totalcharge"=>$postdata['totcharge'],
					"tax"=>$postdata['tax'],
					"contactnumber"=>$postdata['contno'],
					"name"=>$postdata['name'],
					"trnid"=>$postdata['trnid'],
					"rcptno"=>$postdata['rcpt'],
					"gendar"=>$postdata['gender'],
					"visitdate"=>$postdata['visitdate'],
					);
			$where = array("id"=>$postdata['id']);
		Helper::updateData('dailyvisitor',$where,$param);		
		return redirect('club/dailyvisitor');
	}
	public function dvdeleteRecord($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('dailyvisitor',$where);
		return redirect('club/dailyvisitor');
	}
	public function bookingListing()
	{
		$whr[] = array('field'=>"bookingFor","value"=>"Event","action"=>"=");	
		$this->data['list'] = Helper::getData('eventbooking',$whr,'','no',array('*'));
		return view('pages.eventListing')->with($this->data);;
	}
	public function bookdeleteRecord($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('eventbooking',$where);
		return redirect('event/bookingListing');
	}
	public function bookingAdd()
	{
		$rec = Helper::lastRecord('eventbooking','id');
		if(!empty($rec)){
		$this->data['eventid'] = $rec->id + 1;
		}else{
			$this->data['eventid'] = 1;
		}
		
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.bookingAdd')->with($this->data);;
	}
	public function bookingsubmite(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"bookingFor"=>"Event",
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"period"=>$postdata['period'],
					"Session"=>"",
					"property_type"=>"",
					"properyname"=>$postdata['proptypename'],
					"status"=>"",
					"mobile"=>$postdata['mobnum'],
					"custName"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"contactNumber"=>"",
					"pincode"=>$postdata['pincode'],
					"event"=>$postdata['evetn'],
					"dob"=>$postdata['dob'],
					"age"=>$postdata['age'],
					"anndate"=>null,
					"gendar"=>$postdata['gender'],
					"pan"=>"",
					"email"=>$postdata['email'],
					"price_rental"=>$postdata['rental'],
					"price_cleaning"=>0,
					"price_electrical"=>0,
					"price_additinal"=>0,
					"price_total"=>$postdata['rental'],
					"minAdv"=>0,
					"totalB"=>0,
					"gtotal"=>0,
					"advance"=>$postdata['adv'],
					"discount"=>0,
					"balance"=>$postdata['balance'],
					"basicCharge"=>$postdata['other'],
					"cgst"=>$postdata['cgst'],
					"sgst"=>$postdata['sgst'],
					);
					//print_r($param);exit;
		$lstid=Helper::insertData('eventbooking',$param);
		$receptNo = "EP_".time()."_".$lstid;
		if($postdata['balance']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Event Booking",
					"evtid"=>$lstid,
					"bookingAmount"=>$postdata['adv'],
					"bookingDiscount"=>0,
					"bookingFinalAmount"=>$postdata['rental'],
					"bookingOpenAmount"=>$postdata['rental'],
					"balanceAmount"=>$postdata['balance'],
					"paymentStatus"=>$pstatus,
					"dueDate"=>null,
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['adv'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"advance",
					);	
		Helper::insertData('paymenttransaction',$param3);				
		return redirect('event/booking/list');
	}
	public function renitalListing()
	{
		$whr[] = array('field'=>"bookingFor","value"=>"Rental","action"=>"=");	
		
		$this->data['list'] = Helper::getData('eventbooking',$whr,'','no',array('*'));
		return view('pages.rentListing')->with($this->data);;
	}
	public function renitaldeleteRecord($id)
	{
		
		$where = array("id"=>$id);
		Helper::deleteRecord('eventbooking',$where);
		return redirect('event/rentListing');
	}
	public function rentalAdd()
	{
		$rec = Helper::lastRecord('eventbooking','id');
		if(!empty($rec)){
		$this->data['eventid'] = $rec->id + 1;
		}else{
			$this->data['eventid'] = 1;
		}
		
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.rentalAdd')->with($this->data);;
	}
	public function rentalsubmite(Request $request)
	{
		$postdata = $request->all();
		$param=array(
					"bookingFor"=>"Rental",
					"fromdate"=>$postdata['from'],
					"todate"=>$postdata['to'],
					"period"=>$postdata['period'],
					"Session"=>"",
					"property_type"=>"",
					"properyname"=>$postdata['proptypename'],
					"status"=>"",
					"mobile"=>$postdata['mobnum'],
					"custName"=>$postdata['custname'],
					"address"=>$postdata['custadd'],
					"contactNumber"=>"",
					"pincode"=>$postdata['pincode'],
					"event"=>$postdata['evetn'],
					"dob"=>$postdata['dob'],
					"age"=>$postdata['age'],
					"anndate"=>null,
					"gendar"=>$postdata['gender'],
					"pan"=>"",
					"email"=>$postdata['email'],
					"price_rental"=>$postdata['rental'],
					"price_cleaning"=>0,
					"price_electrical"=>0,
					"price_additinal"=>0,
					"price_total"=>$postdata['rental'],
					"minAdv"=>0,
					"totalB"=>0,
					"gtotal"=>0,
					"advance"=>$postdata['adv'],
					"discount"=>0,
					"balance"=>$postdata['balance'],
					"basicCharge"=>$postdata['other'],
					"cgst"=>$postdata['cgst'],
					"sgst"=>$postdata['sgst'],
					);
					//print_r($param);exit;
		$lstid=Helper::insertData('eventbooking',$param);
		$receptNo = "CR_".time()."_".$lstid;
		if($postdata['balance']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Event Booking",
					"evtid"=>$lstid,
					"bookingAmount"=>$postdata['adv'],
					"bookingDiscount"=>0,
					"bookingFinalAmount"=>$postdata['rental'],
					"bookingOpenAmount"=>$postdata['rental'],
					"balanceAmount"=>$postdata['balance'],
					"paymentStatus"=>$pstatus,
					"dueDate"=>null,
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['adv'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"advance",
					);	
		Helper::insertData('paymenttransaction',$param3);				
		return redirect('event/bookingrentals/list');
	}
	public function ajaxCall($table,$id)
	{
		$where[]=array("field"=>'id',"action"=>"=","value"=>$id);	
		$list = Helper::getData($table,$where,'','no',array('name'));
		echo json_encode($list);
	}
	public function ajaxFieldCall($table,$field,$id)
	{
		
		$where[]=array("field"=>$field,"action"=>"=","value"=>$id);	
		$list = Helper::getData($table,$where,'','no',array('name'));
		echo json_encode($list);
		
	}
	public function evetnBookingAvaibilty($fromdate,$todate)
	{
		$query = "Select * from `eventbooking` where '".$todate."' between fromdate and todate OR `todate` between '".$fromdate."' and '".$todate."' OR '".$fromdate."' between `fromdate` and `todate` OR `fromdate` between '".$fromdate."' and '".$todate."' ";
			$res = Helper::selectQeury($query);
			if(empty($res)){ $r = array("status"=>"available"); } else {
			$r = array("status"=>"booked");
			}
			echo json_encode($r);
	}
	public function pymntSubmit(Request $request)
	{
		$postdata = $request->all();
		if($postdata['closbal']== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		$param2 = array(
					"recieptno"=>$postdata['trnid'],
					"bookingType"=>"Event Booking",
					"evtid"=>$postdata['bookingNo'],
					"bookingAmount"=>$postdata['credit'],
					"bookingDiscount"=>$postdata['discount'],
					"bookingFinalAmount"=>$postdata['bkamt'],
					"bookingOpenAmount"=>$postdata['openAmount'],
					"balanceAmount"=>$postdata['closbal'],
					"paymentStatus"=>$pstatus,
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['credit'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"balance amount",
					);	
		Helper::insertData('paymenttransaction',$param3);
		return redirect('event/billing/add');
	}
	public function billingAdd()
	{
		$rec = Helper::lastRecord('paymenttbl','id');
		if(!empty($rec)){
		$this->data['pymid'] = $rec->id + 1;
		}else{
			$this->data['pymid'] = 1;
		}
		$this->data['property'] = Helper::getData('property','','','no',array('pro_name','pro_code'));
		$this->data['trnid'] = $receptNo = "EB_".time()."_".$this->data['pymid'];
		$this->data['season'] = Helper::getData('season','','','no',array('name','code'));
		$this->data['period'] = Helper::getData('period','','','no',array('name','code'));
		return view('pages.billingAdd')->with($this->data);
	}
	public function search()
	{
		return view('pages.search')->with($this->data);
	}
	public function clubPayment()
	{
		$where[]=array("field"=>'settlement',"action"=>"!=","value"=>"Yes");	
		$this->data['plist'] = Helper::getData('memberregistration',$where,'','no',array('*'));
		$where[]=array("field"=>'settlement',"action"=>"=","value"=>"Yes");	
		$query = 'SELECT * FROM `memberregistration` WHERE `settlement` ="Yes" and `settlementDate`= "'.date('Y-m-d').'" ';
		$this->data['slist'] = Helper::selectQeury($query);
		// = Helper::getData('memberregistration',$where,'','no',array('*'));
		$this->data['list'] = Helper::getData('memberregistration','','','no',array('*'));
		//print_r($this->data['list']);exit;
		return view('pages.clubPayment')->with($this->data);
	}
	public function payment($id)
	{
		$where[]=array("field"=>'id',"action"=>"=","value"=>$id);	
		$list = Helper::getData('memberregistration',$where,'','no',array('*'));
		$this->data['list'] =$list;
		//print_r($this->data['list']);exit;
		return view('pages.payment')->with($this->data);
	}
	public function paymentsettlement($id){
		
		$param = array(
				"settlement"=>"Yes",
				"settlementDate"=>date('Y-m-d')
		);
		$where=array('id' =>$id);
		 Helper::updateData('memberregistration',$where,$param);
		return redirect('club/payment');
	}
	public function clubPaymentSubmit(Request $request)
	{
		
		$postdata = $request->all();
		//print_r($postdata);exit;
		$balanceamount = $postdata['totalamount'] - $postdata['payble'];
		if($balanceamount== 0)
		{
			$pstatus = "Completed";
		}else {
			$pstatus = "Pending";
		}
		//$receptNo = "CP_".time()."_".$postdata['id'];
		$receptNo = "CP_19-20/".$postdata['id'];
		$param2 = array(
					"recieptno"=>$receptNo,
					"bookingType"=>"Club Payment",
					"bookingID"=>$postdata['id'],
					"bookingAmount"=>$postdata['payble'],
					"bookingFinalAmount"=>$postdata['totalamount'],
					"balanceAmount"=>$balanceamount,
					"paymentStatus"=>$pstatus,
					);
		$pymid=Helper::insertData('paymenttbl',$param2);	
		$param3 = array(
					"paymentID"=>$pymid,
					"paidAmount"=>$postdata['payble'],
					"paymentBy"=>$postdata['payment'],
					"cheque_tran_no"=>$postdata['cheno'],
					"cheque_tran_date"=>$postdata['chedate'],
					"bankName"=>$postdata['bankName'],
					"remarks"=>"club amount",
					);	
		Helper::insertData('paymenttransaction',$param3); 
		
		$where[]=array("field"=>'id',"action"=>"=","value" =>$postdata['id']);	
		$data = Helper::getData('memberregistration',$where,'','no',array('*'));
		$no = $data[0]->noOfTransaction + 1;
		$param = array(
				"balance"=>$balanceamount,
				"noOfTransaction"=> $no,
				"paystatus"=>$pstatus,
				"settlement"=>"No"
		);
		$where=array('id' =>$postdata['id']);
		 Helper::updateData('memberregistration',$where,$param);
		return redirect('club/payment');
		
	}
	public function ajaxGetDate($cdate,$type)
	{
		/* $postdata = $request->all();
		$cdate = $postdata['cdate'];
		$type = $postdata['type']; */
		
		if($type == "Quarterly"){	$todate = date('Y-m-d', strtotime('+3 month', strtotime($cdate)));	 $todate = date('Y-m-d', strtotime('-1 Days', strtotime($todate)));} 
		if($type == "Halfyearly"){	$todate = date('Y-m-d', strtotime('+6 month', strtotime($cdate))); $todate = date('Y-m-d', strtotime('-1 Days', strtotime($todate)));}
		if($type == "Yearly"){ $todate =date('Y-m-d', strtotime('+1 Year', strtotime($cdate)));	$todate = date('Y-m-d', strtotime('-1 Days', strtotime($todate)));}
		if($type == "21Days"){ $todate = date('Y-m-d', strtotime('+21 Days', strtotime($cdate)));	}
		if($type == "Regular"){	$todate = date('Y-m-d', strtotime('+1 Days', strtotime($cdate)));}
		if($type == "Daily Visitors"){	$todate = date('Y-m-d', strtotime('+1 Days', strtotime($cdate))); }
	echo $todate;
	}
}
