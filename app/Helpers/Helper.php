<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;
class Helper{

	public static function getData($table, $where="",$ordby='',$count = "no",$select=array())
	{
		
		if(!empty($where))
		{
			
			foreach($where as $val)
			{
				
				$field = $val['field'];
				$action = $val['action'];
				$value = $val['value'];
				 $stringwhere= ["$field","$action","$value"];
			}
			
			if($count == "no"){
			$result = DB::table($table)->where([$stringwhere])->orderBy('id', 'desc')->get();
			}else{
				$result = DB::table($table)->where([$stringwhere])->count();
			}
		}
		else{
			if($count == "no"){
				if(!empty($select))
				{
					
					$result = DB::table($table)->select($select)->orderBy('id', 'desc')->get();	
				}else{
					$result = DB::table($table)->orderBy('id', 'desc')->get();	
				}
				
			}else{
				$result = DB::table($table)->count();
			}
		}
		
		
		return $result;
	}
	public static function getMonthData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereMonth($where['field'], $where['val'])
                
                ->sum($field);
		
		
		return $users;
	}
	public static function getMonthMemData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereMonth($where['field'], $where['val'])
                ->where("settlement", "Yes")
                ->sum($field);
		
		
		return $users;
	}
	public static function getMonthEvnData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereMonth($where['field'], $where['val'])
				->where("bookingFor","Rental")
                ->sum($field);
		
		
		return $users;
	}
	public static function getExpData($table, $where)
	{
		
		$users = DB::table($table)
                ->whereMonth($where['field'], $where['val'])
				->orderBy($where['field'], 'desc')
                ->get();
		
		
		return $users;
	}
	public static function getYearData($table, $where, $field)
	{ 
		
		$users = DB::table($table)
                ->whereYear($where['field'], $where['val'])
                ->sum($field);
		
		
		return $users;
	}
	public static function getYearMemData($table, $where, $field)
	{ 
		
		$users = DB::table($table)
                ->whereYear($where['field'], $where['val'])
				->where("settlement", "Yes")
                ->sum($field);
		
		
		return $users;
	}
	public static function getYearEvnData($table, $where, $field)
	{ 
		
		$users = DB::table($table)
                ->whereYear($where['field'], $where['val'])
				->where("bookingFor","Rental")
                ->sum($field);
		
		
		return $users;
	}
	public static function getDayData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereDate($where['field'], $where['val'])
                ->sum($field);
		
		
		return $users;
	}
	public static function getDayEvnData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereDate($where['field'], $where['val'])
				->where("bookingFor","Rental")
                ->sum($field);
		
		
		return $users;
	}
	public static function getDayMemData($table, $where, $field)
	{
		
		$users = DB::table($table)
                ->whereDate($where['field'], $where['val'])
                ->where("settlement", "Yes")
                ->sum($field);
		
		
		return $users;
	}
	
	public static function insertData($table,$param)
	{
		
		$qur = DB::table($table)->insertGetId($param);
		
		return $qur;
	}
	public static function getPmtId($id){
		$users = DB::table('paymenttbl')
                ->where("bookingID", $id)
                ->select("id")
				->get();
	
		return $users[0]->id;
	}
	public static function insertDataWithID($table,$param)
	{
		 DB::table($table)->insertGetId([$param]);
		 dd(DB::getQueryLog());exit;
	}
	public static function lastRecord($table,$field)
	{
		return DB::table($table)->orderBy($field, 'desc')->first();
	}
	public static function selectQeury($que)
	{
		$users = DB::select($que);
		return $users;
	}
	public static function deleteRecord($table,$where)
	{
		//$users = DB::delete($table)->where($where);
		DB::table($table)->where($where)->delete();
		//return $users;
	}
	public static function updateData($table,$where,$data) 
	{
		
		 DB::table($table)->where($where)->update($data);
	}
	public static function numberTowords($num)
	{
		$ones = array( 
		0=>"",
		1 => "one", 
		2 => "two", 
		3 => "three", 
		4 => "four", 
		5 => "five", 
		6 => "six", 
		7 => "seven", 
		8 => "eight", 
		9 => "nine", 
		10 => "ten", 
		11 => "eleven", 
		12 => "twelve", 
		13 => "thirteen", 
		14 => "fourteen", 
		15 => "fifteen", 
		16 => "sixteen", 
		17 => "seventeen", 
		18 => "eighteen", 
		19 => "nineteen" 
		); 
		$tens = array( 
		1 => "ten",
		2 => "twenty", 
		3 => "thirty", 
		4 => "forty", 
		5 => "fifty", 
		6 => "sixty", 
		7 => "seventy", 
		8 => "eighty", 
		9 => "ninety" 
		); 
		$hundreds = array( 
		"hundred", 
		"thousand", 
		"million", 
		"billion", 
		"trillion", 
		"quadrillion" 
		); //limit t quadrillion 
		$num = number_format($num,2,".",","); 
		$num_arr = explode(".",$num); 
		 $wholenum = $num_arr[0]; 
		$decnum = $num_arr[1]; 
		$whole_arr = array_reverse(explode(",",$wholenum)); 
		krsort($whole_arr); 
		$rettxt = ""; 
		
		foreach($whole_arr as $key => $i){ 
		if($i < 20){ 
			if(isset($ones[$i])){
			$rettxt .= $ones[$i];
			}
		}elseif($i < 100){ 
		$rettxt .= $tens[substr($i,0,1)]; 
		$rettxt .= " ".$ones[substr($i,1,1)]; 
		}else{ 
		$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
			if(substr($i,1,1) >0){
				$rettxt .= " ".$tens[substr($i,1,1)];
			}			
				$rettxt .= " ".$ones[substr($i,2,1)];
			
		} 
		
		if($key > 0){ 
		$rettxt .= " ".$hundreds[$key]." "; 
		} 
		}
		if($decnum > 0){ 
		$rettxt .= " and "; 
		if($decnum < 20){ 
		$rettxt .= $ones[$decnum]; 
		}elseif($decnum < 100){ 
		$rettxt .= $tens[substr($decnum,0,1)]; 
		$rettxt .= " ".$ones[substr($decnum,1,1)]; 
		} 
		} 
		return $rettxt; 
			
	}
}
?>