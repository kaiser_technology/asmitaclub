$('document').ready(function() {
		// validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            srno: "required",
            propcode: "required",
            propname: "required",
            propadd: "required",
            proptype: "required",
            capacity: "required",
            minper: "required",
            maxper: "required",
            bentype: "required",
            benname: "required",
            
        },
        messages: {
            srno: "Please enter your Serial Number",
            propcode: "Please enter your Property Code",
            propname: "Please enter your Property Name",
            propadd: "Please enter your Address",
            proptype: "Please enter your Property Type",
            capacity: "Please enter your Capacity",
            minper: "Please enter min person",
            maxper: "Please enter max persion",
            bentype: "Please enter your BEN type",
            benname: "Please enter your BEN name",
        }
    }); 
	$("#packageForm").validate({
        rules: {
            srno: "required",
            pkgname: "required",
            pkgtype: "required",
            pkgcode: "required",
            noofper: "required",
            serv: "required"
        },
        messages: {
            srno: "Please enter your serial number",
            pkgname: "Please enter your packages Name",
            pkgtype: "Please Select your packages Type",
            pkgcode: "Please enter your package code",
            noofper: "Please Select No of person",
            serv: "Please select services",
        }
    });
	$("#vendorForm").validate({
        rules: {
            srno: "required",
            vencode: "required",
            venname: "required",
            venadd: "required",
            ventype: "required",
            venstatus: "required",
            number: {
                required: true,
                minlength: 10
            },
            pincode: "required",
            pan: "required",
            deposit: "required",
            validity: "required",
            regno: "required",
        },
        messages: {
            srno: "Please enter your serial number",
            vencode: "Please enter your Vendor Code",
            venname: "Please Select your Vendor Name",
            venadd: "Please enter your Vendor Address",
            ventype: "Please Select Vendor Type",
            venstatus: "Please select Status",
            number: "Please enter your 10 digit number",
            pincode: "Please enter pincode",
            pan: "Please enter PAN number",
            deposit: "Please enter deposit",
            validity: "Please select validity date",
            regno: "Please select registration Number",
        }
    });
	$("#pkgPriceForm").validate({
        rules: {
            pkgcode: "required",
            pkgname: "required",
            total: "required",
            tax: "required",
            basiccharge: "required",
            prccode: "required"
        },
        messages: {
            pkgcode: "Please enter your Package Code",
            pkgname: "Please Select your Package Name",
            total: "Please  enter your total price",
            tax: "Please enter your tax amount",
            basiccharge: "Please enter basic charge",
            prccode: "Please enter price code",
        }
    });
});