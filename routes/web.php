<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return View::make('pages.dashboard');
}); */
/* Route::group(['namespace' => 'App\Modules\Admin\Controllers','middleware'=>['web']], function () { */
	Route::get('/','LoginController@login');
	Route::get('/logout','LoginController@logout');
	Route::post('/authentication','LoginController@loginSubmit');
	Route::get('/dashboard','IndexController@index');
	Route::get('/masters/property/list','IndexController@propertyListing');
	Route::get('/masters/property/add','IndexController@propertyAdd');
	Route::get('/masters/property/edit/{id}','IndexController@propertyUpdate');
	Route::post('/masters/property/update','IndexController@propertyUpdateSubmit');
	Route::get('/masters/property/delete/{id}','IndexController@deleteRecord');
	Route::post('/masters/property/submit','IndexController@propertySubmit');
	Route::get('/masters/ajax/{table}/{id}','IndexController@ajaxCall');
	Route::get('/masters/price/list','IndexController@priceListing');
	Route::get('/masters/price/package/edit/{id}','IndexController@clubPriceEdit');
	Route::get('/masters/price/property/edit/{id}','IndexController@propertyPriceEdit');
	Route::get('/masters/price/add','IndexController@priceAdd');
	Route::get('/masters/price/pkgpricedelete/{id}','IndexController@pkgpriceDelete');
	Route::post('/masters/price/pkgpriceUpdate','IndexController@pkgpriceUpdate');
	Route::get('/masters/price/propricedelete/{id}','IndexController@propriceDelete');
	Route::post('/masters/price/propriceUpdate','IndexController@propriceUpdate');
	Route::post('/masters/price/pkgpricesubmite','IndexController@pkgpricesubmite');
	Route::post('/masters/price/propricesubmite','IndexController@propricesubmite');
	
	Route::get('/masters/package/list','IndexController@packageListing');
	Route::get('/masters/package/add','IndexController@packageAdd');
	Route::post('/masters/package/pkgsubmite','IndexController@pkgSubmite');
	Route::get('/masters/package/edit/{id}','IndexController@packageUpdate');
	Route::post('/masters/package/update','IndexController@packageUpdateSubmit');
	Route::get('/masters/package/delete/{id}','IndexController@packageRecordDel');
	
	Route::get('/masters/vendor/list','IndexController@vendorListing');
	Route::get('/masters/vendor/add','IndexController@vendorAdd');
	Route::post('/masters/vendor/vendorsubmite','IndexController@vendorsubmite');
	Route::get('/masters/vendor/edit/{id}','IndexController@vendorUpdate');
	Route::post('/masters/vendor/update','IndexController@vendorUpdateSubmit');
	Route::get('/masters/vendor/delete/{id}','IndexController@vendorRecordDel');
	
	Route::get('/enquiry/listing','IndexController@enquiryListing');
	Route::get('/enquiry/add','IndexController@enquiryAdd');
	Route::post('/enquiry/enquirysubmite','IndexController@enquirysubmite');
	Route::get('/enquiry/delete/{id}','IndexController@enquiryDelete');
	Route::get('/enquiry/edit/{id}','IndexController@enquiryUpdate');
	Route::post('/enquiry/update','IndexController@enquiryUpdateSubmit');
	
	Route::get('/club/registration/delete/{id}','IndexController@ragistrationDelete');
	Route::get('/club/registration','IndexController@ragistrationListing');
	Route::get('/club/registration/add','IndexController@ragistration');
	Route::get('/club/registration/edit/{id}','IndexController@ragistrationUpdate');
	Route::post('/club/registration/update','IndexController@ragistrationUpdateSubmit');
	Route::get('/club/dailyvisitor','IndexController@dailyVisitor');
	Route::get('/club/dailyvisitor/add','IndexController@dailyVisitorAdd');
	Route::get('/club/dailyvisitor/edit/{id}','IndexController@dailyVisitorUpdate');
	Route::get('/club/dailyvisitor/delete/{id}','IndexController@dvdeleteRecord');
	Route::post('/club/dailyVisitorUpdate','IndexController@dailyVisitorUpdateSubmit');
	Route::post('/club/dailyVisitorSubmit','IndexController@dailyVisitorSubmit');
	Route::post('/club/ragistrationSubmit','IndexController@ragistrationSubmit');
	Route::get('/club/payment','IndexController@clubPayment');
	Route::post('/club/payment/submit','IndexController@clubPaymentSubmit');
	Route::get('/club/payment/pay/{id}','IndexController@payment');
	Route::get('/club/payment/settlement/{id}','IndexController@paymentsettlement');
	
	Route::get('/event/search/','IndexController@search');
	Route::get('/event/booking/add','IndexController@bookingAdd');
	Route::get('/event/booking/list','IndexController@bookingListing');
	Route::get('/event/booking/delete/{id}','IndexController@bookdeleteRecord');
	Route::post('/event/booking/bookingsubmite','IndexController@bookingsubmite');
	Route::get('/event/bookingrentals/add','IndexController@rentalAdd');
	Route::get('/event/bookingrentals/list','IndexController@renitalListing');
	Route::get('/event/bookingrentals/delete/{id}','IndexController@renitaldeleteRecord');
	Route::post('/event/bookingrentals/rentalsubmite','IndexController@rentalsubmite');
	Route::post('/event/booking/pymntSubmit','IndexController@pymntSubmit');
	Route::get('/event/booking/checkdate/{fromdate}/{todate}','IndexController@evetnBookingAvaibilty');
	Route::get('/event/billing/add','IndexController@billingAdd');
	Route::get('/report/event','ReportController@event');
	
	Route::get('/report/event/bookingDetail/{id}','ReportController@eventDetail');
	Route::post('/report/event/booking/list','ReportController@eventBookingList');
	Route::get('/report/event/booking/list','ReportController@eventBookingList');
	Route::post('/report/event/receipt/list','ReportController@eventReceiptList');
	Route::get('/report/event/receipt/list','ReportController@eventReceiptList');
	Route::get('/report/event/rentreceipt/list','ReportController@rentreceipt');
	Route::get('/report/event/receipt/{id}','ReportController@eventReceipt');
	/*new*/
	Route::get('/report/event/receipt','ReportController@EventReceiptPage');
	Route::get('/report/event/bookingDetail','ReportController@EventBDPage');
	Route::get('/report/event/rentReceipt','ReportController@RentReceiptPage');
	/*new*/
	
	Route::get('/report/ajax/{for}/{id}','ReportController@reportAjax');
	Route::get('/ajax/getDate/{cdate}/{type}','IndexController@ajaxGetDate');
	Route::get('/ajax/{table}/{field}/{id}','IndexController@ajaxFieldCall');
	Route::get('/report/club','ReportController@club');
	
	Route::post('/report/club/member/list','ReportController@clubMemberList');
	Route::post('/report/club/price/list','ReportController@clubPricerList');
	Route::post('/report/club/receipt/list','ReportController@clubReceiptList');
	Route::get('/report/club/receipt/list','ReportController@clubReceiptList');
	Route::post('/report/club/dailyVisitor/list','ReportController@clubDVList');
	Route::get('/report/club/dailyVisitor/list','ReportController@clubDVList');
	Route::get('/report/club/receipt','ReportController@clubReceiptPage');
	Route::get('/report/club/member','ReportController@clubMemberPage');
	Route::get('/report/club/dailyVisitor','ReportController@clubDailyVisitorPage');
	Route::get('/report/club/receipt/{id}','ReportController@clubReceipt');
	Route::get('/report/club/dvreceipt/{id}','ReportController@clubDvReceipt');
	
	Route::get('/report/registrations','ReportController@clubRegistrations');
	Route::get('/report/payment-detial','ReportController@clubPayment');
	Route::get('/report/daily-visitor','ReportController@clubVisitor');
	
	
	
/* } */