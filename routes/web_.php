<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return View::make('pages.dashboard');
}); */
/* Route::group(['namespace' => 'App\Modules\Admin\Controllers','middleware'=>['web']], function () { */
	Route::get('/','LoginController@login');
	Route::get('/logout','LoginController@logout');
	Route::post('/authentication','LoginController@loginSubmit');
	Route::get('/dashboard','IndexController@index');
	Route::get('/masters/property/list','IndexController@propertyListing');
	Route::get('/masters/property/add','IndexController@propertyAdd');
	Route::get('/masters/ajax/{table}/{id}','IndexController@ajaxCall');
	Route::get('/masters/price/list','IndexController@priceListing');
	Route::get('/masters/price/add','IndexController@priceAdd');
	Route::post('/masters/price/pkgpricesubmite','IndexController@pkgpricesubmite');
	Route::post('/masters/price/propricesubmite','IndexController@propricesubmite');
	Route::get('/masters/package/list','IndexController@packageListing');
	Route::get('/masters/package/add','IndexController@packageAdd');
	Route::post('/masters/package/pkgsubmite','IndexController@pkgSubmite');
	Route::get('/masters/vendor/list','IndexController@vendorListing');
	Route::get('/masters/vendor/add','IndexController@vendorAdd');
	Route::post('/masters/vendor/vendorsubmite','IndexController@vendorsubmite');
	
	Route::get('/enquiry/listing','IndexController@enquiryListing');
	Route::get('/enquiry/add','IndexController@enquiryAdd');
	Route::post('/enquiry/enquirysubmite','IndexController@enquirysubmite');
	Route::get('/club/registration','IndexController@ragistrationListing');
	Route::get('/club/registration/add','IndexController@ragistration');
	Route::get('/club/dailyvisitor','IndexController@dailyVisitor');
	Route::get('/club/dailyvisitor/add','IndexController@dailyVisitorAdd');
	Route::post('/club/dailyVisitorSubmit','IndexController@dailyVisitorSubmit');
	Route::post('/club/ragistrationSubmit','IndexController@ragistrationSubmit');
	
	Route::get('/event/search/','IndexController@search');
	Route::get('/event/booking/add','IndexController@bookingAdd');
	Route::get('/event/booking/list','IndexController@bookingListing');
	Route::post('/event/booking/bookingsubmite','IndexController@bookingsubmite');
	Route::get('/event/bookingrentals/add','IndexController@rentalAdd');
	Route::get('/event/bookingrentals/list','IndexController@renitalListing');
	Route::post('/event/bookingrentals/rentalsubmite','IndexController@rentalsubmite');
	Route::post('/event/booking/pymntSubmit','IndexController@pymntSubmit');
	Route::get('/event/booking/checkdate/{fromdate}/{todate}','IndexController@evetnBookingAvaibilty');
	Route::get('/event/billing/add','IndexController@billingAdd');
	Route::get('/ajax/{table}/{field}/{id}','IndexController@ajaxFieldCall');
/* } */