<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
	<link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css"/>
     </head>
  <body>
 <style> 
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
.txtright{
	text-align:right
}

.txtcent{
	text-align:center
}
.header{
	border-top:2px solid;
	border-bottom:2px solid
}
</style>
    <header class="clearfix">
      <div id="logo" style="width:90px">
        <img src="{{RESOURCE_PATH}}images/logo.png">
      </div>
      <div id="company" style="">
        <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
        <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
        <div> BOOKING DETAILS</div>
        <div> CONTACT NO - 022 2811 8865</div>
         </div>
    </header>
    <main>
      <div id="details" class="clearfix">
		<table>
			<thead>
				<tr class="txtcent" style="border-top:2px solid">
					<th class="header">Sr.No.</th>
					<th class="header">Member Code</th>
					<th class="header">Member Type</th>
					<th class="header">Package Name</th>
					<th class="header">Name</th>
					<th class="header">Mobile</th>
					<th class="header">Age</th>
					<th class="header">Gender</th>
					<th class="header">Status</th>
					<th class="header">Valid From</th>
					<th class="header">Valid To</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $i=1;?>
				@foreach($listing as $val)
				<tr>
					<td>{{$i}}</td>
					<td>{{$val['mcode']}}</td>
					<td>{{$val['mtype']}}</td>
					<td>{{$val['pname']}}</td>
					<td>{{$val['name']}}</td>
					<td>{{$val['mobile']}}</td>
					<td>{{$val['age']}}</td>
					<td>{{$val['gender']}}</td>
					<td>{{$val['status']}}</td>
					<td>{{$val['validFrom']}}</td>
					<td>{{$val['validupto']}}</td>
				</tr>
				<?php $i++;?>	
				@endforeach
			</tbody>
		</table>
		
      </div>
	  
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>