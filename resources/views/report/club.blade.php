@extends('layouts.Listing')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid d-print-none">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Club Reports</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
           
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	

    </div>
    <!-- End container-fluid-->
    <h6 class="text-uppercase"></h6>
    <hr>
      <div class="row">
        <div class="col-lg-12">
           <div class="card">
              <div class="card-body"> 
         <div class="row">
          <div class="col-md-3 d-print-none">
                <ul class="nav nav-pills nav-pills-warning flex-column" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link py-4 active" data-toggle="pill" href="#piil-1"><i class="icon-home"></i> <span class="hidden-xs">Receipt</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link py-4" data-toggle="pill" href="#piil-2"><i class="icon-user"></i> <span class="hidden-xs">Member List</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link py-4" data-toggle="pill" href="#piil-3"><i class="icon-envelope-open"></i> <span class="hidden-xs">Price List</span></a>
                  </li>
                   <li class="nav-item">
                    <a class="nav-link py-4" data-toggle="pill" href="#piil-4"><i class="icon-envelope-open"></i> <span class="hidden-xs">Member ladger</span></a>
                  </li>
				  <li class="nav-item">
                    <a class="nav-link py-4" data-toggle="pill" href="#piil-5"><i class="icon-envelope-open"></i> <span class="hidden-xs">Receipt list</span></a>
                  </li>
				   <li class="nav-item">
                    <a class="nav-link py-4" data-toggle="pill" href="#piil-6"><i class="icon-envelope-open"></i> <span class="hidden-xs">Daily Visitor</span></a>
                  </li>
                </ul>
               </div>
         <div class="col-md-9">
                <!-- Tab panes -->
                <div class="tab-content">
                  <div id="piil-1" class="container tab-pane active">
                    <div class="row">
						<div class="col-lg-12">
						 
						   <div class="row">
								<div class="col-lg-12">
								  <div class="card">
									<div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
									<div class="card-body">
									  <div class="table-responsive">
									   <table id="example" class="table table-bordered d-print-none">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Receipt number</th>
												<th>Package Name</th>
												<th>Mobile Number</th>
												<th>Customer Name</th>
												<th>email</th>
												
												<th class="noExport">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1;?>
											@foreach($list as $val)
											<tr>
												<td>{{$i}}</td>
												<td><a href="<?php echo SITEPATH; ?>report/club/receipt/{{$val->pid}}" >{{$val->recieptno}}</a></td>
												<td>{{$val->Package_name}}</td>
												<td>{{$val->contactNumber}}</td>
												<td>{{$val->name}}</td>
												<td>{{$val->email}}</td>
												
												<td>
											
												<a href="<?php echo SITEPATH; ?>report/club/receipt/{{$val->pid}}" ><i aria-hidden="true" class="fa fa-eye" ></i> Vew Receipt</a>
												</td>
											</tr>
											<?php $i++;?>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
						<tr>
												<th>Sr. No.</th>
												<th>Recipt number</th>
												<th>Package Name</th>
												<th>Mobile Number</th>
												<th>Customer Name</th>
												<th>email</th>
												<th class="noExport">Action</th>
											</tr>
											</tr>
										</tfoot>
									</table>

									</div>
									</div>
								  </div>
								</div>
							  </div><!-- End Row-->
						</div>
					  </div><!--End Row-->
                  </div>
                  <div id="piil-2" class="container tab-pane fade">
						 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							   
							  <form id="dailyForm" action="{{ SITEPATH.'report/club/member/list' }}" method="post">
							  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group row">
								 <label for="input-1" class="col-sm-2 col-form-label">Select</label>
								  <div class="col-sm-2">
									<select class="form-control" id="input-1" name="action" required>
										<option value="member_type">Member Type</option>
										<option  value="Package_name">Package Name</option>
									</select>
								  </div>
								  
								<label for="input-3" class="col-sm-2 col-form-label">value</label> 
								  <div class="col-sm-2">
									 <input type="text" id="actionValue" class="form-control" name="value">
								  </div>
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker1"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								</div>
							</div>
							 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							</form>
						  </div>
						</div>
					  </div><!--End Row-->
                  </div>
                  <div id="piil-3" class="container tab-pane fade">
							 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							 <form id="dailyForm" action="{{ SITEPATH.'report/club/price/list' }}" method="post">
							  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								
								<div class="form-group row">
								 <label for="input-1" class="col-sm-2 col-form-label">Property</label>
								  <div class="col-sm-2">
									<select class="form-control" id="input-1" name="property" required>
										<option>All</option>
										@foreach($package as $val)
											<option value="{{ $val->pkg_name }}">{{ $val->pkg_name }}</option>
										@endforeach

									</select>
								  </div>
								  
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker2" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker3"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								  
								</div>
								 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							  </form>
							</div>
						  </div>
						</div>
					  </div><!--End Row-->
                  </div>
                  <div id="piil-4" class="container tab-pane fade">
						 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							  <form id="dailyForm" action="{{ SITEPATH.'report/event/booking/list' }}" method="post">
							  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								
								<div class="form-group row">
								 <label for="input-1" class="col-sm-2 col-form-label">Property</label>
								  <div class="col-sm-2">
									<select class="form-control" id="input-1" name="property" required>
										<option>All</option>
									@foreach($package as $val)
											<option value="{{ $val->pkg_name }}">{{ $val->pkg_name }}</option>
										@endforeach

									</select>
								  </div>
								  
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker4" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker5"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								  
								</div>
								 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							  </form>
							</div>
						  </div>
						</div>
					  </div><!--End Row-->
                  </div>
				  <div id="piil-5" class="container tab-pane fade">
						 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							    <form id="dailyForm" action="{{ SITEPATH.'report/club/receipt/list' }}" method="post">
							  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								
								<div class="form-group row">
								 <label for="input-1" class="col-sm-2 col-form-label">Property</label>
								  <div class="col-sm-2">
									<select class="form-control" id="input-1" name="property" required>
										<option>All</option>
										@foreach($package as $val)
											<option value="{{ $val->pkg_name }}">{{ $val->pkg_name }}</option>
										@endforeach

									</select>
								  </div>
								  
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker6" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker7"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								  
								</div>
								 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							  </form>
							</div>
						  </div>
						</div>
					  </div><!--End Row-->
                  </div>
				  <div id="piil-6" class="container tab-pane fade">
						 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							    <form id="dailyForm" action="{{ SITEPATH.'report/club/dailyVisitor/list' }}" method="post">
							  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								
								<div class="form-group row">
								  
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker8" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker9"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								  
								</div>
								 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							  </form>
							</div>
						  </div>
						</div>
					  </div><!--End Row-->
                  </div>
                 </div>
        </div>
        </div><!--End row-->
              </div>
           </div>
        </div>

       </div><!--End Row-->
<iframe name="upload_iframe" id="upload_iframe_id" style="width: 400px; height: 800px; display: none;"> </iframe>
    </div><!--End content-wrapper-->
	<link href="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <script src="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     <script>
    $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	 $('#autoclose-datepicker1').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker3').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker4').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker5').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker6').datepicker({
        autoclose: true,
        todayHighlight: true
      });$('#autoclose-datepicker7').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker8').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker9').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  
function viewMod(str){ 

		 $.ajax({
            url: "<?php echo SITEPATH; ?>report/ajax/eventRecept/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				ses = data[0].Session.split('|');
				per = data[0].period.split('|');
				date = data[0].createDate.split(' ');
				$('#recNo').html(data[0].recieptno);
                $('#custName').html(data[0].custName);
                $('#recFor').html(data[0].event);
                $('#proName').html(data[0].properyname);
                $('#amt').html(data[0].bookingAmount);
                $('#inword').html(data[0].amountInWord);
                $('#repPer').html(per[0]);
                $('#frdate').html(data[0].fromdate);
                $('#toDate').html(data[0].todate);
                $('#sess').html(ses[0]);
                $('#paidBy').html(data[0].paymentBy);
                $('#chNo').html(data[0].cheque_tran_no);
                $('#bnName').html(data[0].bankName);
                $('#dat').html(date[0]);
                $('#viewclick').click();
            }
        });
}

function eventDetial(str){ 
		//str = $('#actionValue').val();
		 $.ajax({
            url: "<?php echo SITEPATH; ?>report/ajax/memberList/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				var i = 1;
				$('#listdetail').html('');
				$.each(data,function(key,val){
					string="<tr><td>"+i+"</td><td>"+val.memborCode+"</td><td>"+val.contactNumber+"</td><td>"+val.name+"</td><td>"+val.Package_name+"</td><td><a href='<?php echo SITEPATH; ?>report/event/bookingDetail/"+val.id+"' id='detailclick'  ><i aria-hidden='true' class='fa fa-eye'> Vew Receipt</i></a></td></tr>";
					$('#listdetail').append(string);
					i++;
				});	
            }
        });
}
</script>
@stop