<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
	<link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css"/>
     </head>
  <body>
 <style> 
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
.txtright{
	text-align:right
}

.txtcent{
	text-align:center
}
.header{
	border-top:2px solid;
	border-bottom:2px solid
}
</style>
    <header class="clearfix">
      <div id="logo" style="width:90px">
        <img src="{{RESOURCE_PATH}}images/logo.png">
      </div>
      <div id="company" style="">
        <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
        <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
        <div> DAILY VISITOR LIST</div>
        <div> CONTACT NO - 022 2811 8865</div>
         </div>
    </header>
    <main>
      <div id="details" class="clearfix">
		<table>
			<thead>
				<tr class="txtcent" style="border-top:2px solid">
					<th class="header">Sr.No.</th>
					<th class="header">Member Code</th>
					<th class="header">Member Type</th>
					<th class="header">Period</th>
					<th class="header">Package Name</th>
					<th class="header">Person</th>
					<th class="header">Total Charges</th>
					<th class="header">CGST</th>
					<th class="header">SGST</th>
					<th class="header">contactnumber</th>
					<th class="header">Name</th>
					<th class="header">Rcpt no</th>
					<th class="header">Rcpt Date</th>
					<th class="header">Visitdate</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $i=1;
			$total=0;
			$tottax = 0;
			?>
				@foreach($listing as $val)
			<?php 
				$total= $total + $val['totalcharge'];
				$tottax = $tottax + $val['tax'];
			?>	
				<tr>
					<td>{{$i}}</td>
					<td>{{$val['memcode']}}</td>
					<td>{{$val['memtype']}}</td>
					<td>{{$val['period']}}</td>
					<td>{{$val['packageName']}}</td>
					<td>{{$val['person']}}</td>
					<td>{{money_format('%!i',$val['totalcharge'])}}</td>
					<td>{{ money_format('%!i',round($val['tax'] / 2))}}</td>
					<td>{{ money_format('%!i',round($val['tax'] / 2))}}</td>
					<td>{{$val['contactnumber']}}</td>
					<td>{{$val['name']}}</td>
					<td>{{$val['rcptno']}}</td>
					<td>{{date('d-m-Y',strtotime($val['createDate']))}}</td>
					<td>{{$val['visitdate']}}</td>
				</tr>
				<?php $i++;?>	
				@endforeach
			</tbody>
			<tfoot >
				<tr >
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header">{{money_format('%!i',$total)}}</th>
					<th class="header">{{money_format('%!i',round($tottax/2))}}</th>
					<th class="header">{{money_format('%!i',round($tottax/2))}}</th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
					<th class="header"></th>
				</tr>
			</tfoot>
		</table>
		
      </div>
	  
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>