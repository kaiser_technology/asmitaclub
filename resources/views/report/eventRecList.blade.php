<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
	<link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css"/>
     </head>
  <body>
 <style> 
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
.txtright{
	text-align:right
}

.txtcent{
	text-align:center
}
.header{
	border-top:2px solid;
	border-bottom:2px solid
}
</style>
    <header class="clearfix">
      <div id="logo" style="width:90px">
        <img src="{{RESOURCE_PATH}}images/logo.png">
      </div>
      <div id="company" style="">
        <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
        <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
        <div> EVENT RECIEPT DETAILS LIST</div>
        <div> CONTACT NO - 022 2811 8865</div>
         </div>
    </header>
    <main>
      <div id="" class="clearfix">
		<table>
			<thead>
				<tr class="txtcent" style="border-top:2px solid">
					<th class="header">Sr.No.</th>
					<th class="header">R.No.</th>
					<th class="header">R Date</th>
					<th class="header">Amount</th>
					<th class="header">Cus_Name</th>
					<th class="header">Mobile</th>
					<th class="header">Pro. Name</th>
					<th class="header">Pay Mode</th>
					<th class="header">Open Bal</th>
					<th class="header">Credit</th>
					<th class="header">Clos Bal</th>
					<th class="header">Tran Date</th>
					<th class="header">Dated</th>
					<th class="header">Bank Name</th>
					<th class="header">Fm Date</th>
					<th class="header">To Date</th>
				</tr>
			</thead>
			<tbody>
			<?php $i=1;
			$totalamt = 0;
			$openbal = 0;
			$closbal = 0;
			?>
			
				@foreach($listing as $val)
				
				<?php 
				$totalamt = $totalamt + $val['amt'];
				$openbal = $openbal + $val['openBal'];
				$closbal = $closbal + $val['closbal'];
				?>
				<tr>
					<td>{{$i}}</td>
					<td>{{$val['rno']}}</td>
					<td>{{$val['rdate']}}</td>
					<td class="txtright">{{number_format($val['amt'],2)}}</td>
					<td>{{$val['custName']}}</td>
					<td>{{$val['number']}}</td>
					<td>{{$val['propertyName']}}</td>
					<td>{{$val['payMode']}}</td>
					<td class="txtright">{{number_format($val['openBal'],2)}}</td>
					<td class="txtright">{{number_format($val['credit'],2)}}</td>
					<td class="txtright">{{number_format($val['closbal'],2)}}</td>
					<td >{{$val['trnDate']}}</td>
					<td >{{$val['dated']}}</td>
					<td>{{$val['bankName']}}</td>
					<td>{{$val['frdate']}}</td>
					<td>{{$val['todate']}}</td>
				</tr>
				<?php $i++;?>	
				@endforeach
				
			</tbody>
				<tfoot>
				<tr>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
					<th class="txtright header">{{number_format($totalamt,2)}}</th>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
					<th class="txtright header">{{number_format($openbal,2)}}</th>
					<th class="txtright header">{{number_format($totalamt,2)}}</th>
					<th class="txtright header">{{number_format($closbal,2)}}</th>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
					<td class="header"></td>
				</tr>
				</tfoot>
		</table>
		
      </div>
	  
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>