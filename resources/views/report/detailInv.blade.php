<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
	<link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css"/>
     </head>
  <body>
 
    <header class="clearfix">
      <div id="logo" style="width:90px">
        <img src="{{RESOURCE_PATH}}images/logo.png">
      </div>
      <div id="company" style="">
        <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
        <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
        <div> BOOKING DETAILS</div>
		 <div> CONTACT NO - 022 2811 8865</div>
         </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <table style="width:100%">
			<tr>
				<th >Customer Name</th>
				<td >{{$cname}}</td>
				<th >Email ID</th>
				<td >{{$email}}</td>
			</tr>
			<tr>
				<th >Address </th>
				<td >{{$add}}</td>
				<th >Event Name</th>
				<td >{{$event}}</td>
			</tr>
			<tr>
				<th >Contact No </th>
				<td >{{$contNum}}</td>
				<th >Pan Card No</th>
				<td >{{$pan}}</td>
			</tr>
			<tr>
				<th >Booking From </th>
				<td >{{$bfrom}}</td>
				<th >Booking Upto</th>
				<td >{{$bto}}</td>
			</tr>
			<tr>
				<th >Season </th>
				<td >{{$season}}</td>
				<th >Session Name</th>
				<td >{{$sessionname}}</td>
			</tr>
			<tr>
				<th >Property Name </th>
				<td >{{$proname}}</td>
				<th >Booking No</th>
				<td >{{$bookno}}</td>
			</tr>
			<tr>
				<th >Booking Code </th>
				<td >{{$bcode}}</td>
				<th >Booking Date</th>
				<td >{{$bookdate}}</td>
			</tr>
			
		</table>
		<br/>
		<br/>
      </div>
	   <div id="details" class="clearfix">
		<h3>CHARGE DETAILS</h3>
	   </div>
	   <div id="details" class="clearfix">
        <table style="width:100%">
			<tr>
				<th >Rent Charges</th>
				<td >{{$rentCharge}}</td>
				<th >Total Booking Amount</th>
				<td >{{$totalBookAmt}}</td>
			</tr>
			<tr>
				<th >Cleaning Charges</th>
				<td >{{$cleaningCharge}}</td>
				<th >Minimum Advance Paid</th>
				<td >{{$adv}}</td>
			</tr>
			<tr>
				<th >Additional Charges</th>
				<td >{{$additionalCharge}}</td>
				<th >Balance Amount</th>
				<td >{{$balAmt}}</td>
			</tr>
			<tr>
				<th >Electrical Charges</th>
				<td >{{$eleCharge}}</td>
				<th >Discount</th>
				<td >{{$discount}}</td>
			</tr>
			<tr>
				<th >Total Charges</th>
				<td >{{$totalCharge}}</td>
				<th >Last Date For Payment</th>
				<td >{{$lastPayment}}</td>
			</tr>
		</table>	
		</div>
		<div id="" class="clearfix">
		<h3>TERMS AND CONDITIONS</h3>
	   </div>	
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>