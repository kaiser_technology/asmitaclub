<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
	<link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css"/>
     </head>
  <body>
 <style> 
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
.txtright{
	text-align:right
}

.txtcent{
	text-align:center
}
.header{
	border-top:2px solid;
	border-bottom:2px solid
}
</style>
    <header class="clearfix">
      <div id="logo" style="width:90px">
        <img src="{{RESOURCE_PATH}}images/logo.png">
      </div>
      <div id="company" style="">
        <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
        <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
        <div> BOOKING DETAILS</div>
        <div> CONTACT NO - 022 2811 8865</div>
         </div>
    </header>
    <main>
      <div id="details" class="clearfix">
		<table>
			<thead>
				<tr class="txtcent" style="border-top:2px solid">
					<th class="header">Sr.No.</th>
					<th class="header">B.No.</th>
					<th class="header">Fr Date</th>
					<th class="header">To Date</th>
					<th class="header">Season</th>
					<th class="header">Session</th>
					<th class="header">Pro. Name</th>
					<th class="header">Bkg Date</th>
					<th class="header">Tot Amnt</th>
					<th class="header">Bkg Amnt</th>
					<th class="header">Advance</th>
					<th class="header">Discount</th>
					<th class="header">Balance</th>
					<th class="header">Due Date</th>
					<th class="header">Cus_Name</th>
					<th class="header">Mobile</th>
				</tr>
			</thead>
			<tbody>
			<?php $i=1;?>
				@foreach($listing as $val)
				<tr>
					<td>{{$i}}</td>
					<td>{{$val['bno']}}</td>
					<td>{{$val['frdate']}}</td>
					<td>{{$val['todate']}}</td>
					<td>{{$val['season']}}</td>
					<td>{{$val['session']}}</td>
					<td>{{$val['propertyName']}}</td>
					<td>{{$val['bkgdate']}}</td>
					<td class="txtright">{{$val['totamt']}}</td>
					<td class="txtright">{{$val['bookamt']}}</td>
					<td class="txtright">{{$val['adv']}}</td>
					<td class="txtright">{{$val['discount']}}</td>
					<td class="txtright">{{$val['balance']}}</td>
					<td>{{$val['duedate']}}</td>
					<td>{{$val['custName']}}</td>
					<td>{{$val['number']}}</td>
				</tr>
				<?php $i++;?>	
				@endforeach
			</tbody>
		</table>
		
      </div>
	  
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>