@extends('layouts.Listing')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid d-print-none">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Club Daily Visitor List</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
           
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	

    </div>
    <!-- End container-fluid-->
    <h6 class="text-uppercase"></h6>
    <hr>
      <div class="row">
        <div class="col-lg-12">
           <div class="card">
              <div class="card-body"> 
         <div class="row">
          
         <div class="col-md-12">
                <!-- Tab panes -->
                <div class="tab-content">
                  
                    <div class="row">
						<div class="col-lg-12">
							 <div class="row">
						<div class="col-lg-12">
						  <div class="card">
							<div class="card-body">
							   
							  <form id="dailyForm" action="" method="get">
							 <div class="form-group row">
								
								<label for="input-3" class="col-sm-2 col-form-label">From Date</label>
								  <div class="col-sm-2">
									 <input type="text" id="autoclose-datepicker" data-date-format="yyyy-mm-dd" class="form-control" name="from">
								  </div>
								<label for="input-3" class="col-sm-2 col-form-label">To Date</label>
								  <div class="col-sm-2">
									<input type="text" id="autoclose-datepicker1"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
								  </div>
								</div>
							</div>
							 <div class="form-footer">
                   
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Search</button>
                </div>
							</form>
						  </div>
						</div>
					  </div><!--End Row-->
						   <div class="row">
								<div class="col-lg-12">
								  <div class="card">
									<div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
									<div class="card-body">
									  <div class="table-responsive">
									   <table id="example" class="table table-bordered d-print-none">
									   <a class="btn btn-light buttons-pdf" href="{{SITEPATH}}report/club/dailyVisitor/list?1=1&<?php if(isset($_GET['from'])){ echo 'from='.$_GET['from']."&";} if(isset($_GET['to'])){ echo 'to='.$_GET['to']."&";}?>" style="float: left;"><span>PDF</span></a>
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Member Code</th>
												<th>Member Type</th>
												<th>Period</th>
												<th>Package Name</th>
												<th>Person</th>
												<th>Total Charges</th>
												<th>CGST</th>
												<th>SGST</th>
												<th>Customer Name</th>
												<th>Rcpt No</th>
												<th class="header">Rcpt Date</th>
												<th>Visit Date</th>
												
											</tr>
										</thead>
										<tbody>
											<?php $i=1;
										$total=0;
										$tottax = 0;
										?>
											@foreach($list as $val)
										<?php 
										
											$total= $total + $val->totalcharge;
											$tottax = $tottax + $val->tax;
										?>	
											<tr>
												
												<td>{{$i}}</td>
												<td>{{$val->memcode}}</td>
												<td>{{$val->memtype}}</td>
												<td>{{$val->period}}</td>
												<td>{{$val->packageName}}</td>
												<td>{{$val->person}}</td>
												<td>{{money_format('%!i',$val->totalcharge)}}</td>
												<td>{{money_format('%!i',round($val->tax / 2)) }}</td>
												<td>{{money_format('%!i',round($val->tax / 2)) }}</td>
												<td>{{$val->name}}</td>
												<td>{{$val->rcptno}}</td>
												<td>{{date('d-m-Y',strtotime($val->createdDate))}}</td>
												<td>{{$val->visitdate }}</td>
											</tr>
											<?php $i++;?>
											@endforeach
											<tr>
												<td>{{$i}}</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td>{{money_format('%!i',$total)}}</td>
												<td>{{money_format('%!i',round($tottax/2))}}</td>
												<td>{{money_format('%!i',round($tottax/2))}}</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												
											</tr>
										</tbody>
										
									</table>

									</div>
									</div>
								  </div>
								</div>
							  </div><!-- End Row-->
						</div>
					  </div><!--End Row-->
                 
                 </div>
        </div>
        </div><!--End row-->
              </div>
           </div>
        </div>

       </div><!--End Row-->
<iframe name="upload_iframe" id="upload_iframe_id" style="width: 400px; height: 800px; display: none;"> </iframe>
    </div><!--End content-wrapper-->
	<link href="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <script src="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     <script>
    $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	 $('#autoclose-datepicker1').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker3').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker4').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker5').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker6').datepicker({
        autoclose: true,
        todayHighlight: true
      });$('#autoclose-datepicker7').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker8').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $('#autoclose-datepicker9').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	  $(document).ready(function() {
	 $('.dt-buttons').append('<a class="btn btn-light buttons-pdf" href=""><span>PDF</span></a>'); 
	 });
function viewMod(str){ 

		 $.ajax({
            url: "<?php echo SITEPATH; ?>report/ajax/eventRecept/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				ses = data[0].Session.split('|');
				per = data[0].period.split('|');
				date = data[0].createDate.split(' ');
				$('#recNo').html(data[0].recieptno);
                $('#custName').html(data[0].custName);
                $('#recFor').html(data[0].event);
                $('#proName').html(data[0].properyname);
                $('#amt').html(data[0].bookingAmount);
                $('#inword').html(data[0].amountInWord);
                $('#repPer').html(per[0]);
                $('#frdate').html(data[0].fromdate);
                $('#toDate').html(data[0].todate);
                $('#sess').html(ses[0]);
                $('#paidBy').html(data[0].paymentBy);
                $('#chNo').html(data[0].cheque_tran_no);
                $('#bnName').html(data[0].bankName);
                $('#dat').html(date[0]);
                $('#viewclick').click();
            }
        });
}

function eventDetial(str){ 
		//str = $('#actionValue').val();
		 $.ajax({
            url: "<?php echo SITEPATH; ?>report/ajax/memberList/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				var i = 1;
				$('#listdetail').html('');
				$.each(data,function(key,val){
					string="<tr><td>"+i+"</td><td>"+val.memborCode+"</td><td>"+val.contactNumber+"</td><td>"+val.name+"</td><td>"+val.Package_name+"</td><td><a href='<?php echo SITEPATH; ?>report/event/bookingDetail/"+val.id+"' id='detailclick'  ><i aria-hidden='true' class='fa fa-eye'> Vew Receipt</i></a></td></tr>";
					$('#listdetail').append(string);
					i++;
				});	
            }
        });
}
</script>
@stop