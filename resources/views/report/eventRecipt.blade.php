<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Asmita Resorts - RECIEPT</title>
    <link href="{{RESOURCE_PATH}}css/pdfstyle.css" rel="stylesheet" type="text/css" /> </head>

<body style="font-size:11px">
    <header class="clearfix">
        <div id="logo" style="width:90px"> <img src="{{RESOURCE_PATH}}images/logo.png" width="80"> </div>
        <div id="company" style="">
            <h2 class="name">ASMITA RESORTS PVT. LTD</h2>
            <div>ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</div>
            <div> RECIEPT DETAILS</div>
			<div >FOR PROPERTY: {{$properyname}} </div>
            <div> CONTACT NO - 022 2811 8865</div>
        </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <table style="width:100%">
			<caption style="text-align:left;font-weight: bold;">Property Info</caption>
			<tr>
				<td style="text-align:left;">RECIEPT NO: </td><td>{{$recieptno}}</td>
				<td style="text-align:left">FROM DATE: </td><td>{{$fromdate}}</td>
				<td style="text-align:left">TO DATE: </td><td>{{$todate}}</td>
			</tr>
			<tr>
				<td style="text-align:left">PERIOD: </td><td>{{$period}}</td>
				<td style="text-align:left"></td><td></td>
				<td style="text-align:left">PROPERTY NAME: </td><td>{{$properyname}}</td>
			</tr>
		</table><br>
		 <table style="width:100%">
			<caption style="text-align:left;font-weight: bold;">Booking Info</caption>
			<tr>
				<td style="text-align:left">MOBILE NO: </td><td>{{$mobile}} </td>
				<td style="text-align:left">CUSTOMER NAME:</td><td>{{$custName}} </td>
				<td style="text-align:left"> CUSTOMER ADD:</td><td>{{$address}}</td>
			</tr>
			<tr>
				<td style="text-align:left">EMAIL ID:</td><td>{{$email}}</td>
				<td style="text-align:left">GENDER:</td><td>{{$gendar}}</td>
				<td style="text-align:left">PINCODE:</td><td>{{$pincode}}</td>
			</tr>
			<tr>
				<td style="text-align:left">EVENT:</td><td>{{$event}}</td>
				<td style="text-align:left">DOB:</td><td>{{$dob}}</td>
				<td style="text-align:left">AGE:</td><td>{{$age}}</td>
			</tr>
			
		</table><br>
		
		 <table style="width:100%">
			<caption style="text-align:left;font-weight: bold;">Payment Info</caption>
			<tr>
				<td style="text-align:left">RENT CHARGES: </td><td>{{$price_total}} </td>
				<td style="text-align:left">ADVANCE:</td><td>{{$advance}} </td>
				<td style="text-align:left" > BALANCE:</td><td colspan="2">{{$balance}}</td>
			</tr>
			<tr>
				<td style="text-align:left">BASIC:</td><td>{{$basic}}</td>
				<td style="text-align:left">CGST:</td><td>{{round($tax / 2)}}</td>
				<td style="text-align:left" >SGST:</td><td colspan="2">{{round($tax / 2)}}</td>
			</tr>
			@if($paymentBy == "Cash")
			<tr>
				<td style="text-align:left">PAYMENT:</td><td>{{$paymentBy}}</td>
				<td style="text-align:left"></td><td></td>
				<td style="text-align:left" ></td><td colspan="2"></td>
			</tr>
			@elseif($paymentBy == "Card")
				<tr>
					<td style="text-align:left">PAYMENT:</td><td>{{$paymentBy}}</td>
					<td style="text-align:left">BANK NAME:</td><td>{{$bankName}}</td>
					<td style="text-align:left" ></td><td colspan="2"></td>
				</tr>
			@else
				<tr>
					<td style="text-align:left">PAYMENT:</td><td>{{$paymentBy}}</td>
					<td style="text-align:left">CHQUE NO:</td><td>{{$cheque_tran_no}}</td>
					<td style="text-align:left">CHQUE DT:</td><td>{{$cheque_tran_date}}</td>
					<td style="text-align:left">BANK NAME:</td><td>{{$bankName}}</td>
				</tr>
			@endif
		</table><br>
		
		
		
      </div>
	 <div id="details" class="clearfix">
            <table style="width:100%;margin-bottom: 15px;">
                <tr>
                    <td style="width:15%;text-align:right;font-size:11px">Signature</td>
                    <td><img src="{{RESOURCE_PATH}}images/stamp.jpg" width="60"></td>
                    <td style="width:15%;text-align:right;font-size:11px">User Name:</td>
                    <td>ASMITA CLUB</td>
                </tr>
            </table>
            <div id="">
                <div style="font-size:11px">Reciept are subject to realisation of chque. The reciept is further subject to <b><a href="javascript:void(0)">TERM & CONDITION</a></b>.</div>
                <div class="notice" style="font-size:11px">Payment Once Made are not <b><a href="javascript:void(0)">REFUNDABLE & TRANSFERABLE</a></b>.</div>
            </div>
        </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html> 