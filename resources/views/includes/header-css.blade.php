 <!--favicon-->
  <link rel="icon" href="{{RESOURCE_PATH}}images/favicon.png" type="image/x-icon">
  <!-- Vector CSS -->
  <link href="{{RESOURCE_PATH}}plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
  <!-- simplebar CSS-->
  <link href="{{RESOURCE_PATH}}plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="{{RESOURCE_PATH}}css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="{{RESOURCE_PATH}}css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="{{RESOURCE_PATH}}css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="{{RESOURCE_PATH}}css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="{{RESOURCE_PATH}}css/app-style.css" rel="stylesheet"/> 