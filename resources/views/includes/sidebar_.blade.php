<!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="{{SITEPATH}}">
       <img src="{{RESOURCE_PATH}}images/logo-icon.png" class="logo-icon" alt="logo icon">
       <h5 class="logo-text">Asmita Club</h5>
     </a>
   </div>
   <div class="user-details">
    <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
      <div class="avatar"><img class="mr-3 side-user-img" src="{{RESOURCE_PATH}}images/avatars/avatar-13.png" alt="user avatar"></div>
       <div class="media-body">
       <h6 class="side-user-name">{{ Session::get('logindetail')['name']}}</h6>
      </div>
       </div>
     <div id="user-dropdown" class="collapse">
      <ul class="user-setting-menu">
            <li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>
			<li><a href="{{SITEPATH}}logout"><i class="icon-power"></i> Logout</a></li>
      </ul>
     </div>
      </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MAIN NAVIGATION</li>
		@foreach($menu as $key=>$val)
			<li>
				<a href="{{SITEPATH}}{{$val['main']->menu_link}}" class="waves-effect">
				  <i class="zmdi {{$val['main']->icon}}"></i> <span>{{$val['main']->menu_name}}</span>@if(isset($val['chield']))<i class="fa fa-angle-left pull-right"></i>@endif
				</a>
				@if(isset($val['chield']))
					<ul class="sidebar-submenu">
						@foreach($val['chield'] as $skey=>$sval)
							  <li><a href="{{SITEPATH}}{{$sval->menu_link}}"><i class="zmdi {{$sval->icon}}"></i> {{$sval->menu_name}}</a></li>
						@endforeach	  
					</ul>
				@endif	
			</li>
		@endforeach	
    </ul>
   
   </div>
   <!--End sidebar-wrapper-->