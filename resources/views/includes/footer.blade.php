<!-- Bootstrap core JavaScript-->
  <script src="{{RESOURCE_PATH}}js/jquery.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/popper.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/bootstrap.min.js"></script>
	
 <!-- simplebar js -->
  <script src="{{RESOURCE_PATH}}plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="{{RESOURCE_PATH}}js/sidebar-menu.js"></script>
  <!-- loader scripts -->
  <script src="{{RESOURCE_PATH}}js/jquery.loading-indicator.html"></script>
  <!-- Custom scripts -->
  <script src="{{RESOURCE_PATH}}js/app-script.js"></script>
  <!-- Chart js -->
  
  <script src="{{RESOURCE_PATH}}plugins/Chart.js/Chart.min.js"></script>
  <!-- Vector map JavaScript -->
  <script src="{{RESOURCE_PATH}}plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- Easy Pie Chart JS -->
  <script src="{{RESOURCE_PATH}}plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
  <!-- Sparkline JS -->
  <script src="{{RESOURCE_PATH}}plugins/sparkline-charts/jquery.sparkline.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/jquery-knob/excanvas.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/jquery-knob/jquery.knob.js"></script>
    
    <script>
        $(function() {
            $(".knob").knob();
        });
    </script>
  <!-- Index js -->
  <script src="{{RESOURCE_PATH}}js/index.js"></script>

  