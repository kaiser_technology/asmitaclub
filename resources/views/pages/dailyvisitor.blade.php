@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Daily Visitor</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Daily Visitor</li>
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
               <form id="dailyForm" action="{{ SITEPATH.'club/dailyVisitorSubmit' }}" method="post">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Visitor Detail
                </h4>
                <div class="form-group row">
                  <label for="input-1" class="col-sm-2 col-form-label">Member Code</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{$DLcode}}" readonly id="input-1" name="memCode">
                  </div>
                  <label for="input-2" class="col-sm-2 col-form-label">Mem Type</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-2" name="memtype" required>						<option>Swimming Guest</option>
												{{{--@foreach($msterType as $val)
						<option>{{ $val->name }}</option>
						@endforeach --}}}

					</select>
                  </div>
                </div>
               

                <div class="form-group row">
					<label for="input-3" class="col-sm-2 col-form-label">Period</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-3" name="session" required>
                                      <option>Hourly</option>															{{{--@foreach($period as $val)
                                        <option>{{ $val->name }}</option>
                                        @endforeach--}}}
                                    </select>
                  </div>	
                 
                  <label for="input-4" class="col-sm-2 col-form-label">Package Name</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-4" name="packageName" required>
						<option>Daily Visitor</option>													{{{--@foreach($package as $val)
							<option value="{{ $val->pkg_name }}">{{ $val->pkg_name }}</option>
						@endforeach--}}}
					</select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Person</label>
                  <div class="col-sm-4">
				  <input type="text" class="form-control" onchange="prceCal(this.value)" id="input-5" name="person">
                  </div>
				  <label for="input-6" class="col-sm-2 col-form-label">Total Charges</label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" readonly id="input-6" name="totcharge">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-7" class="col-sm-2 col-form-label">Tax</label>
                  <div class="col-sm-4">
                     <input type="text" class="form-control" readonly id="input-7" name="tax">
                  </div>
				  
                </div>
				<div class="form-group row">
                  <label for="input-9" class="col-sm-2 col-form-label">Contact No</label>
                  <div class="col-sm-4">
                     <input type="text" class="form-control" id="input-9" name="contno">
                  </div>
				  <label for="input-10" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" id="input-10" name="name">
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-11" class="col-sm-2 col-form-label">TRN ID</label>
                  <div class="col-sm-4">
                     <input  type="text" class="form-control" id="input-11" name="trnid">
                  </div>
				  <label for="input-12" class="col-sm-2 col-form-label">RCPT NO</label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" value="{{$DVRECcode}}"id="input-12" name="rcpt">
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-11" class="col-sm-2 col-form-label">Gender</label>
                  <div class="col-sm-4">
                      <select class="form-control" id="input-11" name="gender" required>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
					</select>
                  </div>
				  <label for="input-12" class="col-sm-2 col-form-label">Visit Date</label>
                  <div class="col-sm-4">
                   <input type="text" id="autoclose-datepicker" value="{{date('Y-m-d')}}" data-date-format="yyyy-mm-dd" class="form-control" name="visitdate">
                  </div>
                </div>
                <div class="form-footer">
                                       <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   
      <script>function prceCal(str){	var total = parseInt(str) * 200;		  var bcharge = parseInt(total)*100/118;	 var tax = parseInt(total) - parseInt(bcharge);	 $('#input-6').val(total);	 $('#input-7').val(tax);}</script>
@stop