@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Registration Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Registration Listing</li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}club/registration/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Registration</a>
       
       
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th>
                        <th>Period</th>
                        <th>Person</th>
                        <th>Contact Number</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                         <th>Pay Status</th>
                        <th>Price Code</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;
					$udata=array();
					?>
                    @foreach($list as $val)
					<?php 
					
					// if(in_array($val->memborCode,$udata)){ continue; }else{
					// 	$udata[] = $val->memborCode;
					// }
						?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->memborCode}}</td>
                        <td>{{$val->Package_name}}</td>
                        <td>{{$val->member_type}}</td>
                        <td>{{$val->period}}</td>
                        <td>{{$val->person}}</td>
                        <td>{{$val->contactNumber}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->email}}</td>
                        <td>{{$val->address.'- '.$val->pincode}}</td>
                        <td>{{$val->totalamount}}</td>
                        <td>{{$val->paystatus}}</td>
                        <td>{{$val->pricecode}}</td>

                        <td><!--a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a-->						
                        <a href="{{SITEPATH}}club/registration/edit/{{$val->id}}" id="eidtbuton"><i aria-hidden="true" class="fa fa-edit"></i></a-->
                        <a href="{{SITEPATH}}club/registration/delete/{{$val->id}}" id="delbuton" ><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                         <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th>
                        <th>Period</th>
                        <th>Person</th>
                        <th>Contact Number</th>
                        <th>Name</th>
						<th>Email</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                         <th>Pay Status</th>
                        <th>Price Code</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop