@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Price Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void(0)">Price Listing</a></li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}masters/price/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Price Creation</a>
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Club Price</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Price Code	</th>
                        <th>Package Name</th>
                        <th>Member Type</th>
                        <th>Period</th>
                        <th>Season</th>
                        <th>Basic Charge</th>
                        <th>Tax</th>
                        <th>Tax Amount</th>
                        <th>Total Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php $i = 1;?>
                    @foreach($list as $val)
					<tr>
                        <td>{{$i}}</td>
                        <td>{{$val->price_code}}</td>
                        <td>{{$val->pkg_name}}</td>
                        <td>{{$val->member_type}}</td>
                        <td>{{$val->period}}</td>
                        <td>{{$val->season}}</td>
                        <td>{{$val->basic_charge}}</td>
                        <td>{{$val->tax_cgst}}</td>
                        <td>{{$val->tax_amnt}}</td>
                        <td>{{$val->total_amnt}}</td>
                        <td><!--a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a-->
						<a href="{{SITEPATH}}masters/price/package/edit/{{$val->id}}" id="eidtbuton" ><i aria-hidden="true" class="fa fa-edit"></i></a>
						<a href="{{SITEPATH}}masters/price/pkgpricedelete/{{$val->id}}" id="delbuton"><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                  <?php $i++;?>
					@endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>Sr No.</th>
                        <th>Price Code	</th>
                        <th>Package Name</th>
                        <th>Member Type</th>
                        <th>Period</th>
                        <th>Season</th>
                        <th>Basic Charge</th>
                        <th>Tax</th>
                        <th>Tax Amount</th>
                        <th>Total Amount</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
		  <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Property Price</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Season</th>
                        <!--th>Start Date</th>
                        <th>End Date</th-->
                        <th>Time</th>
                        <th>Property Name</th>
                        <th>Period</th>
                        <th>Rent Price</th>
                        <th>Cleaning Price</th>
                        <th>Electrical Price</th>
                        <th>Other Pirce</th>
                        <th>Total Amount</th>
                        <th>Min</th>
                        <th>Max</th>
                        <th>Capacity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
					<?php $i = 1;?>
                    @foreach($prolist as $val)
					<tr>
                        <td>{{$i}}</td>
                        <td>{{$val->season}}</td>
                        <!--td>{{$val->startDate}}</td>
                        <td>{{$val->endDate}}</td-->
						<td>{{$val->time}}</td>
                        <td>{{$val->proName}}</td>
                        <td>{{$val->period}}</td>
                        <td>{{$val->rentTotalPrice}}</td>
                        <td>{{$val->cleaningTotalRate}}</td>
                        <td>{{$val->eleTotalRate}}</td>
                        <td>{{$val->othTotalRate}}</td>
                        <td>{{$val->totalAmount}}</td>
                        <td>{{$val->min}}</td>
                        <td>{{$val->max}}</td>
                        <td>{{$val->capacity}}</td>
                        <td><!--a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a-->
						<a href="{{ SITEPATH.'masters/price/property/edit/'.$val->id }}" id="eidtbuton" ><i aria-hidden="true" class="fa fa-edit"></i></a>
						<a href="{{SITEPATH}}masters/price/propricedelete/{{$val->id}}" id="delbuton"><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                  <?php $i++;?>
					@endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <tr>
                       <th>Sr No.</th>
                        <th>Season</th>
                        <!--th>Start Date</th>
                        <th>End Date</th-->
                        <th>Time</th>
                        <th>Property Name</th>
                        <th>Period</th>
                        <th>Rent Price</th>
                        <th>Cleaning Price</th>
                        <th>Electrical Price</th>
                        <th>Other Pirce</th>
                        <th>Total Amount</th>
                        <th>Min</th>
                        <th>Max</th>
                        <th>Capacity</th>
                        <th>Action</th>
                    </tr>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop