@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Daily Visitor Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Daily Visitor Listing</li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}club/dailyvisitor/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i>  Daily Visitor</a>
       
       
      </div>
     </div>
     </div>
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Today Daily Visitor</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th>
                        <th>Period</th>
                        <th>Person</th>
						<th>Total Amount</th>
						<th>Tax</th>
						<th>Contact Number</th>
                        <th>Name</th>
                        <th>TRN id</th>
                        <th>RCPT no</th>
                        <th>Gender</th>
                         <th>Visit Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;?>
                    @foreach($todaylist as $val)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->memcode}}</td>
                        <td>{{$val->packageName}}</td>
                        <td>{{$val->memtype}}</td>
                        <td>{{$val->period}}</td>
                        <td>{{$val->person}}</td>
                        <td>{{$val->totalcharge}}</td>
                        <td>{{$val->tax}}</td>
                        <td>{{$val->contactnumber}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->trnid}}</td>
                        <td>{{$val->rcptno}}</td>
                        <td>{{$val->gendar}}</td>
                        <td>{{$val->visitdate}}</td>

                        <td><a href="{{SITEPATH}}report/club/dvreceipt/{{$val->id}}" ><i aria-hidden="true" class="fa fa-eye"></i></a>
                           <a href="{{SITEPATH}}club/dailyvisitor/edit/{{$val->id}}" id="eidtbuton"><i aria-hidden="true" class="fa fa-edit"></i></a>                       
						   <a href="{{SITEPATH}}club/dailyvisitor/delete/{{$val->id}}" id="delbuton" ><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                          <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th>
                        <th>Period</th>
                        <th>Person</th>
						<th>Total Amount</th>
						<th>Tax</th>
						<th>Contact Number</th>
                        <th>Name</th>
                        <th>TRN id</th>
                        <th>RCPT no</th>
                        <th>Gender</th>
                         <th>Visit Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th> 
                        <th>Period</th>
                        <th>Person</th>
						<th>Total Amount</th>
						<th>Tax</th>
						<th>Contact Number</th>
                        <th>Name</th>
                        <th>TRN id</th>
                        <th>RCPT no</th>
                        <th>Gender</th>
                         <th>Visit Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;?>
                    @foreach($list as $val)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->memcode}}</td>
                        <td>{{$val->packageName}}</td>
                        <td>{{$val->memtype}}</td>
                        <td>{{$val->period}}</td>
                        <td>{{$val->person}}</td>
                        <td>{{$val->totalcharge}}</td>
                        <td>{{$val->tax}}</td>
                        <td>{{$val->contactnumber}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->trnid}}</td>
                        <td>{{$val->rcptno}}</td>
                        <td>{{$val->gendar}}</td>
                        <td>{{$val->visitdate}}</td>

                        <td><a href="{{SITEPATH}}report/club/dvreceipt/{{$val->id}}" ><i aria-hidden="true" class="fa fa-eye"></i></a>
                           <a href="{{SITEPATH}}club/dailyvisitor/edit/{{$val->id}}" id="eidtbuton"><i aria-hidden="true" class="fa fa-edit"></i></a>                       
						   <a href="{{SITEPATH}}club/dailyvisitor/delete/{{$val->id}}" id="delbuton" ><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                          <th>Sr. No.</th>
                        <th>Member Code </th>
                        <th>Package Name</th>
                        <th>Member Type </th>
                        <th>Period</th>
                        <th>Person</th>
						<th>Total Amount</th>
						<th>Tax</th>
						<th>Contact Number</th>
                        <th>Name</th>
                        <th>TRN id</th>
                        <th>RCPT no</th>
                        <th>Gender</th>
                         <th>Visit Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop