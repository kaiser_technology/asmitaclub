@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Enquiry</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Enquiry Add</li>
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="enqForm"  action="{{ SITEPATH.'enquiry/update' }}" method="post">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Add Enquiry
                </h4>
                <div class="form-group row">
                <label for="input-1" class="col-sm-2 col-form-label">Enquiry No</label>
                   <div class="col-sm-4">
                     <input type="text" class="form-control" value="{{$pro->id}}" id="input-1" name="enqno" readonly="">
                  </div>
                  <label for="input-2" class="col-sm-2 col-form-label">Enquiry For</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-2" name="enqfor" required>
                        <option value="" >Select</option>
                        <option <?php if($pro->enq_for == "Club"){ echo "selected"; }?>>Club</option>
                    </select>
                  </div>
                </div>
				
				 <div class="form-group row">
					<label for="input-3" class="col-sm-2 col-form-label">Contact No</label>
                   <div class="col-sm-4">
                     <input type="text" class="form-control" value="{{$pro->contact_no}}" id="input-3" name="number">
					 </div>
				
                 <label for="input-5" class="col-sm-2 col-form-label">Customer Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{$pro->contact_name}}" id="input-5" name="custname">
                  </div>
                </div>
				<div class="form-group row">
				<label for="input-9" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-4">
           <input type="text" class="form-control" id="input-9" value="{{$pro->email}}" name="email">
                  </div>
                  <label for="input-7" class="col-sm-2 col-form-label">Address</label>
                  <div class="col-sm-4">
                   <textarea class="form-control" name="custadd" value="{{$pro->address}}" rows="4" id="input-7"></textarea>
                  </div>
                </div>
				<div class="form-group row">
					<label for="input-8" class="col-sm-2 col-form-label">Pincode</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{$pro->pincode}}" id="input-8" name="pincode">
                  </div>
                  <label for="input-10" class="col-sm-2 col-form-label">Refer</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{$pro->refer}}" id="input-10" name="refer">
                  </div>
                </div>
              <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Enquiry Status</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-12" name="enqstatus" required>
                        <option value="" >Select</option>
                        <option <?php if($pro->status == "Active"){ echo "selected"; }?>>Active</option>
                    </select>
                  </div>
                  <label for="input-1." class="col-sm-2 col-form-label">Next Followup Date</label>
                  <div class="col-sm-4">
                    <input type="text" id="autoclose-datepicker" value="{{$pro->next_follow_date}}" class="form-control" data-date-format="yyyy-mm-dd" name="enqdate">
                  </div>
                </div>
				<div class="form-group row">
					
                  <label for="input-11" class="col-sm-2 col-form-label">Enquiry Detail</label>
                  <div class="col-sm-10">
				   
				   <textarea class="form-control" name="enqdetail" rows="4" id="input-11">{{$pro->enq_detail}}</textarea>
                  </div>
                </div>
                 <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Check Status
                </h4>

                <div class="form-group row">
					<label for="input-18" class="col-sm-2 col-form-label">From</label>
                  <div class="col-sm-2">
                    <input type="text" id="autoclose-datepicker1" data-date-format="yyyy-mm-dd" value="{{$pro->from}}" class="form-control" name="from">
                  </div>	
                 
                  <label for="input-19" class="col-sm-2 col-form-label">TO</label>
                  <div class="col-sm-2">
                  <input type="text" id="autoclose-datepicker2"  data-date-format="yyyy-mm-dd" value="{{$pro->to}}" class="form-control" name="to">
                  </div>
				  <label for="input-20" class="col-sm-2 col-form-label">Period</label>
                  <div class="col-sm-2">
                     <select class="form-control" id="input-20" name="period" required>
                         @foreach($season as $val)
                          <option <?php if($pro->period == $val->name){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
				 <div class="form-group row">
				 <label for="input-21" class="col-sm-2 col-form-label">Session</label>
                  <div class="col-sm-2">
                     <select class="form-control" id="input-21" name="session" required>
                       @foreach($season as $val)
							@if($val->name == "Season" || $val->name == "Off Season") @php continue; @endphp @endif
                          <option <?php if($pro->session == $val->name){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                  </div>
					<label for="input-22" class="col-sm-2 col-form-label">Property Name</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-22" name="proptypename" onchange="pDetail(this.value)" required>
                         @foreach($property as $val)
                          <option <?php if($pro->propertyName == $val->pro_name){ echo "selected"; }?> value="{{ $val->pro_name.'|'.$val->pro_code }}">{{ $val->pro_name }}</option>
                        @endforeach
                    </select>
                  </div>	
                 
                  <label for="input-23" class="col-sm-2 col-form-label">Person</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-23" class="form-control" value="{{$pro->persion }}" name="person">
                  </div>
				  
				  
				  
                </div>
                <div class="form-group row">
				 <label for="input-24" class="col-sm-2 col-form-label">Mex Person</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-24" class="form-control" value="{{$pro->mex }}" readonly name="maxperson">
                  </div>
					<label for="input-25" class="col-sm-2 col-form-label">Min Person</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-25" class="form-control" value="{{$pro->min }}" readonly name="minperson">
                  </div>
                 
                 
                  <!--div class="col-sm-4">
					<button type="submit" class="btn btn-success"><i class="fa fa-times"></i> Check</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> B. Details</button>
					<button type="button"  class="btn btn-success"><i class="fa fa-check-square-o"></i> P. Details</button>	
                  </div-->
				  
				  
				  
                </div>
                 <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  detail
                </h4>
                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Rental</label>
                  <div class="col-sm-4">
                     <input type="text" id="input-14" class="form-control" value="{{$pro->rental}}" readonly name="rental">
                    
                  </div>
                  <label for="input-15" class="col-sm-2 col-form-label">Electrical</label>
                  <div class="col-sm-4">
                    <input type="text" id="input-15" class="form-control" value="{{$pro->electrical}}" readonly name="electrical">
                  </div>
                </div>
                
                <div class="form-group row">
                  <label for="input-16" class="col-sm-2 col-form-label">Cleaning</label>
                  <div class="col-sm-4">
                     <input type="text" id="input-16" class="form-control" value="{{$pro->cleaning}}" readonly name="cleaning">
                    
                  </div>
                  <label for="input-17" class="col-sm-2 col-form-label">Other</label>
                  <div class="col-sm-4">
                    <input type="text" id="input-17" class="form-control" value="{{$pro->other}}" readonly name="other">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-18" class="col-sm-2 col-form-label">total</label>
                  <div class="col-sm-4">
                     <input type="text" id="input-18" class="form-control" value="{{$pro->total}}"  readonly name="total">
                    
                  </div>
                 
                </div>  
				
				<div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->
 
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <script>
function pDetail(str){ 
var value = str.split("|");
	
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/property_price_tbl/proName/"+value[0],
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				$('#input-14').val(data[0].rentTotalPrice);
                $('#input-15').val(data[0].eleTotalRate);
                $('#input-16').val(data[0].cleaningTotalRate);
                $('#input-17').val(data[0].othTotalRate);
                 $('#input-18').val(data[0].totalAmount);
                 $('#input-24').val(data[0].max);
                 $('#input-25').val(data[0].min);
            }
        });
}

function fper(str){ 
		//var str = $('#input-7').val();	
	
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/member_type/name/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				$('#input-8').val(data[0].person);
                
            }
        });
}
</script>
      
@stop