@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Search Detail</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
           
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
               <form id="dailyForm" action="{{ SITEPATH.'club/dailyVisitorSubmit' }}" method="post">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Visitor Detail
                </h4>
                <div class="form-group row">
                 <label for="input-1" class="col-sm-2 col-form-label">Search</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-1" name="search" required>
						
						<option>Event</option>
						<option>Rental</option>

					</select>
                  </div>
				  <label for="input-3" class="col-sm-2 col-form-label">Based On</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-3" name="basedon" >
                                      
						<option>Partyna</option>
						
					</select>
                  </div>
				<label for="input-3" class="col-sm-2 col-form-label">Value</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-3" name="value">
                  </div>				  
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   
      
@stop