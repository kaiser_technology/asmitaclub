@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Property Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Property Listing</li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}masters/property/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Property Creation</a>
       
       
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Property Code</th>
                        <th>Property Name</th>
                        <th>Property Type</th>
                        <th>Capacity</th>
                        <th>Min Person</th>
                        <th>Max Person</th>
                        <th>Ben Type</th>
                        <th>Ben Name</th>
                        <th class="noExport">Action</th>
                    </tr>
                </thead>
                <tbody>
					<?php $i = 1;?>
                    @foreach($list as $val)
                    <tr>
						<td>{{$i}}</td>
                        <td>{{$val->pro_code}}</td>
                        <td>{{$val->pro_name}}</td>
                        <td>{{$val->pro_type}}</td>
                        <td>{{$val->capacity}}</td>
                        <td>{{$val->min_per}}</td>
                        <td>{{$val->max_per}}</td>
                        <td>{{$val->ben_type}}</td>
                        <td>{{$val->ben_name}}</td>
                        <td>
						<i aria-hidden="true" class="fa fa-eye" onclick="viewMod({{$val->id}})" ></i>
						<a href="javascript:void(0)" id="viewclick" data-toggle="modal" data-target="#largesizemodal" ></a>
						<a href="{{ SITEPATH.'masters/property/edit/'.$val->id }}"  ><i aria-hidden="true"  class="fa fa-edit"></i></a>
						<a href="{{ SITEPATH.'masters/property/delete/'.$val->id }}"  id="delbuton"><i aria-hidden="true" class="fa fa-trash"></i></a>
						</td>
                    </tr>
					
		     
             
					
					
					<?php $i++;?>
					@endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Property Code</th>
                        <th>Property Name</th>
                        <th>Property Type</th>
                        <th>Capacity</th>
                        <th>Min Person</th>
                        <th>Max Person</th>
                        <th>Ben Type</th>
                        <th>Ben Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
			 <!-- Modal -->
                <div class="modal fade" id="largesizemodal">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Property</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" style="white-space: wrap;">
                       <form>
					   <input type="hidden" name="id" id ="id" value=""/>
                             <div class="form-group row">
								<div class="col-sm-6">	
								   <label for="input-1">Property Code</label>
								   <input type="text" class="form-control" id="input-1" disabled value="">
								</div> 
								<div class="col-sm-6">
								   <label for="input-2">Property Name</label>
								   <input type="text" class="form-control" id="input-2" disabled value="">
                             </div>		
                             </div>
                            
                             <div class="form-group row">
								<div class="col-sm-12">
								   <label for="input-3">Property Address</label>
								   <textarea disabled class="form-control" id="input-3"> </textarea>
								</div> 
										
                             </div>
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-4">Property Type</label>
								   <input type="text" class="form-control" id="input-4" disabled value="">
								</div>
								<div class="col-sm-6">
								   <label for="input-5">capacity</label>
								   <input type="text" class="form-control" id="input-5" disabled value="">
								</div>
															
                             </div>
							
							 
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-6">Min Person</label>
								   <input type="text" class="form-control" id="input-6" disabled value="">
								</div>	
								<div class="col-sm-6">
								   <label for="input-7">Max Person</label>
								   <input type="text" class="form-control" id="input-7" disabled value="">
								</div>
                             </div>
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-8">BEN Type</label>
								   <input type="text" disabled class="form-control" id="input-8" value="">
								</div>
								 <div class="col-sm-6">
								   <label for="input-9">BEN Name</label>
								   <input type="text" disabled class="form-control" id="input-9" value="">
								  
								</div>
                             </div>
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-inverse-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        
                      </div> </form>
                    </div>
                  </div>
                </div>
				 <!-- Modal -->
                <div class="modal fade" id="largesizemodalforedit">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Property</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" style="white-space: wrap;">
                       <form action="/" method="post">
					   <input type="hidden" name="id" id ="eid" value=""/>
                             <div class="form-group row">
								<div class="col-sm-6">	
								   <label for="input-11">Property Code</label>
								   <input type="text" class="form-control" id="input-11"  value="">
								</div> 
								<div class="col-sm-6">
								   <label for="input-12">Property Name</label>
								   <input type="text" class="form-control" id="input-12"  value="">
                             </div>		
                             </div>
                            
                             <div class="form-group row">
								<div class="col-sm-12">
								   <label for="input-13">Property Address</label>
								   <textarea  class="form-control" id="input-13"> </textarea>
								</div> 
										
                             </div>
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-14">Property Type</label>
								   <input type="text" class="form-control" id="input-14"  value="">
								</div>
								<div class="col-sm-6">
								   <label for="input-15">capacity</label>
								   <input type="text" class="form-control" id="input-15"  value="">
								</div>
															
                             </div>
							
							 
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-16">Min Person</label>
								   <input type="text" class="form-control" id="input-16"  value="">
								</div>	
								<div class="col-sm-6">
								   <label for="input-17">Max Person</label>
								   <input type="text" class="form-control" id="input-17"  value="">
								</div>
                             </div>
							 <div class="form-group row">
								<div class="col-sm-6">
								   <label for="input-8">BEN Type</label>
								   <input type="text"  class="form-control" id="input-18" value="">
								</div>
								 <div class="col-sm-6">
								   <label for="input-8">BEN Name</label>
								   <input type="text"  class="form-control" id="input-19" value="">
								  
								</div>
                             </div>
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-inverse-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
                      </div> </form>
                    </div>
                  </div>
                </div>
				
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
<script type="text/javascript">
// This is our actual script
function viewMod(str){ 
		 $.ajax({
            url: "http://asmitaclub.jackfruittech.com/masters/ajax/property/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				
				$('#id').val(data[0].id);
                $('#input-1').val(data[0].pro_code);
                $('#input-2').val(data[0].pro_name);
                $('#input-3').html(data[0].pro_address);
                $('#input-4').val(data[0].pro_type);
                $('#input-5').val(data[0].capacity);
                $('#input-6').val(data[0].min_per);
                $('#input-7').val(data[0].max_per);
                $('#input-8').val(data[0].ben_type);
                $('#input-9').val(data[0].ben_name);
                $('#viewclick').click();
            }
        });
}
function editMod(str){ 
		 $.ajax({
            url: "http://asmitaclub.jackfruittech.com/masters/ajax/property/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				
				$('#eid').val(data[0].id);
                $('#input-11').val(data[0].pro_code);
                $('#input-12').val(data[0].pro_name);
                $('#input-13').html(data[0].pro_address);
                $('#input-14').val(data[0].pro_type);
                $('#input-15').val(data[0].capacity);
                $('#input-16').val(data[0].min_per);
                $('#input-17').val(data[0].max_per);
                $('#input-18').val(data[0].ben_type);
                $('#input-19').val(data[0].ben_name);
                $('#editclick').click();
            }
        });
}

</script>
@stop