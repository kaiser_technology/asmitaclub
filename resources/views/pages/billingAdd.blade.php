@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Payment Recievable</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="vendorForm" method="post" enctype="multipart/form-data" action="{{ SITEPATH.'event/booking/pymntSubmit' }}" class="">
			   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Payement Info
                </h4>
                <div class="form-group row">
                  <label for="input-1" class="col-sm-2 col-form-label">Booking Number</label>
                  <div class="col-sm-2">
                    <input type="text" onchange="pDetail(this.value)"  class="form-control" id="input-1" name="bookingNo"  value="">
                  </div>
                 <label for="fromdate" class="col-sm-2 col-form-label">From</label>
                  <div class="col-sm-2">
                    <input type="text" id="autoclose-datepicker1" data-date-format="yyyy-mm-dd" class="form-control" name="from">
                  </div>	
                 
                  <label for="todate" class="col-sm-2 col-form-label">TO</label>
                  <div class="col-sm-2">
                  <input type="text" id="autoclose-datepicker2"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
                  </div>
				  
                </div>
				<!---- ROW 2 ----->
                <div class="form-group row">
					 
				   <label for="input-3" class="col-sm-2 col-form-label">Session</label>
                  <div class="col-sm-2">
                    <input type="text" id="input-3" class="form-control" name="session">
                  </div>
				  <label for="input-4" class="col-sm-2 col-form-label">Property Name</label>
                  <div class="col-sm-2">
                    <input type="text" id="input-4" class="form-control" name="propName">
                  </div>
				  <label for="input-5" class="col-sm-2 col-form-label">Status</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-5" class="form-control" name="status">
                  </div>
				</div>
				<!---- ROW 3 ----->
				<div class="form-group row">
					
					<label for="input-6" readonly class="col-sm-2 col-form-label">Booking Date</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-6" readonly class="form-control" name="bookingDate">
                  </div>
				  <label for="input-7" class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-2">
                  <input type="text" id="input-7" readonly class="form-control" name="">
                  </div>
				  <label for="input-8" readonly class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-2" id="status">
                 <input type="text" id="input-8" readonly class="form-control" name="">
                  </div>
				</div>
				<!---- ROW 4 ----->
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Payment Detail
                </h4>
				<!---- ROW 6 ----->
                <div class="form-group row">
				 <label for="input-31" class="col-sm-1 col-form-label">SR No</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-9" class="form-control" value="{{$pymid}}" name="srno">
                    
                  </div>
				  <label for="input-32" class="col-sm-1 col-form-label">Trn id</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-32" class="form-control" value="{{$trnid}}" name="trnid">
                    
                  </div>
                  <label for="input-33" class="col-sm-1 col-form-label">Bking Amnt</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-33" class="form-control" name="bkamt">
                  </div>
				  <label for="input-34" class="col-sm-1 col-form-label">Advance</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-34" class="form-control" name="advance">
                  </div>
				</div>
				 <div class="form-group row">
				 
				  <label for="input-35" class="col-sm-1 col-form-label">Discount</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-35" class="form-control" name="discount">
                    
                  </div>
                  <label for="input-36" class="col-sm-1 col-form-label">Open Balance</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-36" class="form-control" name="openAmount">
                  </div>
				   <label for="input-37" class="col-sm-1 col-form-label">Credit</label>
                  <div class="col-sm-2">
                    <input type="text" onchange="closebal(this.value)"  id="input-37" class="form-control" name="credit">
                  </div>
				   <label for="input-38" class="col-sm-1 col-form-label">Close Balance</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-38" class="form-control" name="closbal">
                  </div>
				</div>
				<div class="form-group row">
				<label for="input-39" class="col-sm-1 col-form-label">Payment</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-39" name="payment" required>
                        <option >Cash</option>
                        <option>Cheque</option>
                        <option>Other</option>
                    </select>
                  </div>
				
				<label for="input-40" class="col-sm-1 col-form-label">Cheque/DD No</label>
                  <div class="col-sm-2">
                     <input type="text"  id="input-40" class="form-control" name="cheno">
                  </div>
				<label for="input-41"  class="col-sm-1 col-form-label">Cheque Date</label>
                  <div class="col-sm-2">
                   <input type="text" id="autoclose-datepicker6"  data-date-format="yyyy-mm-dd" class="form-control" name="chedate">
                  </div>
				  <label for="input-42"  class="col-sm-1 col-form-label">Bank Name</label>
                  <div class="col-sm-2">
                     <input type="text" id="input-42" class="form-control" name="bankName">
                  </div>
				</div>
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Booking Info
                </h4>
				<!---- ROW 5 ----->
				<div class="form-group row">
					<label for="input-9" class="col-sm-2 col-form-label">Mobile No</label>
                   <div class="col-sm-2">
                     <input type="text" class="form-control" id="input-9" name="mobnum">
					 </div>
				
                 <label for="input-10" class="col-sm-2 col-form-label">Customer Name</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-10" name="custname">
                  </div>
				  <label for="input-11" class="col-sm-2 col-form-label">Customer Address</label>
                  <div class="col-sm-2">
                     <textarea class="form-control" name="custadd" rows="4" id="input-11"></textarea>
                  </div>
				</div>
				<!---- ROW 3 ----->
				<div class="form-group row">
					<label for="input-20" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-2">
					<input type="text" class="form-control" id="input-20" name="email">
                  </div>
					<label for="input-13" class="col-sm-2 col-form-label">Pincode</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-13" name="pincode">
                  </div>
				   <label for="input-14" class="col-sm-2 col-form-label">Event</label>
                  <div class="col-sm-2">
                   <input type="text" class="form-control" id="input-14" name="event">
                  </div>
				</div>
				<!---- ROW 5 ----->
                <div class="form-group row">
				 <label for="input-15" class="col-sm-2 col-form-label">DOB</label>
                  <div class="col-sm-2">
                    <input type="text" id="input-15"   value=""   class="form-control" name="dob">
                  </div>
				  <label for="input-18" class="col-sm-2 col-form-label">Gender</label>
                  <div class="col-sm-2">
					 
					  <input type="text" id="input-16"   value=""   class="form-control" name="gender">
                  </div>
				  <label for="input-19" class="col-sm-2 col-form-label">Pancard</label>
                  <div class="col-sm-2">
					<input type="text" class="form-control" id="input-17" name="pancard">
                  </div>
				</div>
				<!---- ROW 6 ----->
              
				
                <div class="form-footer">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <script>
function pDetail(str){ 
		
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/eventbooking/id/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				$('#autoclose-datepicker1').val(data[0].fromdate);
                $('#autoclose-datepicker2').val(data[0].todate);
                $('#input-3').val(data[0].Session);
                $('#input-4').val(data[0].properyname);
                $('#input-6').val(data[0].createDate);
                $('#input-9').val(data[0].mobile);
                $('#input-10').val(data[0].custName);
                $('#input-11').val(data[0].address);
                $('#input-13').val(data[0].pincode);
                $('#input-14').val(data[0].event);
                $('#input-15').val(data[0].dob);
                $('#input-16').val(data[0].gendar);
                $('#input-17').val(data[0].pan);
                $('#input-20').val(data[0].email);
                $('#input-33').val(data[0].price_total);
                $('#input-34').val(data[0].advance);
                $('#input-35').val(data[0].discount);
               
            }
        });
		pinfo(str);
}
function pinfo(str)
{
	$.ajax({
            url: "<?php echo SITEPATH;?>ajax/paymenttbl/bookingID/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
					var paidAmount = 0;
					var totalAmount = 0;
					var disAmount = 0;
					
					$.each(data, function(i, item) {
					paidAmount = paidAmount +  parseInt(data[i].bookingAmount);
						totalAmount = parseInt(data[i].bookingFinalAmount);
						disAmount = parseInt(data[i].bookingDiscount);
				});
					var openbal = totalAmount - paidAmount;
				
               
                $('#input-36').val(openbal);
                $('#input-38').val(openbal);
                $('#input-35').val(disAmount);
               
            }
        });
}
function closebal(str)
{
	var val = $('#input-36').val();
	$('#input-38').val(parseInt(val) - parseInt(str));
}
   </script>
      
@stop