@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2"> 
        <div class="col-sm-9">
		    <h4 class="page-title">Booking Detail</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Booking Add</li>
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="vendorForm" method="post" enctype="multipart/form-data" action="{{ SITEPATH.'club/payment/submit' }}" class="">
			   <input type="hidden" name="_token" value="{{ csrf_token() }}">
			   <input type="hidden" name="id" value="{{ $list[0]->id }}">			   <input type="hidden" name="settlement" value="{{ isset($_GET['settlement'])?$_GET['settlement']:''}}">
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Payment Detail
                </h4>
				<!---- ROW 6 ----->
                <div class="form-group row">
				 <label for="input-21" class="col-sm-2 col-form-label">Total Amount</label>
                  <div class="col-sm-4">
                     <input type="text" readonly id="Amount" class="form-control" value="{{$list[0]->balance}}" name="totalamount">
                    
                  </div>
				  <label for="input-23" class="col-sm-2 col-form-label">payble Amount</label>
                  <div class="col-sm-4">
                     <input type="text"  id="payble" class="form-control" name="payble">
                  </div>
                  
				</div>
				
               
				
				<!--- row 11 -->
				<div class="form-group row">
					<label for="input-35" class="col-sm-2 col-form-label">Payment</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-35" name="payment" required>
                        <option >Cash</option>
                        <option>Cheque</option>
                        <option>Card</option>
                    </select>
                  </div>
				
				<label for="input-36" class="col-sm-2 col-form-label">Cheque/DD No</label>
                  <div class="col-sm-4">
                     <input type="text"  id="input-36" class="form-control" name="cheno">
                  </div>
				</div>
			<div class="form-group row">
				<label for="input-37"  class="col-sm-2 col-form-label">Cheque Date</label>
                  <div class="col-sm-4">
                   <input type="text" id="autoclose-datepicker6"  data-date-format="yyyy-mm-dd" class="form-control" name="chedate">
                  </div>
				  <label for="input-38"  class="col-sm-2 col-form-label">Bank Name</label>
                  <div class="col-sm-4">
                     <input type="text" id="input-38" class="form-control" name="bankName">
                  </div>
			</div>
                <div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

      
@stop