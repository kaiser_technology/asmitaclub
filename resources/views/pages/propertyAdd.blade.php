@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Property</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Property Add</li>
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" method="post" action="{{ SITEPATH.'masters/property/submit' }}" name="propForm">
			   <input type="hidden" name="_token" value="{{ csrf_token() }}">	
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Property Master
                </h4>
                <div class="form-group row">
                  <label for="input-1" class="col-sm-2 col-form-label">Sr Number</label>
                  <div class="col-sm-4">
                    <input type="text" value="{{$propid}}" readonly class="form-control" id="input-1" name="srno">
                  </div>
                  <label for="input-2" class="col-sm-2 col-form-label">Property Code</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="input-2" name="propcode">
                  </div>
                </div>
               
                <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Property Info
                </h4>

                <div class="form-group row">
					<label for="input-3" class="col-sm-2 col-form-label">Property Name</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="input-3" name="propname">
                  </div>	
                 
                  <label for="input-4" class="col-sm-2 col-form-label">Property Address</label>
                  <div class="col-sm-4">
                   <textarea class="form-control" readonly name="propadd" rows="4" id="input-4">ASMITA RESORTS PVT. LTD.ASMITA'S CLUB, ASMITA ENCLAVE PHASE-I, MIRA ROAD (E). DIST. THANE-401 107</textarea>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">Property Type</label>
                  <div class="col-sm-4">
                     <select class="form-control" id="input-5" name="proptype" required>
                        <option value="" >Select</option>
                        <option>Open Ground</option>                        <option>Hall</option>                        <option>Lawn</option>                        <option>A/C Banquet Hall</option>
                    </select>
                  </div>
				  <label for="input-6" class="col-sm-2 col-form-label">Capacity</label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" id="input-6" name="capacity">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-7" class="col-sm-2 col-form-label">Min Person</label>
                  <div class="col-sm-4">
                     <input type="text" class="form-control" id="input-7" name="minper">
                  </div>
				  <label for="input-8" class="col-sm-2 col-form-label">Max Person</label>
                  <div class="col-sm-4">
                   <input type="text" class="form-control" id="input-8" name="maxper">
                  </div>
                </div>
				<div class="form-group row">
                  <label for="input-9" class="col-sm-2 col-form-label">BEN Type</label>
                  <div class="col-sm-4">
                     <select class="form-control" id="input-9" name="bentype" required>
                        <option value="" >Select</option>
                        <option>Trust Name</option>
                        <option>Asmita Resort Pvt Ltd</option>
                    </select>
                  </div>
				  <label for="input-10" class="col-sm-2 col-form-label">BEN Name</label>
                  <div class="col-sm-4">
                   <input type="text" value="Asmita Resort Pvt Ltd" readonly class="form-control" id="input-10" name="benname">
                  </div>
                </div>
				
                <div class="form-footer">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   
      
@stop