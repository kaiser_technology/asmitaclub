@extends('layouts.add') @section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Member Registration Details</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Registration</li>

                </ol>
            </div>

        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="regForm" action="{{ SITEPATH.'club/registration/update' }}" method="post">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Member Detail
                </h4>

                            <div class="form-group row">
                                <label for="input-1" class="col-sm-2 col-form-label">Member Sr No.</label>
                                <div class="col-sm-2">
                                    <input type="text" id="input-1" class="form-control" readonly value="{{$pro->id}}" name="srno">
                                </div>
								 <label for="input-7" class="col-sm-2 col-form-label">Package Name</label>
                                <div class="col-sm-2">
                                    <select class="form-control" onchange="gencode(this.value)" id="input-7" name="proptypename" required>
                                        @foreach($package as $val)
											<option data-code="" <?php if($val->pkg_name == $pro->Package_name){ echo "selected";}?> value="{{ $val->pkg_name }}|{{$val->pkg_code}}">{{ $val->pkg_name }}</option>
										@endforeach
                                    </select>
                                </div>
                                <label for="input-2" class="col-sm-2 col-form-label">Member Code</label>
                                <div class="col-sm-2">
                                    <input type="text" id="input-2" class="form-control" readonly value="{{$pro->memborCode}}" name="memcode">
									<input type="hidden" id="pknumber" value="{{$pro->memborCode}}" />
                                </div>
                               
                               
                            </div>
                            <div class="form-group row">
								<label for="input-4" class="col-sm-2 col-form-label">Member Type</label>
                                <div class="col-sm-2">
                                    <select class="form-control" onchange="fper(this.value)" id="input-4" name="memtype" required>
									
                                        @foreach($msterType as $val)
                                        <option <?php if($val->name == $pro->member_type){ echo "selected";}?> >{{ $val->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <label for="input-5" class="col-sm-2 col-form-label">Period</label>
                                <div class="col-sm-2">
                                    <select class="form-control" onchange="resetdate()" id="input-5" name="session" required>
                                       @foreach($period as $val)
                                        <option <?php if($val->name == $pro->period){ echo "selected";}?>>{{ $val->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                               <label for="input-8" class="col-sm-2 col-form-label">Person</label>
                                <div class="col-sm-2">
                                    <input type="text" value="1" id="input-8" class="form-control" value="{{$pro->person}}" readonly name="person">
                                </div>
                            </div>
							
                           
                            <div class="form-group row">
                                
                                <div class="col-sm-4 offset-6 ">
                                    
                                    <button type="button" onclick="pDetail()" class="btn btn-success"><i class="fa fa-check-square-o"></i> P. Details</button>
                                </div>

                            </div>
                            <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Persional Detail
                </h4>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-2 col-form-label">Contact Number</label>
                                <div class="col-sm-2">
                                    <input type="text"  value="{{$pro->contactNumber}}" class="form-control" id="input-10" name="number">
                                </div>
                                <label for="input-11" class="col-sm-2 col-form-label">Email Id</label>
                                <div class="col-sm-2">
                                   <input type="text"  value="{{$pro->email}}"  class="form-control" id="input-11" name="email">
                                </div>
								<label for="input-3" class="col-sm-2 col-form-label">Basic Charge</label>
                                <div class="col-sm-2">
                                    <input type="text"  value="{{$pro->basicCharge}}"  readonly id="input-3" class="form-control" name="basic_charge">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input-12" class="col-sm-2 col-form-label">Customer Name</label>
                                <div class="col-sm-2">
                                    <input type="text"  value="{{$pro->name}}" class="form-control" id="input-12" name="name">
                                </div>
                                
                               <label for="input-13" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-2">
                                    <textarea   class="form-control" name="custadd" rows="2"  id="input-13">{{$pro->address}}</textarea>
                                </div>
								<label for="input-6" class="col-sm-2 col-form-label">Tax (<span id="taxper">{{$pro->taxPer}}</span>%)</label>
                                <div class="col-sm-2">
								<input type="hidden" name="taxper" id="taxval" value="{{$pro->taxPer}}">
                                    <input type="text"  value="{{$pro->taxamount}}" readonly id="input-6" class="form-control" name="tax">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-14" class="col-sm-2 col-form-label">Pincode</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="input-14" value="{{$pro->pincode}}" name="pincode">
                                </div>
                                <label for="input-15" class="col-sm-2 col-form-label">DOB</label>
                                <div class="col-sm-2">
                                    <input type="text" id="autoclose-datepicker" value="{{$pro->dob}}" onchange="countAge(this.value)" data-date-format="yyyy-mm-dd" class="form-control" name="dob">
                                </div>
								<label for="input-9" class="col-sm-2 col-form-label">Total Amount</label>
                                <div class="col-sm-2">
                                    <input type="text" readonly id="input-9" value="{{$pro->totalamount}}" class="form-control" name="totalamount">
                                </div>
                            </div>
                            <div class="form-group row">
								<label for="input-16" class="col-sm-2 col-form-label">Age</label>
                                <div class="col-sm-2">
                                    <input type="text" readonly class="form-control" value="{{$pro->age}}" id="input-16" name="age">
                                </div>
							
							
                                <label for="input-16" class="col-sm-2 col-form-label">Gender</label>
                                <div class="col-sm-2">
                                   <select class="form-control" id="input-16"  name="gender" required>
                                        <option <?php if("Male" == $pro->gender){ echo "selected";}?>>Male</option>
                                        <option <?php if("Female" == $pro->gender){ echo "selected";}?>>Female</option>
                                    </select>
                                </div>
                                <label for="input-21" class="col-sm-2 col-form-label">Discount</label>
                                <div class="col-sm-2">
                                    <input type="text" onKeyUp="descountCal()" value="{{$pro->discount}}" class="form-control" id="input-21" name="discount">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-17" class="col-sm-2 col-form-label">MEM Statu</label>
                                <div class="col-sm-2">
                                     <select class="form-control" id="input-17"  name="memstatus" required>
                                        <option <?php if("Active" == $pro->status){ echo "selected";}?> >Active</option>
                                        <option <?php if("Not Active" == $pro->status){ echo "selected";}?>>Not Active</option>
                                    </select>
                                </div>
                                <label for="input-18" class="col-sm-2 col-form-label">Card Status</label>
                                <div class="col-sm-2">
                                     <select class="form-control" id="input-18" name="cardstatus" required>
                                        <option <?php if("Issued" == $pro->cardStatus){ echo "selected";}?>>Issued</option>
                                        <option <?php if("Not Issued" == $pro->cardStatus){ echo "selected";}?>>Not Issued</option>
                                    </select>
                                </div>
								 <label for="input-22" class="col-sm-2 col-form-label">Balance</label>
                                <div class="col-sm-2">
                                    <input type="text" readonly class="form-control"  value="{{$pro->balance}}" id="input-22" name="balance">
                                </div>
                            </div>
							<div class="form-group row">
								<label for="input-19" class="col-sm-2 col-form-label">From</label>
							  <div class="col-sm-2">
								<input type="text" id="autoclose-datepicker1"  value="{{$pro->fromdate}}" onchange="selectTodate(this.value)" data-date-format="yyyy-mm-dd" class="form-control" name="from">
							  </div>	
							 
							  <label for="input-20" class="col-sm-2 col-form-label">TO</label>
							  <div class="col-sm-2">
							  <input type="text" id="todate" value="" readonly value="{{$pro->todate}}" class="form-control" name="to">
							  </div>
							  <label for="input-23" class="col-sm-2 col-form-label">Pay Status</label>
                                <div class="col-sm-2">
                                   <select class="form-control" id="input-13" name="paystatus" required>
                                        <option <?php if("Not Paid" == $pro->paystatus){ echo "selected";}?>>Not Paid</option>
                                        <option <?php if("Paid" == $pro->paystatus){ echo "selected";}?>>Paid</option>
                                    </select>
                                </div>
							</div>
							<div class="form-group row">
								
							  
							  <label for="input-24" class="col-sm-2 offset-8 col-form-label">Price Code</label>
                                <div class="col-sm-2">
                                  <input type="text" readonly class="form-control" value="{{$pro->pricecode}}" id="input-24" name="pricecode">
                                </div>
							</div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<script>
function pDetail(){ 
		var str = $('#input-7').val();	
		var packgcode = str.split('|');
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/package_price_tbl/pkg_name/"+packgcode[0],
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				$('#eid').val(data[0].id);
                $('#input-3').val(data[0].basic_charge);
                $('#taxper').html(data[0].tax_per);
                $('#taxval').val(data[0].tax_per);
                $('#input-6').val(data[0].tax_amnt);
                $('#input-9').val(data[0].total_amnt);
                 $('#input-22').val(data[0].total_amnt);
                 $('#input-24').val(data[0].price_code);
            }
        });
}
function fper(str){ 
		//var str = $('#input-7').val();	
	
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/member_type/name/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				$('#input-8').val(data[0].person);
               
            }
        });
}
function descountCal(){ 

		var total = $('#input-9').val();
               var discount = $('#input-21').val();
				var finalamount = parseInt(total) - parseInt(discount);
				$('#input-22').val(finalamount);
		
}
function gencode(str)
{
	var packgcode = str.split('|');
	//alert(packgcode[1]);
	var code = $('#pknumber').val();
		$('#input-2').val(packgcode[1]+ ' - ' +code);
}
function countAge(dob)
{
	dob = new Date(dob);
	var today = new Date();
	var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
	$('#input-16').val(age);
}
function selectTodate(str)
{
	var per = $('#input-5').val();
	var CurrentDate = new Date();
	if(per == "Quarterly"){	var todate = addMonths(new Date(str), 4);	}
	if(per == "Halfyearly"){	var todate = addMonths(new Date(str), 7); }
	if(per == "Yearly"){ var todate = addMonths(new Date(str), 13);	}
	if(per == "21Days"){ var todate = addDays(new Date(str), 21);	}
	if(per == "Regular"){	var todate = addDays(new Date(str), 1); }
	if(per == "Daily Visitors"){	var todate = addDays(new Date(str), 1); }
	

	$('#todate').val(todate);
}

function daysInMonth(year, month)
{
    return new Date(year, month + 1, 0).getDate();
}

function addMonths(date, months)
{
    var target_month = date.getMonth() + months;
    var year = date.getFullYear() + parseInt(target_month / 12);
    var month = target_month % 12;
    var day = date.getDate();
    var last_day = daysInMonth(year, month);
    if (day > last_day)
    {
        day = last_day;
    }
    var new_date = year+"-"+month+"-"+day;
    return new_date;
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
	
	
	 var date = new Date(result),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-");
    return result;
}
function resetdate()
{
	$('#autoclose-datepicker1').val('');
	$('#todate').val('');
}
</script>
@stop