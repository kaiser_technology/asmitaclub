@extends('layouts.add')

@section('content')

	 <div class="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumb-->

     <div class="row pt-2 pb-2">

        <div class="col-sm-9">

		    <h4 class="page-title">Vendor</h4>

		    <ol class="breadcrumb">

            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>

            <li class="breadcrumb-item active" aria-current="page">Vendor Add</li>

           

         </ol>

	   </div>

	  

     </div>

    <!-- End Breadcrumb-->

	<div class="row">

        <div class="col-lg-12">

          <div class="card">

            <div class="card-body">

              <form id="vendorForm" method="post" enctype="multipart/form-data" action="{{ SITEPATH.'masters/vendor/update' }}" class="">

			   <input type="hidden" name="_token" value="{{ csrf_token() }}">
			   <input type="hidden" name="id" value="{{ $pro[0]->id }}">

                <h4 class="form-header text-uppercase">

                  <i class="fa fa-address-book-o"></i>

                   Vendor Master

                </h4>

                <div class="form-group row">

                  <label for="input-1" class="col-sm-2 col-form-label">Sr Number</label>

                  <div class="col-sm-4">

                    <input type="text" value="{{ $pro[0]->id }}" class="form-control" id="input-1" name="srno">

                  </div>

                  <label for="input-2" class="col-sm-2 col-form-label">Vendor Code</label>

                  <div class="col-sm-4">

                    <input type="text" value="{{ $pro[0]->vend_code  }}" class="form-control" id="input-2" name="vencode">

                  </div>

                </div>

               

                <h4 class="form-header text-uppercase">

                <i class="fa fa-envelope-o"></i>

                  Vendor Info

                </h4>



                <div class="form-group row">

					<label for="input-3" class="col-sm-2 col-form-label">Vendor Name</label>

                  <div class="col-sm-4">

                    <input type="text" value="{{ $pro[0]->vendor_name }}" class="form-control" id="input-3" name="venname">

                  </div>	

                 

                  <label for="input-4" class="col-sm-2 col-form-label">Vendor Address</label>

                  <div class="col-sm-4">

                   <textarea class="form-control"  name="venadd" rows="4" id="input-4">{{ $pro[0]->address  }}</textarea>

                  </div>

                </div>



                <div class="form-group row">

                  <label for="input-5" class="col-sm-2 col-form-label">Vendor Type</label>

                  <div class="col-sm-4">

                     <select class="form-control" id="input-5" name="ventype" required>

                        <option value="" >Select</option>

                        <option  <?php if($pro[0]->vend_type == "Contract"){ echo "selected";}?>>Contract</option>

                    </select>

                  </div>

				  <label for="input-6" class="col-sm-2 col-form-label">Vendor Status</label>

                  <div class="col-sm-4">

                   <select class="form-control" id="input-6" name="venstatus" required>

                        <option value="" >Select</option>

                        <option <?php if($pro[0]->vend_status == "Active"){ echo "selected";}?>>Active</option>

                        <option <?php if($pro[0]->vend_status == "Deactive"){ echo "selected";}?>>Deactive</option>

                    </select>

                  </div>

                </div>

                <div class="form-group row">

                  <label for="input-7" class="col-sm-2 col-form-label">Contact No</label>

                  <div class="col-sm-2">

                     <input type="text" value="{{ $pro[0]->contactnumber1  }}" class="form-control" id="input-7" name="number">

					 </div>

				<div class="col-sm-2">		

                     <input type="text" value="{{ $pro[0]->contactnumber2  }}" class="form-control" id="input-8" name="number1">

                  </div>

				  <label for="input-9" class="col-sm-2 col-form-label">Pincode</label>

                  <div class="col-sm-4">

                   <input type="text" value="{{ $pro[0]->pincode  }}" class="form-control" id="input-9" name="pincode">

                  </div>

                </div>

				<div class="form-group row">

                  <label for="input-10" class="col-sm-2 col-form-label">PAN Number</label>

                  <div class="col-sm-4">

                     <input type="text" value="{{ $pro[0]->pancard  }}" class="form-control" id="input-10" name="pan">

                  </div>

				  <label for="input-11" class="col-sm-2 col-form-label">Deposit</label>

                  <div class="col-sm-4">

                   <input type="text" value="{{ $pro[0]->diposit  }}" class="form-control" id="input-11" name="deposit">

                  </div>

                </div>

				<div class="form-group row">

                  <label for="input-12" class="col-sm-2 col-form-label">Validity</label>

                  <div class="col-sm-4">

                    

					  <input type="text" value="{{ $pro[0]->validity  }}" data-date-format="yyyy-mm-dd" id="autoclose-datepicker" class="form-control" name="validity">

                  </div>

				  <label for="input-13" class="col-sm-2 col-form-label">Regd No.</label>

                  <div class="col-sm-4">

                   <input type="text" value="{{ $pro[0]->regd_no  }}" class="form-control" id="input-13" name="regno">

                  </div>

                </div>

				<div class="form-group row">

                  <label for="input-14" class="col-sm-2 col-form-label">Account Number</label>

                  <div class="col-sm-4">

                    

					  <input type="text" value="{{ $pro[0]->Acc_no   }}" id="input-14" class="form-control" name="accno">

                  </div>

				  <label for="input-15"  class="col-sm-2 col-form-label">IFSC Code</label>

                  <div class="col-sm-4">

                   <input type="text" value="{{ $pro[0]->ifsc_code  }}" class="form-control" id="input-15" name="ifsccode">

                  </div>

                </div>

				<div class="form-group row">

                  <label for="input-16" class="col-sm-2 col-form-label">Beneficiary Name</label>

                  <div class="col-sm-4">

                    

					  <input type="text" id="input-16" value="{{ $pro[0]-> Bank_name   }}" class="form-control" name="benefname">

                  </div>

				  <label for="input-17" class="col-sm-2 col-form-label">Bank Name</label>

                  <div class="col-sm-4">

                   <input type="text" value="{{ $pro[0]-> Bank_name }}" class="form-control" id="input-17" name="bnkname">

                  </div>

                </div>

				

				<div class="form-group row">

                

				  <label for="input-19" class="col-sm-2 col-form-label">Services Provided / Royalty%</label>

					<div class="col-sm-4">

						<div class="row">

							<div class="icheck-material-white col-sm-4">

								<input type="checkbox" <?php if($pro[0]->photography != ""){ echo "checked";}?> id="user-checkbox1" name="serv[]"/>

								<label for="user-checkbox1">Photography </label>

							</div>

							<input type="text" value="{{$pro[0]->photography}}" class="form-control col-sm-4" id="input-20" name="phoroy">

						</div><br>

						<div class="row">

							<div class="icheck-material-white col-sm-4">

								<input type="checkbox" <?php if($pro[0]->music != ""){ echo "checked";}?> id="user-checkbox2" name="serv[]"/>

								<label for="user-checkbox2">Music </label>

							</div>

							<input type="text" value="{{$pro[0]->music}}" class="form-control col-sm-4" id="input-21" name="musroy">

						</div><br>

						<div class="row">

							<div class="icheck-material-white col-sm-4">

								<input type="checkbox" <?php if($pro[0]->catering != ""){ echo "checked";}?> id="user-checkbox3" name="serv[]"/>

								<label for="user-checkbox3">Catering </label>

							</div>

							<input type="text" value="{{$pro[0]->catering}}" class="form-control col-sm-4" id="input-22" name="catroy">

						</div><br>

						<div class="row">

							<div class="icheck-material-white col-sm-4">

								<input type="checkbox" <?php if($pro[0]->Decoration != ""){ echo "checked";}?> id="user-checkbox4" name="serv[]"/>

								<label for="user-checkbox4">Decoration </label>

							</div>

							<input type="text" value="{{$pro[0]->Decoration}}" class="form-control col-sm-4" id="input-23" name="decroy">

						</div>

					</div>

					

                </div>

                <div class="form-footer">

                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>

                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>

                </div>

              </form>

            </div>

          </div>

        </div>

      </div><!--End Row-->



    </div>

    <!-- End container-fluid-->

    

    </div><!--End content-wrapper-->

   

      

@stop