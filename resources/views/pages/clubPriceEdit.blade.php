@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Price</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Price Add</li>
            
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="pkgPriceForm" method="post" action="{{ SITEPATH.'masters/price/pkgpriceUpdate' }}" name="pkgPriceForm" >
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Price Master
                </h4>
                <div class="form-group row">
                 
                  <div class="col-sm-3">
                     <select class="form-control" onchange="showsec(this.value)" id="input-1" name="type" required>
                        <option>Club</option>
                    </select>
                  </div>				  
                  <div class="col-sm-3">
                     <select class="form-control" onchange="chngDate(this.value)"  id="input-2" name="season" required>
                        <option value="">Select</option>
                        @foreach($season as $val)
							<option class="{{$val->seasonFor}}" <?php if($val->name == $prop[0]->season){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
						@endforeach
                        
                    </select>
                  </div>
				   <div class="col-sm-3 club">
                    <input type="text" id="frm" placeholder="Start Date" value="{{$prop[0]->start_date}}" class="form-control" name="cfrom">
					  <!--input type="text" id="autoclose-datepicker" placeholder="Start Date" data-date-format="yyyy-mm-dd" class="form-control" name="from"-->
                  </div>
				   <div class="col-sm-3 club">
                    <input type="text" id="tod" class="form-control" value="{{$prop[0]->end_date}}" placeholder="End Date" name="cto">
					  <!--input type="text" id="autoclose-datepicker1" class="form-control" data-date-format="yyyy-mm-dd" placeholder="End Date" name="to"-->
                  </div>
                </div>
			<div id="packg"  >
			  <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Package Info
                </h4>

                <div class="form-group row">
					 <label for="input-9" class="col-sm-2 col-form-label">SR No</label>
                  <div class="col-sm-4">
                     <input type="text" value="{{$prop[0]->id}}" readonly class="form-control" id="input-9" name="srNo">
                  </div>
				
					<label for="input-3" class="col-sm-2 col-form-label">Package Name</label>
                  <div class="col-sm-4">
                     <select class="form-control" id="input-3" name="pkgname" required>
                        @foreach($package as $val)
							<option <?php if($val->pkg_name == $prop[0]->pkg_name){ echo "selected"; }?>  value="{{ $val->pkg_name.'|'.$val->pkg_code }}">{{ $val->pkg_name }}</option>
						@endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="input-5" class="col-sm-2 col-form-label">MEM Type</label>
                  <div class="col-sm-4">
                     <select class="form-control" id="input-5" name="memtype" required>
                      @foreach($msterType as $val)
							<option <?php if($val->name == $prop[0]->member_type){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
						@endforeach
                    </select>
                  </div>
				  <label for="input-6" class="col-sm-2 col-form-label">Period</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="input-6" name="period"  required>
                        @foreach($period as $val)
							<option <?php if($val->name == $prop[0]->period){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
						@endforeach
                    </select>
                  </div>
                </div>
				<div class="form-group row">
					 <label for="input-7"  class="col-sm-2 col-form-label">Total</label>
                  <div class="col-sm-4">
                     <input type="text" onfocusout="myFunction()" value="{{$prop[0]->total_amnt}}" class="form-control" id="input-7" name="total">
                  </div>
				
                   <label for="input-10" class="col-sm-2 col-form-label">Basic Charge</label>
                  <div class="col-sm-4">
                   <input type="text" readonly class="form-control" value="{{$prop[0]->basic_charge}}"  id="input-10" name="basiccharge">
                  </div>
				  
				 
                </div>
                <div class="form-group row">
                 
				  <label for="input-8"  class="col-sm-2 col-form-label">Tax%</label>
				  <label for="input-8"  class="col-sm-1 col-form-label">SGST%</label>
                  <div class="col-sm-1">
                   <input type="text" class="form-control" value="{{$prop[0]->tax_sgst}}"  readonly id="input-8" name="tax_sgst">
                  </div>
				  
				   <label for="input-8"  class="col-sm-1 col-form-label">CGST%</label>
                  <div class="col-sm-1">
                   <input type="text"  class="form-control" value="{{$prop[0]->tax_cgst}}"  readonly id="input-8a" name="tax_cgst">
                  </div>
				    <label for="input-9"  class="col-sm-2 col-form-label">Tax Amount</label>
                  <div class="col-sm-4">
                     <input readonly type="text" value="{{$prop[0]->tax_amnt}}" readonly class="form-control" id="input-9" name="taxamt">
                  </div>
                </div>
				
			</div>
			
			<div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <script>
   function showsec(val){
	   if(val=="Club"){
		 $('.other').hide();  
		 $('.club').show();
		$('#packg').show();
		$('#prop').hide();
		var url = '<?php echo SITEPATH ?>masters/price/pkgpricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
	   if(val=="Property"){
		$('.club').hide();
		 $('.other').show();  
		$('#prop').show();
		$('#packg').hide();
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
	   if(val=="Rental"){
		   $('.club').hide();
		    $('.other').show();  
		$('#prop').show();
		$('#packg').hide();
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
   }
   function myFunction(){
	 var total =  $('input:text[name=total]').val();
	 var bcharge = parseInt(total)*100/118;
	  $('input:text[name=basiccharge]').val(Math.round(bcharge));
	 var tax = 18;
	 var taxamt = Math.round(parseInt(total) - parseInt(bcharge));
	 $('input:text[name=taxamt]').val(taxamt);
	 //amit changes
	 var tax_cgst = parseInt(taxamt)/2;
	 $('input:text[name=tax_cgst]').val(tax_cgst);
	 var tax_sgst = parseInt(taxamt)/2;
	 $('input:text[name=tax_sgst]').val(tax_sgst);
	 //amit changes done
	 

   }
   function getpriceCode(){
	  
	 var seasons =  $('#input-2 :selected').val();
	 var spsea = seasons.split("|");
	  var pkgname =  $('#input-3 :selected').val();
	 var sppkg = pkgname.split("|");
	 var memtype =  $('#input-5 :selected').val();
	 var spmem = memtype.split("|");
	 var period =  $('#input-15 :selected').val();
	 var spper = period.split("|");
	 var code = spsea[1]+''+sppkg[1]+''+spmem[1]+''+spper[1];
	 $('input:text[name=prccode]').val(code);
   }
   function pDetail(str){ 
   getpriceCode();
	$.ajax({
		url: "<?php echo SITEPATH;?>ajax/property/pro_name/"+str,
		type: 'GET',
		dataType: 'json',
		success: function (data) {
			$('#input-13').val(data[0].pro_code);
			$('#input-17').val(data[0].min_per);
			$('#input-18').val(data[0].max_per);
			$('#input-16').val(data[0].capacity);
			
		}
	});
	}
	function propPrice(str1,str2,str3)
	{
	 var total =  $('input:text[name=totalAmount]').val();
	 var bcharge =  $('input:text[name='+str1+']').val();
	 var tax =  $('input:text[name='+str2+']').val();
	 var taxamt = Math.round((parseInt(bcharge) * parseInt(tax)) / 100);
	 var totamt = parseInt(bcharge) + taxamt;
	 var totalAmount = parseInt(total) + totamt;
	 
	 $('input:text[name='+str3+']').val(totamt);
	 $('input:text[name=totalAmount]').val(totalAmount);
	}
	function changeURL()
	{
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('href',url);
	}
	function chngDate(str){
		//if(str == "Season|SEA"){
		//	var year = new Date().getFullYear();
		//	 var preyear= new Date().getFullYear()-1;
		//	var sdate = "1-10-" + preyear;
		//	var edate = "30-06-" + year;
		//	$('#tod').val(edate);
		//	$('#frm').val(sdate);
			
		//}else{
		//	var year = new Date().getFullYear();
		//	 var preyear= new Date().getFullYear()-1;
		//	var sdate = "1-7-" + year;
		//	var edate = "30-9-" + year;
		//	$('#tod').val(edate);
		//	$('#frm').val(sdate);
		//}
		
		//amit changed
		if(str == "Season|SEA"){
			var year = new Date().getFullYear();
			 var preyear= new Date().getFullYear()-1;
			var sdate = preyear + "-10-01";
			var edate = year + "-06-30";
			$('#tod').val(edate);
			$('#frm').val(sdate);
			
		}else if(str == "Off Season|OSE"){
			var year = new Date().getFullYear();
			 var preyear= new Date().getFullYear()-1;
			var sdate = year + "-07-01";
			var edate = year + "-09-30";
			$('#tod').val(edate);
			$('#frm').val(sdate);
		}else if(str == "Morning|Mor"){
			$('#timing').val("10am to 4pm");
			 getpriceCode();
		}
		else if(str == "Evening|EVE"){
			$('#timing').val("5pm to 11pm");
			 getpriceCode();
		}
		else if(str == "Full Day|FDA"){
			$('#timing').val("10am to 11pm");
			 getpriceCode();
		}
		//amit changed
		
		
	}
   </script>
      
@stop