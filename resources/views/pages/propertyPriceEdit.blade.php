@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Price</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Price Add</li>
            
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="pkgPriceForm" method="post" action="{{ SITEPATH.'masters/price/propriceUpdate' }}" name="pkgPriceForm" >
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Price Master
                </h4>
                <div class="form-group row">
                 
                  <div class="col-sm-3">
                     <select class="form-control" onchange="showsec(this.value)" id="input-1" name="type" required>
                         <option>Property</option>
                        <option>Rental</option>
                    </select>
                  </div>				  
                  <div class="col-sm-3">
                     <select class="form-control" onchange="chngDate(this.value)"  id="input-2" name="season" required>
                        <option value="">Select</option>
                        @foreach($season as $val)
							<option class="{{$val->seasonFor}}" <?php if($val->name == $prop[0]->season){ echo "selected"; }?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
						@endforeach
                        
                    </select>
                  </div>
				   
				  <div class="col-sm-2 other" >
                   
					  <input type="text" id="autoclose-datepicker" placeholder="Start Date" value="{{$prop[0]->startDate}}"  data-date-format="yyyy-mm-dd" class="form-control" name="from">
                  </div>
				   <div class="col-sm-2 other" >
                    
					  <input type="text" id="autoclose-datepicker1" class="form-control" value="{{$prop[0]->endDate}}"  data-date-format="yyyy-mm-dd" placeholder="End Date" name="to">
                  </div>
				  <div class="col-sm-2 other" >
                    
					  <input type="text" id="timing" class="form-control"  value="{{$prop[0]->time}}"  placeholder="time" readonly name="timing">
                  </div>
                </div>
			
			<div id="prop" >
			  <h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Property Info
                </h4>
					<div class="form-group row">
						 <label for="input-11" class="col-sm-2 col-form-label">Sr. No</label>
						  <div class="col-sm-4">
							 <input type="text" readonly class="form-control" id="input-11" value="{{$prop[0]->id }}" name="srno">
						  </div>
						<label for="input-12" class="col-sm-2 col-form-label">Property Name</label>  
						<div class="col-sm-4">
							
							 <select class="form-control" onchange="pDetail(this.value)" id="input-12" name="prop" required>
							 <option>Select</option>
								@foreach($property as $val)
									<option <?php if($val->pro_name == $prop[0]->proName){ echo "selected"; }?>  value="{{ $val->pro_name }}">{{ $val->pro_name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="input-13" class="col-sm-2 col-form-label">Property Code</label>
						<div class="col-sm-4">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->proCode }}" id="input-13" name="propcode">
						</div>
						<label for="input-14" class="col-sm-2 col-form-label">Price Code</label>
						<div class="col-sm-4">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->priceCode }}" id="input-14" name="prccode">
						</div>
					</div>
					<div class="form-group row">
						<label for="input-15" class="col-sm-2 col-form-label">Period</label>
						<div class="col-sm-4">
							<select class="form-control"  onchange="getpriceCode()" id="input-15" name="period" required>
								@foreach($period as $val)
									<option  <?php if($val->name == $prop[0]->period){ echo "selected"; }?>  value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>
								@endforeach
							</select>
						</div>
						<label for="input-16" class="col-sm-2 col-form-label">Capacity</label>
						<div class="col-sm-4">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->capacity }}" id="input-16" name="capacity">
						</div>
					</div>	
					<div class="form-group row">
						<label for="input-17" class="col-sm-2 col-form-label">Min</label>
						<div class="col-sm-4">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->min }}" id="input-17" name="min">
						</div>
						<label for="input-18" class="col-sm-2 col-form-label">Max</label>
						<div class="col-sm-4">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->max }}" id="input-18" name="max">
						</div>
					</div>
					<h4 class="form-header text-uppercase">
               
                </h4>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Charges</label>
						
						<label  class="col-sm-3 col-form-label">Rate</label>
						
						<label  class="col-sm-3 col-form-label" >Tax (%)</label>
						<label  class="col-sm-3 col-form-label">Amount</label>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Rent</label>
						
						<div class="col-sm-3">
							 <input type="text"  class="form-control" value="{{$prop[0]->rentRate }}" id="input-19" name="rentrate">
						</div>
						
						<div class="col-sm-3">
							 <input type="text"  class="form-control" value="{{$prop[0]->rentTax }}"  id="input-20" onfocusout="propPrice('rentrate','renttax','rentamount')" name="renttax">
						</div>
						<div class="col-sm-3">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->rentTotalPrice }}"  id="input-21" name="rentamount">
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Cleaning</label>
						
						<div class="col-sm-3">
							 <input type="text"  class="form-control" value="{{$prop[0]->cleaningRate }}" id="input-22" name="clerate">
						</div>
						
						<div class="col-sm-3">
							 <input type="text" value="{{$prop[0]->cleaningTax }}" onfocusout="propPrice('clerate','cletax','cleamount')" class="form-control" id="input-23" name="cletax">
						</div>
						<div class="col-sm-3">
							 <input type="text" readonly class="form-control" value="{{$prop[0]->cleaningTotalRate }}" id="input-24" name="cleamount">
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Electrical</label>
						
						<div class="col-sm-3">
							 <input type="text"  class="form-control" value="{{$prop[0]->eleRate }}" id="input-25" name="elerate">
						</div>
						
						<div class="col-sm-3">
							 <input type="text" onfocusout="propPrice('elerate','eletax','eleamount')" value="{{$prop[0]->eleTax }}" class="form-control" id="input-26" name="eletax">
						</div>
						<div class="col-sm-3">
							 <input type="text" readonly class="form-control" id="input-27" value="{{$prop[0]->eleTotalRate }}" name="eleamount">
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Other</label>
						
						<div class="col-sm-3">
							 <input type="text"  class="form-control" id="input-25" value="{{$prop[0]->othRate }}" name="othrate">
						</div>
						
						<div class="col-sm-3">
							 <input type="text" onfocusout="propPrice('othrate','othtax','othamount')" value="{{$prop[0]->othTax }}" class="form-control" id="input-26" name="othtax">
						</div>
						<div class="col-sm-3">
							 <input type="text" readonly class="form-control" id="input-27" value="{{$prop[0]->othTotalRate }}" name="othamount">
						</div>
					</div>
					<div class="form-group row">
						<label  class="col-sm-3 col-form-label">Total</label>
						<div class="offset-6 col-sm-3">
							 <input type="text"  class="form-control" id="input-27" value="{{$prop[0]->totalAmount }}" readonly value="0" name="totalAmount">
						</div>
					</div>
            </div>
			<div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <script>
   function showsec(val){
	   if(val=="Club"){
		 $('.other').hide();  
		 $('.club').show();
		$('#packg').show();
		$('#prop').hide();
		var url = '<?php echo SITEPATH ?>masters/price/pkgpricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
	   if(val=="Property"){
		$('.club').hide();
		 $('.other').show();  
		$('#prop').show();
		$('#packg').hide();
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
	   if(val=="Rental"){
		   $('.club').hide();
		    $('.other').show();  
		$('#prop').show();
		$('#packg').hide();
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('action',url);
	   }
   }
   function myFunction(){
	 var total =  $('input:text[name=total]').val();
	 var bcharge = parseInt(total)*100/118;
	  $('input:text[name=basiccharge]').val(Math.round(bcharge));
	 var tax = 18;
	 var taxamt = Math.round(parseInt(total) - parseInt(bcharge));
	 $('input:text[name=taxamt]').val(taxamt);
	 //amit changes
	 var tax_cgst = parseInt(taxamt)/2;
	 $('input:text[name=tax_cgst]').val(tax_cgst);
	 var tax_sgst = parseInt(taxamt)/2;
	 $('input:text[name=tax_sgst]').val(tax_sgst);
	 //amit changes done
	 

   }
   function getpriceCode(){
	  
	 var seasons =  $('#input-2 :selected').val();
	 var spsea = seasons.split("|");
	  var pkgname =  $('#input-3 :selected').val();
	 var sppkg = pkgname.split("|");
	 var memtype =  $('#input-5 :selected').val();
	 var spmem = memtype.split("|");
	 var period =  $('#input-15 :selected').val();
	 var spper = period.split("|");
	 var code = spsea[1]+''+sppkg[1]+''+spmem[1]+''+spper[1];
	 $('input:text[name=prccode]').val(code);
   }
   function pDetail(str){ 
   getpriceCode();
	$.ajax({
		url: "<?php echo SITEPATH;?>ajax/property/pro_name/"+str,
		type: 'GET',
		dataType: 'json',
		success: function (data) {
			$('#input-13').val(data[0].pro_code);
			$('#input-17').val(data[0].min_per);
			$('#input-18').val(data[0].max_per);
			$('#input-16').val(data[0].capacity);
			
		}
	});
	}
	function propPrice(str1,str2,str3)
	{
	 var total =  $('input:text[name=totalAmount]').val();
	 var bcharge =  $('input:text[name='+str1+']').val();
	 var tax =  $('input:text[name='+str2+']').val();
	 var taxamt = Math.round((parseInt(bcharge) * parseInt(tax)) / 100);
	 var totamt = parseInt(bcharge) + taxamt;
	 var totalAmount = parseInt(total) + totamt;
	 
	 $('input:text[name='+str3+']').val(totamt);
	 $('input:text[name=totalAmount]').val(totalAmount);
	}
	function changeURL()
	{
		var url = '<?php echo SITEPATH ?>masters/price/propricesubmite';
		$('#pkgPriceForm').attr('href',url);
	}
	function chngDate(str){
		//if(str == "Season|SEA"){
		//	var year = new Date().getFullYear();
		//	 var preyear= new Date().getFullYear()-1;
		//	var sdate = "1-10-" + preyear;
		//	var edate = "30-06-" + year;
		//	$('#tod').val(edate);
		//	$('#frm').val(sdate);
			
		//}else{
		//	var year = new Date().getFullYear();
		//	 var preyear= new Date().getFullYear()-1;
		//	var sdate = "1-7-" + year;
		//	var edate = "30-9-" + year;
		//	$('#tod').val(edate);
		//	$('#frm').val(sdate);
		//}
		
		//amit changed
		if(str == "Season|SEA"){
			var year = new Date().getFullYear();
			 var preyear= new Date().getFullYear()-1;
			var sdate = preyear + "-10-01";
			var edate = year + "-06-30";
			$('#tod').val(edate);
			$('#frm').val(sdate);
			
		}else if(str == "Off Season|OSE"){
			var year = new Date().getFullYear();
			 var preyear= new Date().getFullYear()-1;
			var sdate = year + "-07-01";
			var edate = year + "-09-30";
			$('#tod').val(edate);
			$('#frm').val(sdate);
		}else if(str == "Morning|Mor"){
			$('#timing').val("10am to 4pm");
			 getpriceCode();
		}
		else if(str == "Evening|EVE"){
			$('#timing').val("5pm to 11pm");
			 getpriceCode();
		}
		else if(str == "Full Day|FDA"){
			$('#timing').val("10am to 11pm");
			 getpriceCode();
		}
		//amit changed
		
		
	}
   </script>
      
@stop