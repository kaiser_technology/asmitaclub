@extends('layouts.default')
@section('content')
	<div class="content-wrapper">
		<div class="container-fluid">
		  <!--Start Dashboard Content-->
		  <!--<img style="text-align:center" src="{{RESOURCE_PATH}}images/logo-icon.png" class="logo-icon" alt="logo icon">-->
		  <h2 style="text-align:center">Welcome To Asmita Club</h2>
		<div class="row" style="padding-top:50px">
		 <!--<div class="col-12 col-lg-8 col-xl-8">
			<div class="card">
			 <div class="card-header">Site Traffic
			   <div class="card-action">
				 <div class="dropdown">
				 <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
				  <i class="icon-options"></i>
				 </a>
					<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="javascript:void();">Action</a>
					<a class="dropdown-item" href="javascript:void();">Another action</a>
					<a class="dropdown-item" href="javascript:void();">Something else here</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="javascript:void();">Separated link</a>
				   </div>
				  </div>
			   </div>
			 </div>
			 <div class="card-body">
				<ul class="list-inline">
				  <li class="list-inline-item"><i class="fa fa-circle mr-2 text-white"></i>New Visitor</li>
				  <li class="list-inline-item"><i class="fa fa-circle mr-2 text-light"></i>Old Visitor</li>
				</ul>
				<canvas id="chart1" height="115"></canvas>
			 </div>
			 
			 <div class="row m-0 row-group text-center border-top border-light-3">
			   <div class="col-12 col-lg-4">
				 <div class="p-3">
				   <h5 class="mb-0">0.02K</h5>
				   <small class="mb-0">Overall Visitor <span> <i class="fa fa-arrow-up"></i> 2.43%</span></small>
				 </div>
			   </div>
			   <div class="col-12 col-lg-4">
				 <div class="p-3">
				   <h5 class="mb-0">15:48</h5>
				   <small class="mb-0">Visitor Duration <span> <i class="fa fa-arrow-up"></i> 12.65%</span></small>
				 </div>
			   </div>
			   <div class="col-12 col-lg-4">
				 <div class="p-3">
				   <h5 class="mb-0">6.00</h5>
				   <small class="mb-0">Pages/Visit <span> <i class="fa fa-arrow-up"></i> 5.62%</span></small>
				 </div>
			   </div>
			 </div>
			 
			</div>
		 </div>-->
        <!-- Daily Sale -->
		 <div class="col-12 col-lg-4 col-xl-4">
			<div class="card">
			   <div class="card-header">Daily Sales</div>
			   <!--<div class="card-body">
				  <canvas id="chart2" height="180"></canvas>
			   </div>-->
			   <div class="table-responsive">
				 <table class="table align-items-center">
				   <tbody>
					 <tr>
					   <td><i class="fa fa-circle text-white mr-2"></i> Daily Visitors</td>
					   <td style="text-align: right;">{{money_format('%!i',$todaydv)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Membership</td>
					   <td style="text-align: right;">{{money_format('%!i',$todaymem)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Rent</td>
					   <td style="text-align: right;">{{money_format('%!i',$todayeve)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-2 mr-2"></i><b>Grand Total</b></td>
					   <td style="text-align: right;"><b>{{money_format('%!i',$todaydv + $todaymem + $todayeve)}}</b></td>
					 </tr>
					 <!--<tr>
					   <td><i class="fa fa-circle text-light-3 mr-2"></i>Other</td>
					   <td>$1105</td>
					   <td>+5%</td>
					 </tr>-->
				   </tbody>
				 </table>
			   </div>
			 </div>
		 </div>
		<!-- Daily Sale End -->
		
		<!-- Monthly Sale -->
		<div class="col-12 col-lg-4 col-xl-4">
			<div class="card">
			   <div class="card-header">Current Month Sales</div>
			   <!--<div class="card-body">
				  <canvas id="chart2" height="180"></canvas>
			   </div>-->
			   <div class="table-responsive">
				 <table class="table align-items-center">
				   <tbody>
					 <tr>
					   <td><i class="fa fa-circle text-white mr-2"></i> Daily Visitors</td>
					   <td style="text-align: right;">{{money_format('%!i',$mondv)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Membership</td>
					   <td style="text-align: right;">{{money_format('%!i',$monmem)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Rent</td>
					   <td style="text-align: right;">{{money_format('%!i',$moneve)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-2 mr-2"></i><b>Grand Total</b></td>
					   <td style="text-align: right;"><b>{{money_format('%!i',$mondv + $monmem + $moneve) }}</b></td>
					 </tr>
					 <!--<tr>
					   <td><i class="fa fa-circle text-light-3 mr-2"></i>Other</td>
					   <td>$1105</td>
					   <td>+5%</td>
					 </tr>-->
				   </tbody>
				 </table>
			   </div>
			 </div>
		 </div>
		<!-- Monthly Sale End-->
		
		<!-- Annual Sale -->
		<div class="col-12 col-lg-4 col-xl-4">
			<div class="card">
			   <div class="card-header">Current Year Sales</div>
			   <!--<div class="card-body">
				  <canvas id="chart2" height="180"></canvas>
			   </div>-->
			   <div class="table-responsive">
				 <table class="table align-items-center">
				   <tbody>
					 <tr>
					   <td><i class="fa fa-circle text-white mr-2"></i> Daily Visitors</td>
					   <td style="text-align: right;">{{money_format('%!i',$yerdv)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Membership</td>
					   <td style="text-align: right;">{{money_format('%!i',$yermem)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-1 mr-2"></i>Rent</td>
					   <td style="text-align: right;">{{money_format('%!i',$yereve)}}</td>
					 </tr>
					 <tr>
					   <td><i class="fa fa-circle text-light-2 mr-2"></i><b>Grand Total</b></td>
					   <td style="text-align: right;"><b>{{money_format('%!i',$yerdv + $yermem + $yereve)}}</b></td>
					 </tr>
					 <!--<tr>
					   <td><i class="fa fa-circle text-light-3 mr-2"></i>Other</td>
					   <td>$1105</td>
					   <td>+5%</td>
					 </tr>-->
				   </tbody>
				 </table>
			   </div>
			 </div>
		 </div>
		<!-- Annual Sale End -->
        
		<!--End Dashboard Content-->

		</div>
		<!-- End container-fluid-->
		</div>
		</div>
@stop