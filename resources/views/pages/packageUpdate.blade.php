@extends('layouts.add')

@section('content')

	 <div class="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumb-->

     <div class="row pt-2 pb-2">

        <div class="col-sm-9">

		    <h4 class="page-title">Package</h4>

		    <ol class="breadcrumb">

            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>

            <li class="breadcrumb-item active" aria-current="page">Package Add</li>

           

         </ol>

	   </div>

	  

     </div>

    <!-- End Breadcrumb-->

	<div class="row">

        <div class="col-lg-12">

          <div class="card">

            <div class="card-body">

              <form id="packageForm" name="packageForm" action="{{ SITEPATH.'masters/package/update' }}" method="post">

			  <input type="hidden" name="_token" value="{{ csrf_token() }}">

               <input type="hidden" name="id" value="{{$prop[0]->id}}"/>

                <h4 class="form-header text-uppercase">

                  <i class="fa fa-address-book-o"></i>

                   Package Master

                </h4>

                <div class="form-group row">

                  

                  <label for="input-2" class="col-sm-2 col-form-label">Package Name</label>

                  <div class="col-sm-4">

                    <input type="text" value="{{$prop[0]->pkg_name}}" class="form-control" id="input-2" name="pkgname">

                  </div>

                </div>

               

                <div class="form-group row">

                  <label for="input-3" class="col-sm-2 col-form-label">Package Type</label>

                  <div class="col-sm-4">

                     <select class="form-control" id="input-3" name="pkgtype" required>packtype

                         @foreach($packtype as $val)

							<option <?php if($prop[0]->pkg_type ==  $val->name){ echo "selected";} ?> value="{{ $val->name.'|'.$val->code }}">{{ $val->name }}</option>

						@endforeach

                    </select>

                  </div>

				  <label for="input-4" class="col-sm-2 col-form-label">Package Code</label>

                  <div class="col-sm-4">

                   <input type="text"  value="{{$prop[0]->pkg_code}}" class="form-control" id="input-4" name="pkgcode">

                  </div>

                </div>

				 <div class="form-group row">

                  <label for="input-5" class="col-sm-2 col-form-label">No of Person</label>

                  <div class="col-sm-4">

                     <select class="form-control" id="input-5" name="noofper" required>

                        <option value="" >Select</option>

                        <option <?php if($prop[0]->no_of_per == "1"){ echo "selected";} ?>>1</option>

                        <option <?php if($prop[0]->no_of_per == "2"){ echo "selected";} ?>>2</option>

                        <option <?php if($prop[0]->no_of_per == "3"){ echo "selected";} ?>>3</option>

                        <option <?php if($prop[0]->no_of_per == "4"){ echo "selected";} ?>>4</option>

                    </select>

                  </div>

				  <label for="input-6" class="col-sm-2 col-form-label">Services Included</label>

				  <?php $i=1; $z=1; 
				 // echo $prop[0]->services;
				  $servarr = explode(",",$prop[0]->services);
				 // print_r($servarr);exit;
				  ?>

					  @foreach($services as $val)

							@if($i == 1)

								<div class="col-sm-2">

							@endif

							<div class="icheck-material-white">

							<input type="checkbox" <?php if(in_array($val->name,$servarr)){ echo "checked"; }?> id="user-checkbox{{$z}}" value="{{ $val->name }}" name="serv[]"/>

							<label for="user-checkbox{{$z}}">{{ $val->name }} </label>

							</div>

							<?php $i++; $z++; ?>

							@if(count($services)/2 + 1 == $i)

								 <?php $i=1; ?>

								</div>

							@endif

							 

						@endforeach

				  

					</div>

				

                <div class="form-footer">

                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>

                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>

                </div>

              </form>

            </div>

          </div>

        </div>

      </div><!--End Row-->



    </div>

    <!-- End container-fluid-->

    

    </div><!--End content-wrapper-->

   

      

@stop