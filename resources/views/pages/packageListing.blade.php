@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Package Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void(0)">Package Listing</a></li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}masters/package/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Package Creation</a>
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Package Name</th>
                        <th>Package Code</th>
                        <th>Package Type</th>
                        <th>No Of Person</th>
                        <th>Services</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
					<?php $i = 1;?>
                    @foreach($list as $val)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->pkg_name}}</td>
                        <td>{{$val->pkg_code}}</td>
                        <td>{{$val->pkg_type}}</td>
                        <td>{{$val->no_of_per}}</td>
                        <td>{{$val->services}}</td>
                        <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a>
						<a href="{{ SITEPATH.'masters/package/edit/'.$val->id }}" id="eidtbuton" ><i aria-hidden="true" class="fa fa-edit"></i></a>
						<a href="{{ SITEPATH.'masters/package/delete/'.$val->id }}" id="delbuton"><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
					@endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Sr No</th>
                        <th>Package Code</th>
                        <th>Package Name</th>
                        <th>Package Type</th>
                        <th>No Of Person</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop