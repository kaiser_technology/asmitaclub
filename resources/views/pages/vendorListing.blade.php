@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Vendor Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void(0)">Vendor Listing</a></li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}masters/vendor/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Vendor Creation</a>
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Vendor Name</th>
                        <th>Address</th>
                        <th>Contect Number</th>
                        <th>Vendor Code</th>
                        <th>Vendor Type</th>
                        <th>Vendor Status</th>
                        <th>Diposit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
					<?php $i = 1;?>
                    @foreach($list as $val)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->vendor_name}}</td>
                        <td>{{$val->address}}</td>
                        <td>{{$val->contactnumber1.', '.$val->contactnumber2}}</td>
                        <td>{{$val->vend_code}}</td>
                        <td>{{$val->vend_type}}</td>
                        <td>{{$val->vend_status}}</td>
                        <td>{{$val->diposit}}</td>
                       <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a>
						<a href="{{ SITEPATH.'masters/vendor/edit/'.$val->id }}" id="eidtbuton"><i aria-hidden="true" class="fa fa-edit"></i></a>
						<a href="{{ SITEPATH.'masters/vendor/delete/'.$val->id }}" id="delbuton"><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
					@endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Sr No</th>
                        <th>Vendor Name</th>
                        <th>Address</th>
                        <th>Contect Number</th>
                        <th>Vendor Code</th>
                        <th>Vendor Type</th>
                        <th>Vendor Status</th>
                        <th>Diposit</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop