@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Enquiry Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Enquiry Listing</li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}enquiry/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Enquiry Creation</a>
       
       
      </div>
     </div>
     </div>
	 @if($sessiondata['role_id'] != "1")
	  <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Pending Payment Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
						
                        <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;
					$udata = array();
					?>
                    @foreach($plist as $val)
					
                    <tr>
                      
                        <td>{{$i}}</td>
                        <td>{{$val->memborCode}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->contactNumber}}</td>
                        <td>{{$val->totalamount}}</td>
                        <td>{{$val->balance}}</td>
                        <td>{{$val->paystatus}}</td>
                        <td> @if($val->paystatus != "Completed" )<a href="{{SITEPATH}}club/payment/pay/{{$val->id}}" ><i class="fa fa-inr" aria-hidden="true"></i>Pay</a> @endif  @if($val->settlement == "No" )<a href="{{SITEPATH}}club/payment/settlement/{{$val->id}}" > Settlement</a> | <a href="{{SITEPATH}}report/club/receipt/{{Helper::getPmtId($val->id)}}" > Vew Receipt</a> @endif </td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
						
                         <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
	  @endif
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Settlement Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered">
                <thead>
                    <tr>
						
                        <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;
					//print_r($slist);exit;
					?>
                    @foreach($slist as $val)
                    <tr>
						
                        <td>{{$i}}</td>
                        <td>{{$val->memborCode}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->contactNumber}}</td>
                        <td>{{$val->totalamount}}</td>
                        <td>{{$val->balance}}</td>
                        <td>{{$val->paystatus}}</td>
                        <td> @if($val->paystatus != "Completed" )<a href="{{SITEPATH}}club/payment/pay/{{$val->id}}" ><i class="fa fa-inr" aria-hidden="true"></i>Pay</a>  @endif  <a href="{{SITEPATH}}report/club/receipt/{{Helper::getPmtId($val->id)}}" > Vew Receipt</a></td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
						
                         <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Add Data</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example2" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                       
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;
					$udata=array();
					?>
                    @foreach($list as $val)
					<?php 
					
					// if(in_array($val->memborCode,$udata)){ continue; }else{
					// 	$udata[] = $val->memborCode;
					// }
						?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->memborCode}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{$val->contactNumber}}</td>
                        <td>{{$val->totalamount}}</td>
                        <td>{{$val->balance}}</td>
                        <td>{{$val->paystatus}}</td>
                       
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                         <th>Sr. No.</th>
                        <th>Member Code</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Total Amount</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop