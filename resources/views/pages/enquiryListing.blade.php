@extends('layouts.Listing')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Enquiry Listing</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Enquiry Listing</li>
            
         </ol>
	   </div>
	   <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="{{SITEPATH}}enquiry/add" type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Enquiry Creation</a>
       
       
      </div>
     </div>
     </div>
   
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Enquiry Date</th>
                        <th>Enquiry For</th>
                        <th>Contactno 1</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Pincode</th>
                        <th>Email Id</th>
                        <th>Refer</th>
                         <th>Enquery Detail</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php $i = 1;?>
                    @foreach($list as $val)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->next_follow_date}}</td>
                        <td>{{$val->enq_for}}</td>
                        <td>{{$val->contact_no}}</td>
                        <td>{{$val->contact_name}}</td>
                        <td>{{$val->address}}</td>
                        <td>{{$val->pincode}}</td>
                        <td>{{$val->email}}</td>
                        <td>{{$val->refer}}</td>
                        <td>{{$val->enq_detail}}</td>
                        <td>{{$val->status}}</td>

                        <td><!--a href="javascript:void(0)" data-toggle="modal" data-target="#largesizemodal{{$i}}" ><i aria-hidden="true" class="fa fa-eye"></i></a-->						
                        <a href="{{SITEPATH}}enquiry/edit/{{$val->id}}" id="eidtbuton"><i aria-hidden="true" class="fa fa-edit"></i></a>
                        <a href="{{SITEPATH}}enquiry/delete/{{$val->id}}" id="delbuton" ><i aria-hidden="true" class="fa fa-trash"></i></a></td>
                    </tr>
                   <?php $i++;?>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                         <th>Sr. No.</th>
                        <th>Enquiry Date</th>
                        <th>Enquiry For</th>
                        <th>Contactno 1</th>
                        <th>Contactno 2</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Pincode</th>
                        <th>Email Id</th>
                        <th>Refer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->
@stop