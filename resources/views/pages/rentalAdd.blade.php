@extends('layouts.add')
@section('content')
	 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Booking Detail</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITEPATH}}dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Booking Add</li>
           
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->
	<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
               <form id="vendorForm" method="post" enctype="multipart/form-data" action="{{ SITEPATH.'event/bookingrentals/rentalsubmite' }}" class="">
			   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   Peropery Info
                </h4>
                <div class="form-group row">
                  <label for="input-1" class="col-sm-2 col-form-label">Booking Number</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-1" name="bookingNo" readonly value="{{$eventid}}">
                  </div>
                 <label for="fromdate" class="col-sm-2 col-form-label">From</label>
                  <div class="col-sm-2">
                    <input type="text" id="autoclose-datepicker1" data-date-format="yyyy-mm-dd" class="form-control" name="from">
                  </div>	
                 
                  <label for="todate" class="col-sm-2 col-form-label">TO</label>
                  <div class="col-sm-2">
                  <input type="text" id="autoclose-datepicker2"  data-date-format="yyyy-mm-dd" class="form-control" name="to">
                  </div>
				  
                </div>
				<!---- ROW 2 ----->
                <div class="form-group row">
					 <label for="input-2" class="col-sm-2 col-form-label">Period</label>
                  <div class="col-sm-2">
                     <select class="form-control" id="input-2" name="period" required>
                   
                          <option>MONTHLY</option>
                        
                    </select>
                  </div>
				   <!--label for="input-3" class="col-sm-2 col-form-label">Session</label>
                  <div class="col-sm-2">
                     <select class="form-control" id="input-3" name="session" required>
                       @foreach($season as $val)
							@if($val->name == "Season" || $val->name == "Off Season") @php continue; @endphp @endif
                          <option value="{{ $val->name }}">{{ $val->name }}</option>
                        @endforeach
                    </select>
                  </div-->
				  <label for="input-4" class="col-sm-2 col-form-label">Property Name</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-4" name="proptypename" onchange="pDetail(this.value)" required>
                         @foreach($property as $val)
                          <option value="{{ $val->pro_name }}">{{ $val->pro_name }}</option>
                        @endforeach
                    </select>
                  </div>
				</div>
				<!---- ROW 3 -----
				<div class="form-group row">
					<label for="input-5" class="col-sm-2 col-form-label">Mex Person</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-5" class="form-control" name="maxperson">
                  </div>
					<label for="input-6" readonly class="col-sm-2 col-form-label">Min Person</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-6" readonly class="form-control" name="minperson">
                  </div>
				  <label for="input-7" class="col-sm-2 col-form-label">Property Code</label>
                  <div class="col-sm-2">
                  <input type="text" id="input-7" readonly class="form-control" name="propCode">
                  </div>
				  <label for="input-8" readonly class="col-sm-2 col-form-label">Status</label>
                  <div class="col-sm-2" id="status">
                 
                  </div>
				</div>
				<!---- ROW 4 ----->
				<div class="form-group row">
					  <div class="col-sm-6">
					<button type="button" onclick="avability()" class="btn btn-success"><i class="fa fa-times"></i> Check</button>
                   	
                  </div>
				</div>
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Booking Info
                </h4>
				<!---- ROW 5 ----->
				<div class="form-group row">
					<label for="input-9" class="col-sm-2 col-form-label">Mobile No</label>
                   <div class="col-sm-2">
                     <input type="text" class="form-control" id="input-9" name="mobnum">
					 </div>
				
                 <label for="input-10" class="col-sm-2 col-form-label">Customer Name</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-10" name="custname">
                  </div>
				  <label for="input-11" class="col-sm-2 col-form-label">Customer Address</label>
                  <div class="col-sm-2">
                     <textarea class="form-control" name="custadd" rows="4" id="input-11"></textarea>
                  </div>
				</div>
				<!---- ROW 3 ----->
				<div class="form-group row">
				
				<label for="input-20" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-2">
					<input type="text" class="form-control" id="input-20" name="email">
                  </div>
					<label for="input-18" class="col-sm-2 col-form-label">Gender</label>
                  <div class="col-sm-2">
					 <select class="form-control" id="input-18" name="gender" required>
                        <option value="" >Select</option>
                        <option>Male</option>
                        <option>FeMale</option>
                        <option>Other</option>
                    </select>
                  </div>
					<label for="input-13" class="col-sm-2 col-form-label">Pincode</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="input-13" name="pincode">
                  </div>
				  
				</div>
				<!---- ROW 5 ----->
                <div class="form-group row">
				 <label for="input-14" class="col-sm-2 col-form-label">Event</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-14" name="evetn" required>
                        <option value="" >Select</option>
                        <option>CLASSES</option>
                        <option>SHOPE</option>
                    </select>
                  </div>
				 <label for="input-15" class="col-sm-2 col-form-label">DOB</label>
                  <div class="col-sm-2">
                    <input type="text" id="autoclose-datepickerDOB"  data-date-format="yyyy-mm-dd" data-date="1965-02-02" value="1965-02-02" data-date-viewmode="years" onchange="countAge(this.value)" class="form-control" name="dob">
                  </div>
				  <label for="input-16" class="col-sm-2 col-form-label">AGE</label>
                  <div class="col-sm-2">
                
				   <input type="text" class="form-control" id="input-16" name="age">
                  </div>
				 
				</div>
				<!---- ROW 6 -----
                <div class="form-group row">
					
				  <label for="input-19" class="col-sm-2 col-form-label">Pancard</label>
                  <div class="col-sm-2">
					<input type="text" class="form-control" id="input-19" name="pancard">
                  </div>
					
                </div-->
				<h4 class="form-header text-uppercase">
                <i class="fa fa-envelope-o"></i>
                  Payment Details
                </h4>
				<!---- ROW 6 ----->
                <div class="form-group row">
				 <label for="input-21" class="col-sm-2 col-form-label">Rental</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-21" class="form-control" name="rental">
                  </div>
                  <label for="input-31" class="col-sm-2 col-form-label">Advance</label>
                  <div class="col-sm-2">
                    <input type="text"  id="input-31" onchange="countprice(this.value)" class="form-control" name="adv">
                  </div>
                  <label for="input-33"  class="col-sm-2 col-form-label">Balance</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-33" class="form-control" name="balance">
                  </div>
				  <!--<label for="input-23" class="col-sm-2 col-form-label">Cleaning</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-23" class="form-control" name="cleaning">
                  </div>
                  <label for="input-22" class="col-sm-2 col-form-label">Electrical</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-22" class="form-control" name="electrical">
                  </div>-->
				</div>
				<!---- ROW 6 ----->
                <div class="form-group row">
				<label for="input-24" class="col-sm-2 col-form-label">Basic Charge</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-24" class="form-control" name="other">
                  </div>
				
				<label for="input-25" class="col-sm-2 col-form-label">CGST</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-25" class="form-control" name="cgst">
                  </div>
                  
                  <label for="input-25" class="col-sm-2 col-form-label">SGST</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-26" class="form-control" name="sgst">
                  </div>
				  
				</div>
				<!---- ROW 6 ----->
                <!--<div class="form-group row">
					<label for="input-26" class="col-sm-1 col-form-label">Total</label>
                  <div class="col-sm-2">
                    <input type="text" readonly id="input-26" class="form-control" name="total">
                  </div>
				
				<label for="input-27" class="col-sm-1 col-form-label">Min Advance</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-27" class="form-control" name="minadv">
                  </div>
				<label for="input-28"  class="col-sm-1 col-form-label">Total B</label>
                  <div class="col-sm-2">
                     <input type="text" readonly id="input-28" class="form-control" name="totalb">
                  </div>
				  <label for="input-29" readonly class="col-sm-1 col-form-label">G Total A+B</label>
                  <div class="col-sm-2">
                     <input type="text" id="input-29" class="form-control" name="totalg">
                  </div>
				</div>-->
				<!--- row 10 -->
				<!--<div class="form-group row">
				<label for="input-32" class="col-sm-1 col-form-label">Discount</label>
                  <div class="col-sm-2">
                     <input type="text" onchange="aftdiscount(this.value)" id="input-32" class="form-control" name="disount">
                  </div>
				
				  <label for="input-34"  class="col-sm-1 col-form-label">Due date</label>
                  <div class="col-sm-2">
                     <input type="text" id="autoclose-datepicker5"  data-date-format="yyyy-mm-dd" class="form-control" name="due">
                  </div>
				</div>-->
				<!--- row 11 -->
				<div class="form-group row">
					<label for="input-35" class="col-sm-1 col-form-label">Payment</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="input-35" name="payment" required>
                        <option >Cash</option>
                        <option>Cheque</option>
                        <option>Card</option>
                    </select>
                  </div>
				
				<label for="input-36" class="col-sm-1 col-form-label">Cheque/DD No</label>
                  <div class="col-sm-2">
                     <input type="text"  id="input-36" class="form-control" name="cheno">
                  </div>
				<label for="input-37"  class="col-sm-1 col-form-label">Cheque Date</label>
                  <div class="col-sm-2">
                   <input type="text" id="autoclose-datepicker6"  data-date-format="yyyy-mm-dd" class="form-control" name="chedate">
                  </div>
				  <label for="input-38"  class="col-sm-1 col-form-label">Bank Name</label>
                  <div class="col-sm-2">
                     <input type="text" id="input-38" class="form-control" name="bankName">
                  </div>
				</div>
                <div class="form-footer">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Reset</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <script>
   function pDetail(){ 
		var str = $('#input-4').val();	
	
		 $.ajax({
            url: "<?php echo SITEPATH;?>ajax/property_price_tbl/proName/"+str,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				//$('#input-7').val(data[0].proCode);
               // $('#input-5').val(data[0].max);
                //$('#input-6').val(data[0].min);
                $('#input-21').val(data[0].totalAmount);
                // $('#input-22').val(data[0].eleTotalRate);
                // $('#input-23').val(data[0].cleaningTotalRate);
                 //$('#input-24').val(data[0].othTotalRate);
                 //$('#input-25').val(data[0].totalAmount);
                 //$('#input-26').val(data[0].totalAmount);
				 var tot = parseInt(data[0].totalAmount);
				 var minadv = parseInt(data[0].totalAmount);
				 var value = parseInt((minadv*70)/100);
				 var tax = parseInt((tot*18)/100);
				 $('#input-24').val(tot - tax);
				 $('#input-25').val(tax / 2);
				 $('#input-26').val(tax / 2);
				 $('#input-31').val(value);
				 $('#input-33').val(parseInt(data[0].totalAmount) - value);
            }
        });
}
function avability(){ 
fromdate = $('#autoclose-datepicker1').val();
todate = $('#autoclose-datepicker2').val();

		 $.ajax({
            url: "<?php echo SITEPATH;?>event/booking/checkdate/"+fromdate+"/"+todate,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
				if(data.status == "available"){
				$('#status').css("background-color","green");
				}else{
					$('#status').css("background-color","red");
				}
            }
        });
}
function countprice(str)
{
	var val = $('#input-26').val();
	$('#input-33').val(parseInt(val) - parseInt(str));
}
function aftdiscount(str)
{
	var val = $('#input-33').val();
	if(parseInt(str) <= parseInt(val))
	{
	$('#input-33').val(parseInt(val) - parseInt(str));
	}else{
		$('#input-32').val(0);
		alert("Give a valid discount");
	}
}
function countAge(dob)
{
	dob = new Date(dob);
	var today = new Date();
	var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
	$('#input-16').val(age);
}
   </script>
      
@stop