<!DOCTYPE html>
<html lang="en">
	<head>
		@include('includes.meta')
			<!--favicon-->
		  <link rel="icon" href="{{RESOURCE_PATH}}images/favicon.ico" type="image/x-icon">
		  <!-- simplebar CSS-->
		  <link href="{{RESOURCE_PATH}}plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
		  <!-- Bootstrap core CSS-->
		  <link href="{{RESOURCE_PATH}}css/bootstrap.min.css" rel="stylesheet"/>
		  <!-- animate CSS-->
		  <link href="{{RESOURCE_PATH}}css/animate.css" rel="stylesheet" type="text/css"/>
		  <!-- Icons CSS-->
		  <link href="{{RESOURCE_PATH}}css/icons.css" rel="stylesheet" type="text/css"/>
		  <!--Bootstrap Datepicker-->
  <link href="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
		  <!-- Sidebar CSS-->
		  <link href="{{RESOURCE_PATH}}css/sidebar-menu.css" rel="stylesheet"/>
		  <!-- Custom Style-->
		  <link href="{{RESOURCE_PATH}}css/app-style.css" rel="stylesheet"/>
	</head>
	<body class="bg-theme bg-theme1">
		<!-- Start wrapper-->
		<div id="wrapper">
			@include('includes.loader')
			@include('includes.sidebar')
			@include('includes.header')
			<div class="clearfix"></div>
			@yield('content')
			@include('includes.coppy')
		</div>
		<!-- End wrapper-->
	<!-- Bootstrap core JavaScript-->
  <script src="{{RESOURCE_PATH}}js/jquery.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/popper.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="{{RESOURCE_PATH}}plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="{{RESOURCE_PATH}}js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="{{RESOURCE_PATH}}js/app-script.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

  <!--Form Validatin Script-->
    <script src="{{RESOURCE_PATH}}plugins/jquery-validation/js/jquery.validate.min.js"></script>
 <script src="{{RESOURCE_PATH}}js/custom-validation.js"></script>
   <script>
    $('#autoclose-datepicker').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	 $('#autoclose-datepicker1').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepicker3').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepicker4').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepicker5').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepicker6').datepicker({
        autoclose: true,
        todayHighlight: true
      });
	   $('#autoclose-datepickerDOB').datepicker({
        autoclose: true,
        todayHighlight: true
      });
   </script>
	</body>
</html>
