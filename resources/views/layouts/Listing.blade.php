<!DOCTYPE html>
<html lang="en">
	<head>
		@include('includes.meta')
		<link rel="icon" href="{{RESOURCE_PATH}}images/favicon.ico" type="image/x-icon">
		  <!-- simplebar CSS-->
		  <link href="{{RESOURCE_PATH}}plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
		  <!-- Bootstrap core CSS-->
		  <link href="{{RESOURCE_PATH}}css/bootstrap.min.css" rel="stylesheet"/>
		  <!--Data Tables -->
		  <link href="{{RESOURCE_PATH}}plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
		  <link href="{{RESOURCE_PATH}}plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
		  <!-- animate CSS-->
		  <link href="{{RESOURCE_PATH}}css/animate.css" rel="stylesheet" type="text/css"/>
		  <!-- Icons CSS-->
		  <link href="{{RESOURCE_PATH}}css/icons.css" rel="stylesheet" type="text/css"/>
		  <!-- Sidebar CSS-->
		  <link href="{{RESOURCE_PATH}}css/sidebar-menu.css" rel="stylesheet"/>
		  <!-- Custom Style-->
		  <link href="{{RESOURCE_PATH}}css/app-style.css" rel="stylesheet"/>
  <script src="{{RESOURCE_PATH}}js/jquery.min.js"></script>
	</head>
	<body class="bg-theme bg-theme1">
		<!-- Start wrapper-->
		<div id="wrapper">
			@include('includes.loader')
			@include('includes.sidebar')
			@include('includes.header')
			<div class="clearfix"></div>
			@yield('content')
			@include('includes.coppy')
		</div>
		<!-- End wrapper-->
		  <!-- Bootstrap core JavaScript-->
  <script src="{{RESOURCE_PATH}}js/popper.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="{{RESOURCE_PATH}}plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="{{RESOURCE_PATH}}js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="{{RESOURCE_PATH}}js/app-script.js"></script>

  <!--Data Tables js-->
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/jszip.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
  <script src="{{RESOURCE_PATH}}plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>

    <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
       // buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
		
            buttons: [{
                    extend: 'excel',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'csv',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'print',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                } ]
       
		
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
	  
	   var table = $('#example1').DataTable( {
        lengthChange: false,
       // buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
		
            buttons: [{
                    extend: 'pdf',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },{
                    extend: 'excel',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'csv',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'print',
                    title: 'ASMITA RESORTS PVT. LTD.',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                } ]
       
		
      } );
 
     table.buttons().container()
        .appendTo( '#example1_wrapper .col-md-6:eq(0)' );
      } );	
			       var table = $('#example2').DataTable( {        lengthChange: false,		            buttons: [{                    extend: 'excel',                    title: 'ASMITA RESORTS PVT. LTD.',                    exportOptions: {                        columns: "thead th:not(.noExport)"                    }                }, {                    extend: 'csv',                    title: 'ASMITA RESORTS PVT. LTD.',                    exportOptions: {                        columns: "thead th:not(.noExport)"                    }                }, {                    extend: 'print',                    title: 'ASMITA RESORTS PVT. LTD.',                    exportOptions: {                        columns: "thead th:not(.noExport)"                    }                } ]       		      } );      table.buttons().container()        .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
      
      
    </script>
	</body>
</html>
