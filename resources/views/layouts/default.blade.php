<!DOCTYPE html>
<html lang="en">
	<head>
		@include('includes.meta')
		@include('includes.header-css')
	</head>
	<body class="bg-theme bg-theme1">
		<!-- Start wrapper-->
		<div id="wrapper">
			@include('includes.loader')
			@include('includes.sidebar')
			@include('includes.header')
			<div class="clearfix"></div>
			@yield('content')
			@include('includes.coppy')
		</div>
		<!-- End wrapper-->
		@include('includes.footer')
	</body>
</html>
