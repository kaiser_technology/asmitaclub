<!DOCTYPE html>
<html lang="en">
	<head>
		@include('includes.meta')
  <!--favicon-->
  <link rel="icon" href="{{RESOURCE_PATH}}images/favicon.ico" type="image/x-icon"/>
  <!-- simplebar CSS-->
  <link href="{{RESOURCE_PATH}}plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="{{RESOURCE_PATH}}css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="{{RESOURCE_PATH}}css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="{{RESOURCE_PATH}}css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="{{RESOURCE_PATH}}css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="{{RESOURCE_PATH}}css/app-style.css" rel="stylesheet"/>
      <script src="{{RESOURCE_PATH}}js/jquery.min.js"></script>
	</head>
	<body class="bg-theme bg-theme1">
		<!-- Start wrapper-->
		<div id="wrapper">
			
			<div class="clearfix"></div>
			@yield('content')
			
		</div>
		<!-- End wrapper-->

  <script src="{{RESOURCE_PATH}}js/popper.min.js"></script>
  <script src="{{RESOURCE_PATH}}js/bootstrap.min.js"></script>
	
  <!-- simplebar js -->
  <script src="{{RESOURCE_PATH}}plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="{{RESOURCE_PATH}}js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="{{RESOURCE_PATH}}js/app-script.js"></script>
	</body>
</html>
